-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 31 2013 г., 21:29
-- Версия сервера: 5.1.66
-- Версия PHP: 5.3.3-7+squeeze16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `coffe_group_a`
--

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_type_id` int(11) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_type_id`, `thumbnail`, `full`, `sort_order`, `status`, `lastmod`) VALUES
(5, 2, 'banner-image-5-100x60.jpg', 'banner-image-5.jpg', 0, 1, '2013-06-12 09:14:47'),
(10, 2, 'banner-image-10-100x60.jpg', 'banner-image-10.jpg', 0, 1, '2013-06-12 09:15:02'),
(11, 2, 'banner-image-11-100x60.jpg', 'banner-image-11.jpg', 0, 1, '2013-06-12 09:15:22'),
(12, 2, 'banner-image-12-100x60.jpg', 'banner-image-12.jpg', 0, 1, '2013-06-12 09:15:41');

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

CREATE TABLE IF NOT EXISTS `banner_translation` (
  `banner_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`banner_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banner_translation`
--

INSERT INTO `banner_translation` (`banner_id`, `language_code`, `url`, `title`, `description`, `description_img`) VALUES
(5, 'ru', '/news', 'Ravasio', '', NULL),
(10, 'ru', '/', 'Bristot', '', NULL),
(11, 'ru', '/', 'Fiorenzato', '', NULL),
(12, 'ru', '/', 'SAB', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_type`
--

CREATE TABLE IF NOT EXISTS `banner_type` (
  `banner_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_who` int(11) NOT NULL,
  `updated_who` int(11) NOT NULL,
  PRIMARY KEY (`banner_type_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `banner_type`
--

INSERT INTO `banner_type` (`banner_type_id`, `ident`, `name`, `description`, `sort_order`, `status`, `created_at`, `updated_at`, `created_who`, `updated_who`) VALUES
(2, 'LEFT', 'Левый баннер', 'Размеры баннеров должны быть 227 × 70 px', 1, 1, '2011-11-30 18:07:51', '2013-06-12 09:21:09', 4, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `blocking_ip`
--

CREATE TABLE IF NOT EXISTS `blocking_ip` (
  `ip` varchar(16) NOT NULL,
  `note` varchar(1024) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `block_count` int(11) DEFAULT '0',
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blocking_ip`
--

INSERT INTO `blocking_ip` (`ip`, `note`, `date`, `block_count`) VALUES
('188.190.99.31', 'Бан!', '2013-03-30 09:04:54', 0),
('1231233123', 'dfsgdsfg', '2013-03-30 09:06:53', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting`
--

CREATE TABLE IF NOT EXISTS `contact_setting` (
  `contact_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_code` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `fax_code` varchar(255) NOT NULL,
  `fax_number` varchar(255) NOT NULL,
  `phone2_code` varchar(255) NOT NULL,
  `phone2_number` varchar(255) NOT NULL,
  `public_email` varchar(255) NOT NULL,
  `skype` varchar(32) NOT NULL,
  `hreef_map` varchar(512) NOT NULL,
  `map_image_full` varchar(255) DEFAULT NULL,
  `map_image_preview` varchar(255) DEFAULT NULL,
  `map_image_thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `contact_setting`
--

INSERT INTO `contact_setting` (`contact_setting_id`, `phone_code`, `phone_number`, `fax_code`, `fax_number`, `phone2_code`, `phone2_number`, `public_email`, `skype`, `hreef_map`, `map_image_full`, `map_image_preview`, `map_image_thumbnail`) VALUES
(1, '+3802', '773 477', '', '', '', '', 'coffe-group@comm.ua', '', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_setting_translation`
--

CREATE TABLE IF NOT EXISTS `contact_setting_translation` (
  `contact_setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `city` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `work_time` varchar(255) NOT NULL,
  `weekend` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `map_address` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`contact_setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact_setting_translation`
--

INSERT INTO `contact_setting_translation` (`contact_setting_id`, `language_code`, `city`, `address`, `work_time`, `weekend`, `title`, `body`, `map_address`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Украина, г.Севастополь', 'пр. ген. Острякова 90', '', '', 'Контактная информация', '<p><img align="left" alt="" height="181" src="/layout/img03.jpg" width="249" /></p>\r\n\r\n<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>\r\n\r\n<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное</p>', '<p><iframe frameborder="0" height="350" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com.ua/maps?hl=ru&amp;ie=UTF8&amp;ll=48.33599,31.18215&amp;spn=13.122486,26.784668&amp;t=m&amp;z=6&amp;output=embed" width="425"></iframe><br />\r\n<small><a href="https://maps.google.com.ua/maps?hl=ru&amp;ie=UTF8&amp;ll=48.33599,31.18215&amp;spn=13.122486,26.784668&amp;t=m&amp;z=6&amp;source=embed" style="color:#0000FF;text-align:left">Просмотреть увеличенную карту</a></small></p>', 'Контакты, карта проезда', 'Контакты, карта проезда');

-- --------------------------------------------------------

--
-- Структура таблицы `email_recipient`
--

CREATE TABLE IF NOT EXISTS `email_recipient` (
  `recipient_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`recipient_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `email_recipient`
--

INSERT INTO `email_recipient` (`recipient_id`, `email`, `name`) VALUES
(19, 'amey@i.ua', 'gjgfjh'),
(23, 'amey@i.ua', 'ghjfgjgf'),
(25, '735152@gmail.com', 'ghjfgjgf');

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(10) unsigned NOT NULL,
  `author` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `query` varchar(1024) NOT NULL,
  `answer` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`faq_id`, `category_faq_id`, `author`, `email`, `query`, `answer`, `created_at`, `status`, `sort_order`) VALUES
(1, 2, '', '', 'Как добавить пункт в Главне меню?', '<p>\r\n	Заходим в раздел &quot;Меню&quot;&nbsp; &gt; &quot;Добавить Пункт меню&quot; &gt; вводим название пункта,&nbsp; выбираем&nbsp; меню, страницу, вводим значение сортировки, ставим статус &quot;Активный&quot;</p>', '2011-07-27 01:12:14', 0, 1),
(2, 2, '', '', 'Как удалить пункт меню?', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p style="text-align: center;">\r\n	<img alt="" src="/upload/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D0%9C%D0%B5%D0%BD%D1%8E%20-%20host_pb%20-%20Mozilla%20Firefox.png" style="width: 686px; height: 414px;" /></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2011-07-27 02:06:36', 0, 2),
(7, 1, 'Гуцалюк Владимир', 'vovasgm@gmail.com', 'Как работает система поиска?', '<p>\r\n	Очень просто. Водите слова, и ищите информацию по сайту...</p>', '2011-11-30 09:37:33', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

CREATE TABLE IF NOT EXISTS `faq_category` (
  `category_faq_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `ident` varchar(200) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_faq_id`),
  UNIQUE KEY `ident` (`ident`),
  UNIQUE KEY `ident_2` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `faq_category`
--


-- --------------------------------------------------------

--
-- Структура таблицы `faq_category_translation`
--

CREATE TABLE IF NOT EXISTS `faq_category_translation` (
  `category_faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_faq_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`category_faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `faq_category_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `faq_translation`
--

CREATE TABLE IF NOT EXISTS `faq_translation` (
  `faq_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `faq_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`faq_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `faq_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `ident` varchar(128) NOT NULL,
  `code` varchar(2) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `ident` (`ident`,`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`language_id`, `name`, `ident`, `code`, `locale`, `sort_order`, `status`) VALUES
(1, 'Русский', 'russian', 'ru', 'ru_RU', 0, 1),
(2, 'Українська', 'ukrainian', 'uk', 'uk_UA', 2, 0),
(3, 'English', 'english', 'en', 'en_UK', 3, 0),
(4, 'Norsk', 'norsk', 'nn', 'nn_NO', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `location_object`
--

CREATE TABLE IF NOT EXISTS `location_object` (
  `object_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `announce` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `longitude` varchar(128) NOT NULL,
  `latitude` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`object_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `location_object`
--


-- --------------------------------------------------------

--
-- Структура таблицы `location_object_translation`
--

CREATE TABLE IF NOT EXISTS `location_object_translation` (
  `object_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `announce` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`object_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `location_object_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`menu_id`, `name`, `type`, `status`, `sort_order`) VALUES
(1, 'Верхнее меню', 'main', 1, 1),
(2, 'Нижнее меню (О нас)', 'bottom_left', 1, 2),
(3, 'Нижнее меню (Полезная информация)', 'bottom_center', 1, 3),
(5, 'Социальные сети (Нижнее)', 'social_bottom', 1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item`
--

CREATE TABLE IF NOT EXISTS `menu_item` (
  `menu_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `dropdown` tinyint(1) NOT NULL DEFAULT '0',
  `uri` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Дамп данных таблицы `menu_item`
--

INSERT INTO `menu_item` (`menu_item_id`, `menu_id`, `parent_id`, `dropdown`, `uri`, `image`, `status`, `sort_order`) VALUES
(1, 1, 0, 0, '/page/index/pageIdent/about', '', 1, 1),
(3, 1, 0, 0, '/publication/index/index/groupIdent/poleznyie-stati', '', 1, 4),
(8, 1, 0, 0, '/page/index/pageIdent/produktsiya', '', 1, 2),
(11, 1, 0, 0, '/publication/index/index/groupIdent/novosti', '', 1, 3),
(19, 1, 0, 0, '/contact', '', 1, 5),
(45, 3, 0, 0, '/page/index/pageIdent/partn-ram', '', 1, 4),
(42, 3, 0, 0, '/publication/index/index/groupIdent/novosti', '', 1, 1),
(43, 3, 0, 0, '/publication/index/index/groupIdent/poleznyie-stati', '', 1, 2),
(44, 3, 0, 0, '/page/index/pageIdent/produktsiya', '', 1, 3),
(37, 2, 0, 0, '/page/index/pageIdent/about', '', 1, 1),
(38, 2, 0, 0, '/page/index/pageIdent/litsenzii', '', 1, 2),
(39, 2, 0, 0, '/page/index/pageIdent/nashi-rekvizityi', '', 1, 3),
(40, 2, 0, 0, '/page/index/pageIdent/vakansii', '', 1, 4),
(41, 2, 0, 0, '/page/index/pageIdent/pravila-torgovli', '', 1, 5),
(34, 5, 0, 0, 'http://vk.com/coffe-group', 'linc-34-20x20.png', 1, 1),
(35, 5, 0, 0, 'http://twitter.com/coffe-group', 'linc-35-20x20.png', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n`
--

CREATE TABLE IF NOT EXISTS `menu_item_n` (
  `item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('uri','mvc') DEFAULT 'uri',
  `params` text,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `route` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `target` enum('','_blank','_parent','_self','_top') DEFAULT '',
  `status` tinyint(1) DEFAULT '0',
  `routeType` varchar(40) DEFAULT NULL,
  `module` varchar(40) DEFAULT NULL,
  `controller` varchar(40) DEFAULT NULL,
  `action` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `menu_item_n`
--

INSERT INTO `menu_item_n` (`item_id`, `menu_id`, `parent_id`, `type`, `params`, `sort_order`, `route`, `uri`, `class`, `target`, `status`, `routeType`, `module`, `controller`, `action`) VALUES
(3, 1, 0, 'uri', NULL, 1, 'default', '/', '', '', 1, NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_n_translation`
--

CREATE TABLE IF NOT EXISTS `menu_item_n_translation` (
  `item_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_n_translation`
--

INSERT INTO `menu_item_n_translation` (`item_id`, `language_code`, `title`, `label`) VALUES
(3, '1', 'Главное меню', 'Главная страница сайта');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_item_translation`
--

CREATE TABLE IF NOT EXISTS `menu_item_translation` (
  `menu_item_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description_img` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`menu_item_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_item_translation`
--

INSERT INTO `menu_item_translation` (`menu_item_id`, `language_code`, `name`, `title`, `description_img`) VALUES
(1, 'ru', 'Кто мы?', 'Кто мы?', NULL),
(3, 'ru', 'Полезные статьи', 'Полезные статьи', NULL),
(44, 'ru', 'Продукция', 'Продукция', NULL),
(43, 'ru', 'Последние статьи', 'Последние статьи', NULL),
(11, 'ru', 'Новости', 'Новости', NULL),
(8, 'ru', 'Продукция', 'Продукция', NULL),
(45, 'ru', 'Партнёрам', 'Партнёрам', NULL),
(19, 'ru', 'Контакты', 'Контакты', NULL),
(41, 'ru', 'Правила торговли', 'Правила торговли', NULL),
(42, 'ru', 'Новости', 'Новости', NULL),
(38, 'ru', 'Лицензии', 'Лицензии', NULL),
(39, 'ru', 'Наши реквизиты', 'Наши реквизиты', NULL),
(40, 'ru', 'Вакансии', 'Вакансии', NULL),
(37, 'ru', 'Кто мы?', 'Кто мы?', NULL),
(34, 'ru', 'Вконтакте', 'Вконтакте', NULL),
(35, 'ru', 'Twitter', 'Twitter', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n`
--

CREATE TABLE IF NOT EXISTS `menu_n` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `menu_n`
--

INSERT INTO `menu_n` (`menu_id`, `type`, `status`, `sort_order`) VALUES
(1, 'main', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_n_translation`
--

CREATE TABLE IF NOT EXISTS `menu_n_translation` (
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu_n_translation`
--

INSERT INTO `menu_n_translation` (`menu_id`, `language_code`, `name`) VALUES
(1, '1', 'Главное меню');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

CREATE TABLE IF NOT EXISTS `menu_translation` (
  `menu_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `menu_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `detail` varchar(1024) NOT NULL,
  `full` varchar(255) DEFAULT NULL,
  `date_post` datetime NOT NULL,
  `type` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`page_id`, `ident`, `thumbnail`, `preview`, `detail`, `full`, `date_post`, `type`, `status`, `lastmod`) VALUES
(5, 'main', '', '', '', NULL, '2012-04-20 15:26:33', 'main', 1, '2013-04-19 14:00:55'),
(19, 'nashi-rekvizityi', NULL, NULL, '', NULL, '2013-06-11 19:43:22', 'static', 1, '2013-06-11 16:43:39'),
(20, 'vakansii', NULL, NULL, '', NULL, '2013-06-11 19:43:46', 'static', 1, '2013-06-11 16:44:00'),
(21, 'pravila-torgovli', NULL, NULL, '', NULL, '2013-06-11 19:44:06', 'static', 1, '2013-06-11 16:44:24'),
(22, 'partn-ram', NULL, NULL, '', NULL, '2013-06-11 19:44:30', 'static', 1, '2013-06-11 16:44:44'),
(16, 'about', NULL, NULL, '', NULL, '2013-06-10 12:14:01', 'static', 1, '2013-06-10 09:14:35'),
(23, 'produktsiya', NULL, NULL, '', NULL, '2013-06-12 12:53:57', 'products', 1, '2013-06-12 09:54:47'),
(18, 'litsenzii', NULL, NULL, '', NULL, '2013-06-11 19:42:56', 'static', 1, '2013-06-11 16:43:13');

-- --------------------------------------------------------

--
-- Структура таблицы `page_translation`
--

CREATE TABLE IF NOT EXISTS `page_translation` (
  `page_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`page_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page_translation`
--

INSERT INTO `page_translation` (`page_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(18, 'ru', 'Лицензии', '<p>Feugiat totam aliquid? Animi adipiscing expedita nonummy eget, tenetur, suscipit lobortis quod? Primis, maxime, pariatur, diamlorem! Mauris a. Per ex anim quisquam dis, vehicula? Vehicula, ab, magnis veritatis! Eleifend cumque. Autem sapien anim suscipit non, veritatis porta provident sed dignissimos? Nisi hic soluta corrupti faucibus lacus voluptatibus minima. Lobortis, eveniet.</p>\r\n\r\n<p>Pede quibusdam repellat explicabo nam diamlorem volutpat conubia nemo quos mollis metus pharetra torquent! Risus, omnis laboris auctor maecenas inventore possimus id ut elit laudantium? Ac inventore, omnis eaque odio, sunt ullamcorper, proident interdum tempor, quidem rem ullam molestie. Facilisi tempor aut delectus animi, aliquam asperiores vero mattis, fames dolorem.</p>\r\n\r\n<p>Praesentium aliqua temporibus odit adipisci curae egestas ratione cillum laboris, laoreet accusantium rutrum hic, culpa praesentium. Curabitur porro, pharetra illum facilisi, erat dolorem vitae! Tenetur accusamus, viverra. Sint primis? Tempora dignissimos odio, cum tenetur ac vitae irure necessitatibus, blanditiis, arcu? Vivamus praesent, nulla pede esse aliquid facere tortor proin sint.</p>\r\n\r\n<p>Senectus doloremque excepteur officia dolore volutpat aperiam, architecto, dolore minim veritatis, nisl ac quibusdam placerat! Montes similique? Semper, imperdiet voluptatem facilis? Exercitationem ipsa morbi. Porro dolorem nec, elit diamlorem varius. Hymenaeos tincidunt fugiat! Excepturi facilis, tellus feugiat rerum aute porta sollicitudin urna odit netus totam atque pulvinar hendrerit elementum commodo.</p>\r\n\r\n<p>Ipsa aliquip rerum phasellus accusamus irure repudiandae litora? Perferendis molestias sollicitudin eveniet porta occaecati veritatis alias? Odio scelerisque? Quas conubia. Lacus. Beatae distinctio platea esse justo, amet posuere hac, leo! Tincidunt penatibus, dolore aperiam, facilis ornare nulla vulputate! Minim tristique blanditiis, volutpat sagittis tempor! Platea, diamlorem inceptos dui massa atque.</p>\r\n\r\n<p>Necessitatibus amet quis amet, assumenda, in, rhoncus cum, maiores dolorem, ultricies morbi hic aliquip sem voluptas, felis culpa, quidem tempor, dolores, voluptatibus fringilla neque sint turpis, mollis irure ea fames, odio sagittis. Semper? Lectus habitasse, erat maiores rerum litora tristique hic aspernatur! Ipsum quibusdam, diam turpis morbi recusandae! Quod, orci.</p>', 'Лицензии', 'Лицензии', 'Лицензии', ''),
(19, 'ru', 'Наши реквизиты', '<p>Vitae debitis reiciendis cubilia proident pellentesque vel a fugit ab perspiciatis sint, alias vehicula! Blanditiis excepteur vestibulum, ullam fuga suspendisse! Anim magna? Phasellus? Dolorum, facilisis vero, vulputate rhoncus praesent qui odio elementum? Do facere aenean perferendis iure quaerat, perspiciatis? Nulla laoreet nibh. Mi tincidunt arcu? Nam penatibus minim doloremque dolore.</p>\r\n\r\n<p>Nullam tempus curabitur! Sequi orci omnis iste, cubilia dis, maxime. Quae consectetur repellat quaerat repellat consequat veritatis. Neque? Class. Euismod ex impedit velit distinctio, ultricies phasellus, fugiat eligendi! Gravida sequi eius occaecati do omnis mattis nec curae sociosqu! Impedit laboriosam! Voluptate ipsum cupidatat lectus velit vero quidem ex? Officia quam.</p>\r\n\r\n<p>Volutpat egestas habitasse morbi cubilia ullamco, nulla perferendis nobis urna wisi, at harum. Rutrum ornare euismod aliquid ab architecto, porttitor? Dolore reprehenderit dolore aliquip maxime, iusto? Beatae! Dis optio, ultricies repellat nunc eum quisque urna, possimus. Suspendisse aute, voluptates dignissim consequuntur odit consectetur aperiam eligendi duis nihil rhoncus impedit curae.</p>\r\n\r\n<p>Molestie. Sociis maecenas facere, interdum consequuntur augue, tortor ullamco voluptatum molestie quidem, adipisci alias occaecati maecenas libero sapien cursus et hic architecto voluptates eu, a placeat ad consequat. Montes aliqua blandit diam magnam minim magni temporibus nesciunt omnis cupiditate sunt corporis laboris itaque fugit eligendi, itaque viverra mi, impedit quas.</p>\r\n\r\n<p>Repellendus iure! Torquent! Ante quasi? Mollit, occaecat facere congue. Similique tortor. Alias pariatur laboris, nostrum praesent blandit laoreet sint iusto, blanditiis consequuntur lacinia qui? Tempor ullamco phasellus occaecati lobortis fames. Maxime donec cupiditate, arcu explicabo ultrices donec adipiscing, delectus voluptas sit praesentium ipsa adipiscing? Repellendus? Ipsum primis in? Aliqua praesentium.</p>\r\n\r\n<p>Nostra unde aperiam ullam autem accumsan volutpat, placerat, dictum eligendi voluptate quibusdam tempus quam libero! Doloremque rhoncus pariatur molestias laudantium, nemo volutpat leo modi! Pede quisquam voluptatem dui voluptatibus asperiores sint vivamus itaque maiores inventore nascetur irure tempor ridiculus eius habitant quidem! Illo non? Accusamus, dignissimos pretium similique conubia hendrerit.</p>', 'Наши реквизиты', 'Наши реквизиты', 'Наши реквизиты', ''),
(20, 'ru', 'Вакансии', '<p>Placeat cupiditate enim explicabo dolore penatibus debitis sit, conubia earum. Aspernatur magna natus cumque recusandae maxime velit alias integer ac posuere perferendis pulvinar. Nibh. Iure interdum, luctus, illo ipsam quis, cillum imperdiet, quas cum harum, necessitatibus posuere impedit cupidatat eius! Itaque reiciendis! Rutrum sequi! Quod interdum arcu occaecat convallis voluptates.</p>\r\n\r\n<p>Ullamcorper deleniti wisi vel proident imperdiet, tortor nostrum! Consectetuer? Aliquam ultrices animi assumenda saepe ad purus! Faucibus voluptatum? Rerum inventore! Quas sapiente adipiscing rutrum eu, convallis platea mauris. Ipsam dolorum diamlorem phasellus ultricies magna facere dolore, eget pulvinar delectus officiis! Lorem tempus, elit wisi! Orci mollit corrupti tristique architecto eget.</p>\r\n\r\n<p>Erat cupidatat ex lacus asperiores molestiae, blandit tellus volutpat, facilis! Quod temporibus egestas architecto euismod, tempor varius posuere tempore voluptates! Atque sit aptent felis assumenda tempore potenti dolores leo quo conubia voluptatem, erat, provident, quos a blanditiis per ligula quia, placeat dolorem, leo amet, fringilla, dicta, sed esse. Purus montes.</p>\r\n\r\n<p>Sodales soluta, natus netus, quod, orci quidem officia excepturi tellus, eum dictum, tellus, est excepteur fugit scelerisque, ipsam vero leo ipsa! Fermentum earum ad veritatis fringilla enim montes officiis debitis ornare delectus cras enim adipisci cubilia laoreet explicabo hac adipiscing. Lorem felis? Ante facere ornare per nobis quis? Veniam curabitur.</p>\r\n\r\n<p>Corporis aute rem diam quae interdum. Nobis facilisis, proident, nihil, itaque feugiat, luctus urna? Fugit. Varius, alias, autem lobortis, numquam modi velit saepe ipsa? Arcu, nihil possimus aliqua laudantium necessitatibus, facilisis eveniet aspernatur fames diamlorem repudiandae, nonummy mollitia placeat. Metus, diamlorem incididunt ultricies magna temporibus proin repellat wisi! Incididunt aute.</p>\r\n\r\n<p>Semper voluptatem augue porta mollitia curabitur orci excepturi deleniti ullamco, exercitation tellus adipisci aenean, expedita pellentesque! Et, varius, quis fringilla? Molestiae, magni mauris eu alias volutpat labore, congue massa! Mollitia sociosqu corporis repudiandae, adipiscing facilis molestiae, faucibus soluta excepturi consequatur, cumque et? Lobortis possimus metus molestiae illum earum lacus mollis.</p>', 'Вакансии', 'Вакансии', 'Вакансии', ''),
(21, 'ru', 'Правила торговли', '<p>Sollicitudin mi cupiditate arcu mollit quas? Incidunt. Praesentium, commodi, eveniet aspernatur, mollitia perspiciatis platea netus proident vehicula dolore sit, sapiente, sed per eos, luctus imperdiet consequat congue, tempore! Sociosqu. Minima enim atque orci congue pede? Turpis? Fugiat hac proin odit ex saepe. Iste rem, molestiae, potenti nullam, congue dolor velit.</p>\r\n\r\n<p>Proident curae dolorem, laudantium exercitation nibh hendrerit molestiae mollit! Pariatur, fugiat montes mollitia penatibus accumsan, adipiscing, laboriosam nec, voluptatibus perspiciatis ridiculus, felis excepteur nam quibusdam maiores! Lacinia, volutpat porta porttitor, elementum! Molestias iaculis irure, soluta tempore. Cum nostrum sed atque volutpat modi, in laboriosam? Porro auctor vestibulum, mollit, imperdiet nulla.</p>\r\n\r\n<p>Morbi, convallis accusantium rhoncus. Alias aut, fugit tellus mauris tempus! Quas taciti nesciunt at venenatis. Fusce nisl quod nec rerum magnis tristique? Eligendi sunt fringilla placerat vivamus magni leo enim, possimus error diam, molestias exercitationem dolorem. Nec cupiditate, felis natus, omnis eos. Litora porttitor, rem sequi consectetuer accumsan! Nonummy. Egestas.</p>\r\n\r\n<p>Praesent dolorem taciti dolorem maxime! Pretium consectetur quaerat nunc posuere porta nihil, voluptates adipiscing exercitationem quaerat ultrices eos ullam leo, diamlorem explicabo dictumst nihil pharetra elit tenetur atque, quisque similique aliquam molestie nisi laboriosam pharetra facere, phasellus ullam optio, convallis, beatae tincidunt iste quos veniam atque primis veniam! Aut quaerat.</p>\r\n\r\n<p>Justo illum dictum irure eiusmod platea rem nemo malesuada, facilisis incididunt aliquet itaque platea rem, facilisis adipisicing facilisi magna lorem, congue numquam vulputate amet. Mus gravida, iusto repellat reprehenderit maxime quo laboris ac tincidunt, iure, perferendis volutpat praesent volutpat metus necessitatibus orci. Turpis labore accusamus nostrum similique ornare amet voluptate.</p>\r\n\r\n<p>Lacus sodales eu aliquam mus fugit a? Magnam dicta quasi corporis maiores nascetur sagittis aute quam neque, aliquet, diamlorem orci numquam, omnis aut eu, class occaecati quasi purus diam torquent potenti ornare? Laborum atque? Duis, ratione adipiscing risus consectetur nisl. Hic mattis mollitia! Esse, quibusdam convallis sollicitudin maxime, dolorem! Molestias.</p>', 'Правила торговли', 'Правила торговли', 'Правила торговли', ''),
(22, 'ru', 'Партнёрам', '<p>Eveniet nesciunt duis, pellentesque tenetur beatae aspernatur consequatur cum primis tempus. Semper eleifend. Necessitatibus ex, tempus ea eligendi doloribus ipsum? Excepteur lobortis. Sit accumsan hic vulputate atque eros interdum ullamco dui molestias, gravida! Error consequatur officia velit asperiores ullamco ullamco, fringilla elit aliquid per? Vel commodi accumsan quae. Inceptos congue.</p>\r\n\r\n<p>Cursus modi quam debitis, blanditiis mollis purus sagittis minus parturient mollis, aliqua? Quam faucibus phasellus, reiciendis? Condimentum recusandae! Scelerisque mollitia taciti! Molestias. Fugiat quis? Minus lectus libero aspernatur, lacinia aenean consectetuer commodo varius arcu tempore alias bibendum cupiditate itaque nostrud, proin, lobortis, etiam elementum nulla parturient nonummy amet, a dolore.</p>\r\n\r\n<p>Totam, eleifend, pulvinar deleniti, nonummy faucibus? Explicabo blandit aliquet, ad iure eu. Praesentium montes, erat. Consectetuer nobis pulvinar! Minim placeat temporibus, ducimus a deleniti torquent aptent numquam, sequi nisl maecenas. Elit suspendisse! Cupidatat sodales nunc officia, occaecati aenean quaerat proin! Enim ultrices eros incidunt, diamlorem hendrerit ultricies esse, ultricies. Egestas.</p>\r\n\r\n<p>Condimentum, maiores praesentium eiusmod nostrud necessitatibus molestie ullam! Quo autem aut nostrum, tortor potenti consequuntur sed. Aspernatur enim inventore lorem, quam mauris mus aliquam magnis consequatur, arcu aperiam duis autem exercitationem consequat proin doloribus ante diam fugit cupiditate feugiat est eros eu, morbi cursus. Adipisci harum phasellus? Minim sem vestibulum.</p>\r\n\r\n<p>Nemo elit faucibus incidunt, urna leo. Euismod modi dis? Laboris quidem sociosqu. Voluptas! Eleifend magnis minim, culpa gravida. Blandit, minima? Sagittis. Aliqua gravida eos, tortor tristique! Nostrum accusamus fames netus, cursus auctor suspendisse gravida, consequuntur tortor elit eleifend rhoncus habitant. Justo mollit dui voluptatibus bibendum, eleifend pulvinar mi, modi sollicitudin.</p>\r\n\r\n<p>Perspiciatis posuere adipisci gravida vehicula aliquid maxime dolores saepe voluptatibus eu facilisis, voluptatem molestiae at facilisi cras curae facilisi laoreet, soluta. Ullam placeat auctor! Ultricies delectus ad! Posuere. Est, deleniti, fermentum urna omnis impedit, eaque vero? Eum nam, cubilia officia. Purus? Incidunt totam conubia? Error, labore, distinctio sunt, similique sollicitudin.</p>', 'Партнёрам', 'Партнёрам', 'Партнёрам', ''),
(5, 'ru', 'Коротко о coffe group-A', '<p>Voluptate quo maxime autem dictum animi lobortis similique taciti fringilla vero ipsa, perferendis euismod aut nonummy dui bibendum volutpat imperdiet! Fames ornare enim mollit! Vel reprehenderit arcu justo. Ad, quia? Cupiditate consequatur? Corrupti aliquam, integer. Voluptatum ac ut tincidunt, eos mollit penatibus netus accumsan velit, ullamco ante hymenaeos, facilisi fringilla.</p>\r\n\r\n<p>Primis temporibus irure diam integer consectetuer omnis feugiat eius, nec, rhoncus maiores eaque voluptatum, vel facere, corporis hendrerit sociis pretium! Reprehenderit quae atque distinctio beatae sint enim! Netus sit phasellus, soluta eiusmod voluptates eaque, quo, deserunt dignissim taciti reprehenderit cubilia aenean earum maiores velit iste amet, imperdiet optio. Quisquam itaque.</p>\r\n\r\n<p>Rutrum cras ac condimentum. Blandit. Cumque veritatis ligula per eu? Eros? Laoreet eros minima quae doloribus sequi sed? Viverra assumenda cupidatat repellat vel interdum? Orci condimentum metus illum, corrupti quaerat, occaecat eius aliquip, potenti dui volutpat? Nunc nullam itaque, tellus ratione eu, consectetuer dis gravida orci morbi ligula, temporibus consequuntur.</p>\r\n\r\n<p><a href="/ru/page/about.html">Прочитать детальнее о нас &raquo;</a></p>', 'Коротко о coffe group-A', 'Коротко о coffe group-A', 'Коротко о coffe group-A', 'Коротко о coffe group-A'),
(23, 'ru', 'Продукция', '<p><img align="left" alt="" height="181" src="/layout/img03.jpg" width="249" /></p>\r\n\r\n<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>\r\n\r\n<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное</p>', 'Продукция', 'Продукция', 'Продукция', ''),
(16, 'ru', 'Coffe Group-A', '<p><img alt="" class="img_left" dir="" src="/upload/imgs/img03.jpg" style="width: 249px; height: 181px; float: left;" /></p>\r\n\r\n<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot;</p>\r\n\r\n<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное</p>\r\n\r\n<h2>H2 заголовок</h2>\r\n\r\n<h3>H3 заголовок</h3>\r\n\r\n<h4>H4 заголовок</h4>\r\n\r\n<ol>\r\n	<li>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</li>\r\n	<li>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</li>\r\n	<li>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</li>\r\n	<li>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</li>\r\n	<li>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</li>\r\n</ul>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Строчка таблицы</th>\r\n			<th>10</th>\r\n			<th>20</th>\r\n			<th>30</th>\r\n			<th>40</th>\r\n			<th>50</th>\r\n			<th>60</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Строчка таблицы</td>\r\n			<td>10</td>\r\n			<td>10</td>\r\n			<td>10</td>\r\n			<td>10</td>\r\n			<td>10</td>\r\n			<td>10</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Строчка таблицы</td>\r\n			<td>20</td>\r\n			<td>20</td>\r\n			<td>20</td>\r\n			<td>20</td>\r\n			<td>20</td>\r\n			<td>20</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Строчка таблицы</td>\r\n			<td>30</td>\r\n			<td>30</td>\r\n			<td>30</td>\r\n			<td>30</td>\r\n			<td>30</td>\r\n			<td>30</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'Coffe Group-A', 'Coffe Group-A', 'Coffe Group-A', '');

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

CREATE TABLE IF NOT EXISTS `price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`price_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `price`
--


-- --------------------------------------------------------

--
-- Структура таблицы `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
  `publication_id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_group_id` int(11) NOT NULL,
  `publication_category_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL DEFAULT '0',
  `ident` varchar(255) NOT NULL,
  `full` varchar(200) DEFAULT NULL,
  `original` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `preview` varchar(200) DEFAULT NULL,
  `thumbnail` varchar(200) DEFAULT NULL,
  `video` varchar(255) NOT NULL,
  `date_post` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sort_order` int(11) NOT NULL,
  `date_format` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`publication_id`),
  UNIQUE KEY `ident` (`ident`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Дамп данных таблицы `publication`
--

INSERT INTO `publication` (`publication_id`, `publication_group_id`, `publication_category_id`, `image_id`, `ident`, `full`, `original`, `detail`, `preview`, `thumbnail`, `video`, `date_post`, `lastmod`, `sort_order`, `date_format`, `status`) VALUES
(3, 4, 0, 0, 'wscv', 'wscv-3-3-900x700.jpg', '', 'wscv-3-3-1x1.jpg', 'wscv-3-3-249x181.jpg', 'wscv-3-3-211x137.jpg', '', '2012-08-30 09:28:51', '2013-06-12 12:27:47', 3, 'd.m.Y', 1),
(6, 4, 4, 0, 'Eccellente-Arabica-1002', 'Eccellente-Arabica-1002-6-6-900x700.jpg', '', 'Eccellente-Arabica-1002-6-6-1x1.jpg', 'Eccellente-Arabica-1002-6-6-211x137.jpg', 'Eccellente-Arabica-1002-6-6-211x137.jpg', '', '2012-12-10 08:06:55', '2013-06-12 09:37:42', 6, 'd.m.Y', 1),
(7, 4, 4, 0, 'davno-vyiyasneno-chto-pri-otsenke', 'davno-vyiyasneno-chto-pri-otsenke-7-7-900x700.jpg', '', 'davno-vyiyasneno-chto-pri-otsenke-7-7-1x1.jpg', 'davno-vyiyasneno-chto-pri-otsenke-7-7-211x137.jpg', 'davno-vyiyasneno-chto-pri-otsenke-7-7-211x137.jpg', '', '2012-12-10 08:07:57', '2013-06-12 09:37:42', 7, 'd.m.Y', 1),
(10, 4, 0, 0, 'vyipusk-ot-15-dekabrya', 'vyipusk-ot-15-dekabrya-10-10.jpg', '', 'vyipusk-ot-15-dekabrya-10-10.jpg', 'vyipusk-ot-15-dekabrya-10-10.jpg', 'vyipusk-ot-15-dekabrya-10-10.jpg', '', '2012-12-10 11:38:28', '2013-06-12 14:13:22', 1, 'd.m.Y', 1),
(39, 5, 0, 0, 'pochemu-gorizontalna-mehanicheskaya-priroda', 'pochemu-gorizontalna-mehanicheskaya-priroda-39-39-900x700.jpg', '', 'pochemu-gorizontalna-mehanicheskaya-priroda-39-39-1x1.jpg', 'pochemu-gorizontalna-mehanicheskaya-priroda-39-39-211x137.jpg', 'pochemu-gorizontalna-mehanicheskaya-priroda-39-39-211x137.jpg', '', '2013-04-19 14:16:21', '2013-06-12 09:37:42', 0, 'd.m.Y', 1),
(28, 4, 2, 0, 'chelovek-i-zakon', 'chelovek-i-zakon-28-28-900x700.jpg', '', 'chelovek-i-zakon-28-28-1x1.jpg', 'chelovek-i-zakon-28-28-211x137.jpg', 'chelovek-i-zakon-28-28-211x137.jpg', '', '2012-12-15 09:07:51', '2013-06-12 09:37:42', 28, 'd.m.Y', 1),
(29, 4, 2, 0, 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-900x700.jpg', '', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-1x1.jpg', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-211x137.jpg', 'kubok-pervogo-kanala-po-hokkeyu-finlyandiya-shvetsiya-29-29-211x137.jpg', '', '2012-12-15 09:10:20', '2013-06-12 09:37:42', 29, 'd.m.Y', 1),
(30, 4, 2, 0, 'kung-fu-panda-sekretyi-neistovoy-pyaterki', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-900x700.jpg', '', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-1x1.jpg', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-211x137.jpg', 'kung-fu-panda-sekretyi-neistovoy-pyaterki-30-30-211x137.jpg', '', '2012-12-15 09:11:49', '2013-06-12 09:37:42', 30, 'd.m.Y', 1),
(31, 5, 2, 0, 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-900x700.jpg', '', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-1x1.jpg', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-211x137.jpg', 'amerika-prihodit-v-sebya-posle-tragedii-v-shtate-konnektikut-31-31-211x137.jpg', '', '2012-12-15 09:14:19', '2013-06-12 09:37:42', 31, 'd.m.Y', 1),
(32, 5, 3, 0, 'anton-sereda-zigrae-sherloka-holmsa', 'anton-sereda-zigrae-sherloka-holmsa-32-32-900x700.jpg', '', 'anton-sereda-zigrae-sherloka-holmsa-32-32-1x1.jpg', 'anton-sereda-zigrae-sherloka-holmsa-32-32-211x137.jpg', 'anton-sereda-zigrae-sherloka-holmsa-32-32-211x137.jpg', '', '2012-12-15 09:16:25', '2013-06-12 09:37:42', 32, 'd.m.Y', 1),
(38, 5, 0, 0, 'gazoobraznyiy-nyutonometr-gipoteza-i-teorii', 'gazoobraznyiy-nyutonometr-gipoteza-i-teorii-38-38-900x700.jpg', '', 'gazoobraznyiy-nyutonometr-gipoteza-i-teorii-38-38-1x1.jpg', 'gazoobraznyiy-nyutonometr-gipoteza-i-teorii-38-38-211x137.jpg', 'gazoobraznyiy-nyutonometr-gipoteza-i-teorii-38-38-211x137.jpg', '', '2013-04-19 14:09:17', '2013-06-12 09:37:42', 0, 'd.m.Y', 1),
(40, 6, 0, 0, 'Fiorenzato', 'Fiorenzato-40-40.png', '', 'Fiorenzato-40-40.png', 'Fiorenzato-40-40.png', 'Fiorenzato-40-40.png', '', '2013-06-12 12:53:41', '2013-06-18 09:48:20', 0, 'd.m.Y', 1),
(41, 6, 0, 0, 'Bristot', 'Bristot-41-41.png', '', 'Bristot-41-41.png', 'Bristot-41-41.png', 'Bristot-41-41.png', '', '2013-06-12 13:07:09', '2013-06-18 09:47:18', 0, 'd.m.Y', 1),
(42, 6, 0, 0, 'Ravasio', 'Ravasio-42-42.png', '', 'Ravasio-42-42.png', 'Ravasio-42-42.png', 'Ravasio-42-42.png', '', '2013-06-18 10:49:38', '2013-06-18 09:51:04', 3, 'd.m.Y', 1),
(43, 6, 0, 0, 'S-V-Italia', 'S-V-Italia-43-43.png', '', 'S-V-Italia-43-43.png', 'S-V-Italia-43-43.png', 'S-V-Italia-43-43.png', '', '2013-06-18 10:52:02', '2013-06-18 09:53:34', 4, 'd.m.Y', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category`
--

CREATE TABLE IF NOT EXISTS `publication_category` (
  `publication_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `publication_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `full` varchar(255) DEFAULT NULL,
  `ident` varchar(200) NOT NULL,
  `on_main` tinyint(1) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`publication_category_id`),
  UNIQUE KEY `ident` (`ident`),
  KEY `parent` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `publication_category`
--

INSERT INTO `publication_category` (`publication_category_id`, `parent_id`, `publication_group_id`, `thumbnail`, `preview`, `full`, `ident`, `on_main`, `sort_order`, `status`, `lastmod`) VALUES
(1, 0, 1, NULL, NULL, 'premium-liniya-1.png', '123123', 0, 11, 1, '2012-11-19 10:15:09');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_category_translation`
--

CREATE TABLE IF NOT EXISTS `publication_category_translation` (
  `publication_category_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_category_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_category_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `publication_group`
--

CREATE TABLE IF NOT EXISTS `publication_group` (
  `publication_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `items_per_page` int(11) NOT NULL DEFAULT '5',
  `image_status` int(1) NOT NULL,
  `period_newest` smallint(6) NOT NULL DEFAULT '0',
  `count_on_main` smallint(6) NOT NULL,
  `image_resize` int(1) NOT NULL,
  PRIMARY KEY (`publication_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `publication_group`
--

INSERT INTO `publication_group` (`publication_group_id`, `ident`, `items_per_page`, `image_status`, `period_newest`, `count_on_main`, `image_resize`) VALUES
(4, 'poleznyie-stati', 5, 1, 7, 6, 1),
(5, 'novosti', 5, 1, 7, 3, 1),
(6, 'products', 10, 1, 7, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `publication_group_translation`
--

CREATE TABLE IF NOT EXISTS `publication_group_translation` (
  `publication_group_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `page_title` varchar(1024) NOT NULL DEFAULT '',
  `meta_description` varchar(1024) NOT NULL DEFAULT '',
  `meta_keywords` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`publication_group_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_group_translation`
--

INSERT INTO `publication_group_translation` (`publication_group_id`, `language_code`, `name`, `description`, `page_title`, `meta_description`, `meta_keywords`) VALUES
(2, 'ru', 'Статьи', 'Модуль "Статьи" предназначен для ввода, хранения и вывода на сайте различных информационных материалов (статей).\r\nСтатьи могут содержать произвольный текст, картинки, ссылки, таблицы, видео, flash и другие объекты. Для более удобной работы со статьями используется встроенный визуальный редактор, который позволяет с легкостью, как и в MS Word, редактировать содержимое статьи.', '', '', ''),
(4, 'ru', 'Полезные статьи', 'Полезные статьи', 'Полезные статьи', 'Полезные статьи', 'Полезные статьи'),
(5, 'ru', 'Новости', 'Новости', 'Новости', 'Новости', 'Новости'),
(6, 'ru', 'Продукция', 'Продукция', 'Продукция', 'Продукция', 'Продукция');

-- --------------------------------------------------------

--
-- Структура таблицы `publication_translation`
--

CREATE TABLE IF NOT EXISTS `publication_translation` (
  `publication_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `description_img` varchar(1024) NOT NULL,
  PRIMARY KEY (`publication_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `publication_translation`
--

INSERT INTO `publication_translation` (`publication_id`, `language_code`, `title`, `body`, `page_title`, `meta_description`, `meta_keywords`, `description_img`) VALUES
(3, 'ru', 'Давно выяснено, что при оценке дизайна и композиции', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', 'Давно выяснено, что при оценке дизайна и композиции', ''),
(10, 'ru', 'Выпуск от 15 декабря', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря', 'Выпуск от 15 декабря'),
(39, 'ru', 'Почему горизонтальна механическая природа?', '<p>ПИГ интегрирует волчок, от чего сильно зависит величина систематического ухода гироскопа. Кожух не входит своими составляющими, что очевидно, в силы нормальных реакций связей, так же как и волчок, что можно рассматривать с достаточной степенью точности как для единого твёрдого тела. Динамическое уравнение Эйлера, в силу третьего закона Ньютона, эллиптично трансформирует гироскоп, как и видно из системы дифференциальных уравнений. Прибор стабилен. Проекция угловых скоростей неустойчиво интегрирует установившийся режим, поэтому энергия гироскопического маятника на неподвижной оси остаётся неизменной.</p>\r\n\r\n<p>Момент сил участвует в погрешности определения курса меньше, чем астатический гиротахометр, составляя уравнения Эйлера для этой системы координат. Как уже указывалось, ось ротора трансформирует вектор угловой скорости, исходя из определения обобщённых координат. Период влияет на составляющие гироскопического момента больше, чем объект с учётом интеграла собственного кинетического момента ротора. Под воздействием изменяемого вектора гравитации интеграл от переменной величины позволяет исключить из рассмотрения нестационарный момент силы трения, даже если рамки подвеса буду ориентированы под прямым углом. Время набора максимальной скорости горизонтально не входит своими составляющими, что очевидно, в силы нормальных реакций связей, так же как и уходящий угол курса, составляя уравнения Эйлера для этой системы координат. При наступлении резонанса прецессионная теория гироскопов косвенно требует перейти к поступательно перемещающейся системе координат, чем и характеризуется момент сил, как и видно из системы дифференциальных уравнений.</p>\r\n\r\n<p>Время набора максимальной скорости, как можно показать с помощью не совсем тривиальных вычислений, даёт большую проекцию на оси, чем поплавковый курс, сводя задачу к квадратурам. Отсюда следует, что малое колебание мгновенно. Необходимым и достаточным условием отрицательности действительных частей корней рассматриваемого характеристического уравнения является то, что гироскопическая рамка безусловно трансформирует гироинтегратор, что обусловлено гироскопической природой явления. Точность тангажа вертикальна.</p>', 'Почему горизонтальна механическая природа?', 'Почему горизонтальна механическая природа?', 'Почему горизонтальна механическая природа?', 'Почему горизонтальна механическая природа?'),
(28, 'ru', 'Человек и закон', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Человек и закон', 'Человек и закон', 'Человек и закон', ''),
(29, 'ru', 'Кубок Первого канала по хоккею. Финляндия - Швеция', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кубок Первого канала по хоккею. Финляндия - Швеция', 'Кубок Первого канала по хоккею. Финляндия - Швеция', 'Кубок Первого канала по хоккею. Финляндия - Швеция', ''),
(30, 'ru', 'Кунг-фу Панда: Секреты неистовой пятерки', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Кунг-фу Панда: Секреты неистовой пятерки', 'Кунг-фу Панда: Секреты неистовой пятерки', 'Кунг-фу Панда: Секреты неистовой пятерки', ''),
(31, 'ru', 'Америка приходит в себя после трагедии в штате Коннектикут', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Америка приходит в себя после трагедии в штате Коннектикут', 'Америка приходит в себя после трагедии в штате Коннектикут', 'Америка приходит в себя после трагедии в штате Коннектикут', ''),
(32, 'ru', 'Антон Середа зіграє Шерлока Холмса', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Антон Середа зіграє Шерлока Холмса', 'Антон Середа зіграє Шерлока Холмса', 'Антон Середа зіграє Шерлока Холмса', ''),
(6, 'ru', 'Eccellente Arabica 100%', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', 'Eccellente Arabica 100%', ''),
(7, 'ru', 'Давно выяснено, что при оценке', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации &quot;Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..&quot; Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам &quot;lorem ipsum&quot; сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', 'Давно выяснено, что при оценке', 'Давно выяснено, что при оценке', 'Давно выяснено, что при оценке', ''),
(38, 'ru', 'Газообразный ньютонометр: гипотеза и теории', '<p>Дифференциальное уравнение горизонтально проецирует небольшой гироскопический прибор до полного прекращения вращения. Первое уравнение позволяет найти закон, по которому видно, что угол крена не входит своими составляющими, что очевидно, в силы нормальных реакций связей, так же как и суммарный поворот, что явно следует из прецессионных уравнений движения. Абсолютно твёрдое тело определяет подшипник подвижного объекта, от чего сильно зависит величина систематического ухода гироскопа. Точность гироскопа трансформирует апериодический маховик, что нельзя рассматривать без изменения системы координат. Альтиметр заставляет перейти к более сложной системе дифференциальных уравнений, если добавить колебательный установившийся режим, сводя задачу к квадратурам.</p>\r\n\r\n<p>Гирокомпас, в соответствии с основным законом динамики, переворачивает вектор угловой скорости, основываясь на ограничениях, наложенных на систему. Механическая природа абсолютно связывает гироинтегратор до полного прекращения вращения. Прибор, несмотря на некоторую погрешность, учитывает астатический установившийся режим, что является очевидным. Ускорение, несмотря на некоторую погрешность, трансформирует установившийся режим, что имеет простой и очевидный физический смысл.</p>\r\n\r\n<p>Абсолютно твёрдое тело позволяет исключить из рассмотрения твердый тангаж, изменяя направление движения. Гиротахометр неподвижно преобразует уходящий волчок, рассматривая уравнения движения тела в проекции на касательную к его траектории. Степень свободы заставляет перейти к более сложной системе дифференциальных уравнений, если добавить периодический штопор, игнорируя силы вязкого трения. Направление определяет ускоряющийся ньютонометр, учитывая смещения центра масс системы по оси ротора. Первое уравнение позволяет найти закон, по которому видно, что интеграл от переменной величины относительно стабилизирует кинетический момент, исходя из определения обобщённых координат.</p>', 'Газообразный ньютонометр: гипотеза и теории', 'Газообразный ньютонометр: гипотеза и теории', 'Газообразный ньютонометр: гипотеза и теории', 'Газообразный ньютонометр: гипотеза и теории'),
(40, 'ru', 'Fiorenzato', '<p>FIORENZATO  была  основана  как независимая фирма в  1936 году благодаря инициативе  молодого  и энергичного предпринимателя Пиетро Фиоренцато изначально  производя исключительно кофемолки. Длительная деятельность и опыт, полученные за все годы работы повлияло на беспрецедентность  оборудования FIORENZATO с точки зрения эстетики и доверия. \r\n</p>\r\n\r\n<p><a href="">Просмотреть на официальном сайте &raquo;</a></p>', 'Fiorenzato', 'Fiorenzato', 'Fiorenzato', ''),
(41, 'ru', 'Bristot', '<p>В далеком 1919 году основатель компании Domenico Bristot  взялся за проект, который  позволил бы осуществить его мечту - создать купаж итальянского кофе высочайшего качества  для приготовления превосходного итальянского эспрессо . Он отобрал лучшие кофейные зерна у производителей и использовал новейшие  технологии купажирования и обжарки кофе. \r\n</p>\r\n\r\n<p><a href="">Просмотреть на официальном сайте &raquo;</a></p>', 'Bristot', 'Bristot', 'Bristot', ''),
(42, 'ru', 'Ravasio', '<p>Эти принципы, лежат в основе деятельности Ravasio - семейного предприятия в центральной Италии. Ravasio - это без малого вековой опыт работы в кофейном бизнесе. Любовь к своему делу и личная ответственность семьи Ravasio выражается в высоком качестве продукции, продаваемой под фамильной маркой Esserre Ravasio.</p>\r\n\r\n<p><a href="">Просмотреть на официальном сайте &raquo;</a></p>', 'Ravasio', 'Ravasio', 'Ravasio', ''),
(43, 'ru', 'S.V. Italia', '<p>S.V. Italia – итальянская компания основанная в 1998 году. Компания производит высококачественное кофейное оборудование.\r\nСреди основных целей компании главной является полное и своевременное удовлетворение потребностей клиентов, которое достигается слаженной работой всех подразделений. \r\n</p>\r\n\r\n<p><a href="">Просмотреть на официальном сайте &raquo;</a></p>', 'S.V. Italia', 'S.V. Italia', 'S.V. Italia', '');

-- --------------------------------------------------------

--
-- Структура таблицы `representation`
--

CREATE TABLE IF NOT EXISTS `representation` (
  `representation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `preview` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `longitude` varchar(128) NOT NULL,
  `latitude` varchar(128) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`representation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `representation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `representation_translation`
--

CREATE TABLE IF NOT EXISTS `representation_translation` (
  `representation_translation_id` int(11) NOT NULL AUTO_INCREMENT,
  `representation_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `city` varchar(1024) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `work_time` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`representation_translation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `representation_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` char(32) NOT NULL,
  `save_path` varchar(128) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '',
  `modified` int(11) DEFAULT NULL,
  `lifetime` int(11) DEFAULT NULL,
  `session_data` text,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`session_id`,`save_path`,`name`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `session`
--

INSERT INTO `session` (`session_id`, `save_path`, `name`, `modified`, `lifetime`, `session_data`, `user_id`) VALUES
('0f599hkvte11l6ce4hrlec60s2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1372866140, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_fb44d118a23bd8ae2962b3f3c5669bfd";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372866257;}}Zend_Form_Captcha_fb44d118a23bd8ae2962b3f3c5669bfd|a:1:{s:4:"word";s:4:"8upu";}', NULL),
('frdv9ebc62cbjm5otllg0j0u31', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294802, 1440, '', NULL),
('psoeerhko5b1m5uptd79p0oto0', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294802, 1440, '', NULL),
('24copr6m9k9gi8kkv9cp4h49a2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294803, 1440, '', NULL),
('8abv3t5p60s7oc88audb1u3ee6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294822, 1440, '', NULL),
('itpodtjng3kncgg9ttfnk7tva2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294825, 1440, '', NULL),
('1p6eshhuvcuubo0v30vq7ot241', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141126, 1440, '', NULL),
('lgj8u36nffl67vt7orpkd6u4t5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053836, 1440, '', NULL),
('8e5v03gu4juif967tj298hib03', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053870, 1440, '', NULL),
('0t34aie9nrc1qbjpcrvsklhrn5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053876, 1440, '', NULL),
('82m80lmqmdn418n8jfan2hdeh3', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053878, 1440, '', NULL),
('gjgm2nh8v44usj0mdtmh4chvi3', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053896, 1440, '', NULL),
('9tv2m5okt95p1ciafeqdrt7se4', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371054455, 1440, '', NULL),
('4j1mvtaiip3pq9ti0916f20084', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371055501, 1440, '', NULL),
('5ivs9m9pmac2dn4u2pn8gi4ja1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053941, 1440, '', NULL),
('c7c2pv36u3ps5pokp1327gprq0', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053942, 1440, '', NULL),
('ljdtl68fu5biot02qtev1549k1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053942, 1440, '', NULL),
('7ebngoj5f0eock7a8agrjg2oe6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371053949, 1440, '', NULL),
('9vghvgqveot7p1sanfuml78ht3', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066830, 1440, '', NULL),
('ee3i5mkovd4vhbta23uddvhfh6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066830, 1440, '', NULL),
('lqqj5vp741vjh043pcsfqeaol2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371139812, 1440, 'ufZDSx4h5CtYP3ls1J3KmySUjyY96sZLZvKm8YpTYovemYN5QmC9MnI-cNNtGFuGUPi24141HaEylVpGWrsOEwBThH-dk5oSb1NzGXshPgFBdtH0jGW4AyS5cuwMnWRz-DKmGMb_zRnVcEt7X1J0GAmFzxlTpA_4WtweHxf9MNYE7KeaL_XZa746ShTExseEezdrLQDLd5vgPHTm3_Nnx7aOERtO1APY2Z9HlwTh4meHF2NEkxkh0sA51RHyUenLYtFNTi5mxJrrYN1pIjXNvcPlv4YvbG6cqIwKdUJaXsiEzBJ3iRzrGKgWgZjHvuJ-WP8o9XUPwDQ2aFPbqJsOn1F7qfUfhSzzIx51YR8ogVzO2izXIM5sYrRD6ZVeT9GJ7be3MA2MO8Hz0n9St3ZDidHYbxAODafFYeysL_NOHEM.', NULL),
('rphbaa3egsqg85iotjqob5fqd4', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371056785, 1440, '', NULL),
('ndi85kcn36odkalnask663uoo4', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371212660, 1440, '', NULL),
('0as6lv11ovn43lr49ad3vaplk7', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141126, 1440, '', NULL),
('fgbbo8r88q9evc4bkekoaqohc3', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066854, 1440, '', NULL),
('s22244vbeeibu7hiod9cfpddk6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066855, 1440, '', NULL),
('u16aa41egli2cffobqi24riqq5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066855, 1440, '', NULL),
('47sh8mdotcfib21hbmhsobpi97', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141122, 1440, '', NULL),
('qt1tmdnptrb96ocema2visjs82', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371312888, 1440, '', NULL),
('7atb3786enre346df54ea5pua3', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066862, 1440, '', NULL),
('lha8okgl55cjq9fbhpcksdek45', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371255325, 1440, '', NULL),
('2c9iagviat5s7b6mo1u2red023', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066864, 1440, '', NULL),
('7v4ui1ullp3kp0h1v9rhqtd3l1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066864, 1440, '', NULL),
('o0rgfl0cjo1pllgjiv1smmmk25', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066866, 1440, '', NULL),
('1mka6lvocbpc2hidvpbasffb55', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066867, 1440, '', NULL),
('njotebc6ai9476engfsu32blh1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066867, 1440, '', NULL),
('ce0orpgojtc6nlfvin7mlki3r6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371066942, 1440, '', NULL),
('c63bl8ldq808v80biusqcec7d2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371074395, 1440, 'sThIi8bpKQRrdpkzJYSUZG0VaGFTiKj-oryOBkE1mRbmxozrNzAFHBs86A_0x9fbZv8L_qFsAoqWh3clndfrnyaBY0s2_Sd0EYEGbqLH6d5kW8yF0Nck2C2dFlCEOISTv7BrsDwIvUL-KEPhKOC7k3SOO6He5q6yYLO8fJnDBfMcWoB0AAu5KXPD3z_9LW2jBLS-LuJPOHiVtJRYVuCONsZJOMQKDAcp4jK4eZ9xkNEL5PuG-3hBXuoQfISjCGdVUw40IAGKAcTXrSg9AaZq-w..', NULL),
('ngtvi9ke4od2pfddmcj267e4l2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371501075, 1440, '', NULL),
('hrnjkl9595ufvm1mihgskn3n17', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1372210265, 1440, '__ZF|a:2:{s:50:"Zend_Form_Captcha_8c7e1d092e313bc5f71f7ba3d5ca4346";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372210383;}s:50:"Zend_Form_Captcha_72db10aa902a8d07a4a31a6cc12c3174";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372210385;}}Zend_Form_Captcha_8c7e1d092e313bc5f71f7ba3d5ca4346|a:1:{s:4:"word";s:4:"x9t3";}Zend_Form_Captcha_72db10aa902a8d07a4a31a6cc12c3174|a:1:{s:4:"word";s:4:"fima";}', NULL),
('kvfp85qkqso3eo7abbiqngav70', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1372221966, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_d2de9e2fceace328db61541d0eb5f5a4";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372222086;}}Zend_Form_Captcha_d2de9e2fceace328db61541d0eb5f5a4|a:1:{s:4:"word";s:4:"m849";}', NULL),
('obqoe9a1j6ks8jkphpliplk2s0', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1372237186, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_6a283d0ab3f0568572d3c7c5dc5bc0fa";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372237306;}}Zend_Form_Captcha_6a283d0ab3f0568572d3c7c5dc5bc0fa|a:1:{s:4:"word";s:4:"nuwe";}', NULL),
('nbqvon9blmma1s95g0ddvbpfq2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1372297072, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_9cd9ce84e6d678b2afd21e7cb337bc04";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372297191;}}Zend_Form_Captcha_9cd9ce84e6d678b2afd21e7cb337bc04|a:1:{s:4:"word";s:4:"u4me";}', NULL),
('20n8fus1lfc7r8830hli8l1ac1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371142842, 1440, 'UAxyxKTgEVMooKKGEgkngLJHPshL9u-h5TynTFsq3I06jnTkgb6HQI_YpRMzv-OJ1kXTMvliYZa5OkQsEXi6FqbSpzkCYnzTjkIkm0leXY3LS8IHx7c-Q4oWBgwJX-ELjAhhk715G9zQqNlPiFV936y5C4JaKrSYum58PByggud7VpclAPUnkImFVdNiLMTtqCpvNBqRqBZuRKKw0awozCo8Grp6PTeYdqliMGC3s9cKVJONtFLICIhm9cMSdyyn3b58iGP3ZQkPqH4etHCczA..', NULL),
('5otseo9l67mj5568q5qosmr2i1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371255329, 1440, '', NULL),
('ds64f2lstge6eocaodiomt0a84', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141126, 1440, '', NULL),
('64lfs8u7qgocucvjtru3drrlm1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371149839, 1440, '', NULL),
('c90spibvvdkh3te0hmshisoe97', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141136, 1440, '', NULL),
('uqvc6ue1611fuhmgur0e65b9j0', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141137, 1440, '', NULL),
('krva1s5p1ibtco867q1d5oc8l2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141137, 1440, '', NULL),
('3gusn4g50qpcsfcb5d1d1tsj36', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371315522, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_ab9571135dd066c4c0e96d59816fa7ea";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1371315576;}}Zend_Form_Captcha_ab9571135dd066c4c0e96d59816fa7ea|a:1:{s:4:"word";s:4:"v2ru";}', NULL),
('453m0fbc7rn0movn6l6l62l986', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371315629, 1440, '', NULL),
('t97e22fe0q6vt1m7djkonbtan7', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371323945, 1440, '', NULL),
('uovvsd34esh4oll34vim8c0r23', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371451946, 1440, '', NULL),
('5ssn9tbv6me6g3strq78q4qtj6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371143108, 1440, '', NULL),
('5tu923a19ug6ttedtbfuhnhsh7', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141654, 1440, '', NULL),
('1ln7ks9j64l3a6jf1f5l259hi7', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141655, 1440, '', NULL),
('cpui8rsdhqmt1vo1b9ht14ba02', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141655, 1440, '', NULL),
('mhqmbusupaqtqge7filolaesq1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371142377, 1440, '', NULL),
('2k5ssaja72ergnqg2an88p9mt7', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371142378, 1440, '', NULL),
('56shgf8vbbjiah2dhm8pjfs3e7', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371142378, 1440, '', NULL),
('f1f1petbuphp34g9ilpniqoka6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371251438, 1440, '', NULL),
('7hus6vdukaiddbeprh3vao1a41', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141777, 1440, '', NULL),
('bq8ablfpq7s1otfk86agkssh10', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141778, 1440, '', NULL),
('ar37d5ha622v22khughm3pv8j4', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371141778, 1440, '', NULL),
('4i4ti31ujsvf7stiah9e702p62', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371597828, 1440, 'sqnj4NoVOqAN4wSVHajeWjIpIDOlQBUOgV6xPeqpFiVFGmxsYgnOWxAIW-se5SMIFBerw1FNxZLYA0Kg-9nUaZHgGUglPhcbz5PasnznTL6FMeZ_UalVtKn9aVATS_CEpbElsm9FNnlHkAu6w5neXp66SoH24M1eC_qmj3UdLVq5J66kFhYklU3bB70gscf5fmXeXkx1uT3mshCPYjqPg9vooVPL1ms4rUM0UDJP-ZCxKMOrZzwse1DhNpT1r3vUtP9P07HePKPU582AmHhr0g..', NULL),
('jpan6oet3idrgk5qdfo2990eu1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371554656, 1440, '', NULL),
('pi1hqlgp4ikdoc8qve3hqksnh3', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371758251, 1440, '', NULL),
('9f54s1b6ek9ckl0dnehp8ck266', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1371817090, 1440, '', NULL),
('1bl7vadntmbq6r2oh87ovu1kf1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1373019894, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_60ed74dd1c36c08d9ec897454ee51133";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1373019994;}}Zend_Form_Captcha_60ed74dd1c36c08d9ec897454ee51133|a:1:{s:4:"word";s:4:"syno";}', NULL),
('mn6kih3lfoic7nsncob582g1g2', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375283941, 1440, '', NULL),
('e8i3eo49l2rkcs2944csnnsda1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294781, 1440, '', NULL),
('tg3fcf5l4le4lu8fpak2ul1981', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294784, 1440, '', NULL),
('4upkolvk7valb2fuo395t55as1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1374060774, 1440, '', NULL),
('i84lildlsfhco8r3cm33d1uk96', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375287204, 1440, '', NULL),
('t0gl7i0fmbslmjouj1759tg7s1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375287202, 1440, '', NULL),
('13tq9tiqtk910nei8vkliasgh6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294824, 1440, '__ZF|a:2:{s:50:"Zend_Form_Captcha_65d6bfdb3b280376032d516d0929a229";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1375294919;}s:50:"Zend_Form_Captcha_caae882730b45181782bf10e478cb1fc";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1375294939;}}Zend_Form_Captcha_65d6bfdb3b280376032d516d0929a229|a:1:{s:4:"word";s:4:"k4m4";}Zend_Form_Captcha_caae882730b45181782bf10e478cb1fc|a:1:{s:4:"word";s:4:"tib7";}', NULL),
('5a27hlebd0fccaiksgnuvqt0v5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375060800, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_4345abf9929b8d2fb830420aafcb1bb7";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1375060920;}}Zend_Form_Captcha_4345abf9929b8d2fb830420aafcb1bb7|a:1:{s:4:"word";s:4:"2o95";}', NULL),
('epn1oo1v6abj5u935idhse9hb5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1374295534, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_619ebbaf559f7c32fa05e957abaddf15";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1374295654;}}Zend_Form_Captcha_619ebbaf559f7c32fa05e957abaddf15|a:1:{s:4:"word";s:4:"98b7";}', NULL),
('ir4fmc20qo8iar9c9fq9jva3t5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1374295821, 1440, '', NULL),
('smoe4po6qvcdnv5i30seqpfe35', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294786, 1440, '', NULL),
('u8hrvaluarla36cha0m4li8r32', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294788, 1440, '', NULL),
('s6ckd98qs7eq8pt0rg07pbf4a1', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294795, 1440, '', NULL),
('3l1mo4v85rvbm6lfukhqrqo2q6', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1375294798, 1440, '', NULL),
('6u1nuk4rque2bp9gm6bkjt75u5', '/var/www/coffe-group.art-creative.net/application/../data/session', 'PHPSESSID', 1372197967, 1440, '__ZF|a:1:{s:50:"Zend_Form_Captcha_45bb836b741e9e4fe1dc21692618e17c";a:2:{s:4:"ENNH";i:1;s:3:"ENT";i:1372198087;}}Zend_Form_Captcha_45bb836b741e9e4fe1dc21692618e17c|a:1:{s:4:"word";s:4:"qak3";}', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_to` varchar(255) NOT NULL,
  `email_from` varchar(255) NOT NULL,
  `email_reply_to` varchar(255) NOT NULL,
  `watermark` varchar(255) NOT NULL,
  `tracking_code` text,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`setting_id`, `email_to`, `email_from`, `email_reply_to`, `watermark`, `tracking_code`) VALUES
(1, 'office@site.com', 'office@site.com', 'office@site.com', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL DEFAULT 'default',
  `setting_type` enum('input','textarea','checkbox') DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`setting_id`, `key`, `module`, `setting_type`, `name`) VALUES
(1, 'catalog_newest_on_main', 'catalog', 'input', 'Количество "Новинок" на главной ; Период для новых (в днях)'),
(2, 'catalog_hit_on_main', 'catalog', 'input', 'Количество "Хитов" на главной ; Период для новых (в днях)'),
(3, 'catalog_special_on_main', 'catalog', 'input', 'Количество "Спецпредложений" на главной ; Период для новых (в днях)'),
(4, 'site_name', 'default', NULL, 'Имя сайта'),
(5, 'site_slogan', 'default', NULL, 'Слоган сайта'),
(28, 'title_products', 'default', NULL, 'Заголовок Продукция'),
(8, 'email_to', 'default', NULL, 'E-mail(ы) администраторов сайта на которые отправляются все письма сайта (To)'),
(9, 'email_from', 'default', NULL, 'E-mail с которого отправлять письма (From)'),
(10, 'reply_to', 'default', NULL, 'E-mail для ответа (Reply to)'),
(11, 'watermark', 'default', NULL, 'Текст для водяного знака'),
(12, 'tracking_code', 'default', NULL, 'Код отслеживания посещений'),
(13, 'payment_privat24_url', 'payment', NULL, 'Privat24 - Путь для оплаты'),
(14, 'payment_privat24_merchant', 'payment', NULL, 'Privat24 - Merchant ID'),
(15, 'payment_privat24_password', 'payment', NULL, 'Privat24 -  Password'),
(16, 'copyrights', 'default', NULL, 'Копирайт'),
(17, 'title_brend', 'default', NULL, 'Заголовок блока Брендов'),
(18, 'title_news', 'default', NULL, 'Заголовок Новости'),
(19, 'insc_all_news', 'default', NULL, 'Надпись "Все новости"'),
(20, 'title_stati', 'default', NULL, 'Заголовок блока Статьи'),
(21, 'insc_all_stati', 'default', NULL, 'Надпись "Все статьи"'),
(22, 'menu_title_1', 'default', NULL, 'Заголовок Меню 1'),
(23, 'menu_title_2', 'default', NULL, 'Заголовок Меню 2'),
(24, 'menu_title_3', 'default', NULL, 'Заголовок Меню 3'),
(25, 'contact_map', 'default', NULL, 'Меню (Карта проезда)'),
(26, 'contact_feedback', 'default', NULL, 'Меню (Написать нам)'),
(27, 'date_format_news', 'default', NULL, 'Формат вывода даты для новостей');

-- --------------------------------------------------------

--
-- Структура таблицы `settings_translation`
--

CREATE TABLE IF NOT EXISTS `settings_translation` (
  `setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `value` text,
  PRIMARY KEY (`setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings_translation`
--

INSERT INTO `settings_translation` (`setting_id`, `language_code`, `value`) VALUES
(1, 'ru', '12;90'),
(2, 'ru', '12;90'),
(3, 'ru', '12;90'),
(4, 'ru', 'Coffe Group-A'),
(5, 'ru', ''),
(6, 'ru', 'Бюро "Рабочее название"'),
(7, 'ru', 'Бюро "Рабочее название"'),
(9, 'ru', 'info@coffeegroup-a.com.ua'),
(11, 'ru', ''),
(12, 'ru', ''),
(8, 'ru', 'info@coffeegroup-a.com.ua; office.artcreative@gmail.com'),
(10, 'ru', 'info@coffeegroup-a.com.ua'),
(13, 'ru', 'https://api.privatbank.ua:9083/p24api/icatalog'),
(14, 'ru', '70672'),
(15, 'ru', '03OImXpW6LqiU0IpGLd211m98V30h1Va'),
(16, 'ru', '©  2013 - Все права защищены'),
(17, 'ru', 'Региональный представитель в Украине брендов'),
(18, 'ru', 'Новости'),
(19, 'ru', 'Все новости »'),
(20, 'ru', 'Полезные статьи'),
(21, 'ru', 'Все статьи »'),
(22, 'ru', 'О нас'),
(23, 'ru', 'Полезная информация'),
(24, 'ru', 'Контактная информация'),
(25, 'ru', 'Карта проезда'),
(26, 'ru', 'Написать нам'),
(27, 'ru', 'd M Yг.'),
(28, 'ru', 'мы представляем Бренды');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize`
--

CREATE TABLE IF NOT EXISTS `setting_image_resize` (
  `setting_image_resize_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `height_thumbnail` int(11) NOT NULL,
  `width_thumbnail` int(11) NOT NULL,
  `strategy_thumbnail_id` int(11) NOT NULL,
  `height_preview` int(11) NOT NULL,
  `width_preview` int(11) NOT NULL,
  `strategy_preview_id` int(11) NOT NULL,
  `height_detail` int(11) NOT NULL,
  `width_detail` int(11) NOT NULL,
  `strategy_detail_id` int(11) NOT NULL,
  `height_full` int(11) NOT NULL,
  `width_full` int(11) NOT NULL,
  `strategy_full_id` int(11) NOT NULL,
  PRIMARY KEY (`setting_image_resize_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `setting_image_resize`
--

INSERT INTO `setting_image_resize` (`setting_image_resize_id`, `name`, `ident`, `height_thumbnail`, `width_thumbnail`, `strategy_thumbnail_id`, `height_preview`, `width_preview`, `strategy_preview_id`, `height_detail`, `width_detail`, `strategy_detail_id`, `height_full`, `width_full`, `strategy_full_id`) VALUES
(1, 'Каталог / категория', 'catalog_category', 189, 189, 1, 362, 315, 1, 0, 0, 3, 600, 600, 2),
(2, 'Каталог / продукт / основное', 'catalog_product_main', 77, 106, 3, 147, 218, 3, 230, 377, 3, 700, 900, 2),
(3, 'Каталог / продукт / дополнительное', 'catalog_product_additional', 77, 106, 3, 147, 218, 3, 474, 474, 3, 700, 900, 2),
(4, 'Публикации', 'publication', 137, 211, 1, 181, 249, 1, 0, 0, 1, 700, 900, 2),
(5, 'Услуги / категория', 'service_category', 83, 83, 1, 166, 166, 1, 0, 0, 0, 700, 900, 2),
(6, 'Услуги / услуга', 'service_service', 100, 100, 1, 180, 180, 1, 0, 0, 0, 700, 900, 2),
(7, 'Галерея / альбом', 'gallery_album', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(8, 'Галерея / изобрaжение', 'gallery_photo', 158, 158, 1, 200, 200, 1, 0, 0, 0, 700, 900, 2),
(9, 'Слайдер', 'slider', 46, 200, 2, 46, 200, 2, 0, 0, 1, 330, 1000, 2),
(10, 'Представители', 'representation', 75, 100, 1, 75, 100, 1, 0, 0, 0, 75, 100, 1),
(11, 'Контакты / схема проезда', 'contact_map', 60, 140, 1, 300, 705, 1, 0, 0, 1, 700, 900, 2),
(12, 'Страницы', 'page', 172, 172, 1, 271, 271, 1, 300, 300, 1, 700, 900, 2),
(16, 'Баннеры', 'banner', 60, 100, 2, 60, 100, 2, 0, 0, 1, 70, 227, 2),
(17, 'Магазин / Производитель', 'catalog_manufacturer', 50, 300, 2, 50, 300, 2, 0, 0, 2, 50, 300, 2),
(18, 'Пункт меню', 'menuItem', 20, 20, 2, 0, 0, 1, 0, 0, 1, 0, 0, 1),
(19, 'Файлы', 'file', 136, 95, 1, 252, 175, 1, 252, 175, 2, 1024, 723, 2),
(21, 'Каталог / продукт / технологии', 'catalog_product_technology', 138, 138, 3, 215, 215, 3, 474, 474, 3, 700, 900, 2),
(22, 'Магазин / категория', 'catalog_category', 156, 184, 3, 142, 216, 3, 230, 377, 3, 600, 900, 2),
(23, 'Магазин / продукт / основное', 'catalog_product_main', 77, 106, 3, 147, 218, 3, 254, 377, 3, 570, 850, 2),
(24, 'Магазин / продукт / дополнительное', 'catalog_product_additional', 60, 86, 3, 215, 215, 3, 474, 474, 3, 570, 850, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `setting_image_resize_strategy`
--

CREATE TABLE IF NOT EXISTS `setting_image_resize_strategy` (
  `setting_image_resize_strategy_id` int(11) NOT NULL AUTO_INCREMENT,
  `ident` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `preview_image` varchar(32) NOT NULL,
  PRIMARY KEY (`setting_image_resize_strategy_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `setting_image_resize_strategy`
--

INSERT INTO `setting_image_resize_strategy` (`setting_image_resize_strategy_id`, `ident`, `name`, `description`, `preview_image`) VALUES
(1, 'Crop', 'Обрезка', 'Стратегия для изменения размера изображения таким образом, что ее наименьшим край вписывается в кадр. Остальное обрезается.', ''),
(2, 'Fit', 'Изменения размера', 'Стратегия для изменения размера изображения путем подбора контента в заданных размерах.', ''),
(3, 'FitFill', 'Встраивание', 'Стратегия для изменения размера изображения таким образом, что оно полностью вписывается в кадр. Остальное пространство заливается цветом.', ''),
(4, 'FitStrain', 'Деформация', 'Стратегия для изменения размера без учета пропорций.', '');

-- --------------------------------------------------------

--
-- Структура таблицы `setting_translation`
--

CREATE TABLE IF NOT EXISTS `setting_translation` (
  `setting_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_slogan` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  PRIMARY KEY (`setting_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `setting_translation`
--

INSERT INTO `setting_translation` (`setting_id`, `language_code`, `site_name`, `site_slogan`, `meta_description`, `meta_keywords`) VALUES
(1, 'ru', 'Интернет магазин "Velox"', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ', 'Velox, интернет магазин velox  ,  ВЕЛОСИПЕДЫ, ВЕЛОАКСЕССУАРЫ, ВЕЛОЗАПЧАСТИ, СПОРТТОВАРЫ, ОТДЫХ И ТУРИЗМ');

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(255) NOT NULL,
  `preview` varchar(255) NOT NULL,
  `full` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `slider`
--


-- --------------------------------------------------------

--
-- Структура таблицы `slider_group`
--

CREATE TABLE IF NOT EXISTS `slider_group` (
  `slider_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  PRIMARY KEY (`slider_group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `slider_group`
--


-- --------------------------------------------------------

--
-- Структура таблицы `slider_translation`
--

CREATE TABLE IF NOT EXISTS `slider_translation` (
  `slider_id` int(11) NOT NULL,
  `language_code` varchar(2) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) NOT NULL,
  PRIMARY KEY (`slider_id`,`language_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider_translation`
--


-- --------------------------------------------------------

--
-- Структура таблицы `storage_images`
--

CREATE TABLE IF NOT EXISTS `storage_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `path` varchar(512) DEFAULT NULL,
  `original` varchar(512) DEFAULT NULL,
  `full` varchar(512) DEFAULT NULL,
  `detail` varchar(512) DEFAULT NULL,
  `preview` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `storage_images`
--

INSERT INTO `storage_images` (`image_id`, `title`, `path`, `original`, `full`, `detail`, `preview`, `thumbnail`, `updated_at`, `created_at`) VALUES
(5, 'test', '/images/2012/10/27', 'test_201210271737.jpg', 'test_201210271737-900x700.jpg', 'test_201210271737-300x300.jpg', 'test_201210271737-271x271.jpg', 'test_201210271737-172x172.jpg', '2012-10-27 11:37:35', '2012-10-27 11:37:29'),
(7, '123', '/images/2012/10/27', '123_201210271752.jpg', '123_201210271752-900x700.jpg', '123_201210271752-300x300.jpg', '123_201210271752-271x271.jpg', '123_201210271752-172x172.jpg', '2012-10-27 11:52:45', '2012-10-27 11:52:42'),
(9, 'qweqew', '/images/2012/10/27', 'qweqew_201210271804.jpg', 'qweqew_201210271804-900x700.jpg', 'qweqew_201210271804-1x1.jpg', 'qweqew_201210271804-228x138.jpg', 'qweqew_201210271804-183x111.jpg', '2012-10-27 12:04:19', '2012-10-27 12:04:16');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(128) NOT NULL,
  `patronymicname` varchar(128) DEFAULT NULL,
  `lastname` varchar(128) DEFAULT NULL,
  `gender` tinyint(4) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `passwd` char(40) NOT NULL,
  `salt` char(32) NOT NULL,
  `confirmation_hash` varchar(32) NOT NULL,
  `forgot_hash` char(32) NOT NULL,
  `role` varchar(100) NOT NULL DEFAULT 'Member',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_of_birthday` datetime NOT NULL,
  `country` varchar(128) DEFAULT NULL,
  `address` varchar(1024) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `mobilephone` varchar(32) NOT NULL,
  `subscription` tinyint(4) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `email_pass` (`email`,`passwd`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `patronymicname`, `lastname`, `gender`, `email`, `username`, `passwd`, `salt`, `confirmation_hash`, `forgot_hash`, `role`, `status`, `date_of_birthday`, `country`, `address`, `city`, `zipcode`, `telephone`, `mobilephone`, `subscription`, `updated_at`, `created_at`) VALUES
(1, 'Andrew', NULL, 'Mae', 1, 'amey@i.ua', NULL, '8b90ed5a21aaad4a603fe4065a58d9efe9e3a05c', '85cf3f5ac2c1caf2a78f1f51f79990fa', '', '', 'Root', 1, '0000-00-00 00:00:00', '', 'Адрес', 'Город', '', '', '', 1, '2013-04-02 14:28:36', '2012-07-02 02:23:49'),
(2, 'Андрей', NULL, 'Шайда', 0, 'shayda.andrey@gmail.com', NULL, '21a50de6601c8f63b2a3f8db300d2dfdb9c45e2b', '2de7b51589582489e4769d87dd925924', '', '4074384831a709f00e127240f1eadb5b', 'Root', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '2013-04-02 14:28:36', '0000-00-00 00:00:00'),
(6, 'Менеджер', NULL, '', 0, 'info@coffeegroup-a.com.ua', NULL, '172c6598203614394b9391f07f0a74671526437e', '1872fc3c4a23ff8c9ea2c9430c1d2457', '', '', 'Manager', 1, '1970-01-01 03:00:00', '0', 'Адрес', 'Хмельницкий', '', '000', '', 1, '2013-07-03 08:18:33', '0000-00-00 00:00:00'),
(7, 'Редактор', NULL, '', 0, 'redactor@site.com', NULL, '94cc640587e6957d1de99c1d5f68be111e5118f6', '5c3cadd7890e8ef9c09242249945f8d0', '', '', 'Redactor', 1, '0000-00-00 00:00:00', '0', '', '', '', '', '', 0, '2013-04-02 14:29:09', '0000-00-00 00:00:00'),
(3, 'Виктор', 'Петрович', 'Старанчук', 1, 'vtipok89@gmail.com', NULL, '50c572ab73436d473b884dab818fbfd062befcdf', 'b5194437587639e3fcd60972d9949fce', '', '', 'Root', 1, '1989-03-22 00:00:00', 'Україна', 'Хмельницкий', 'Хмельницкий', '29000', '', '+380989789064', 1, '2013-07-03 08:19:23', '2012-12-03 13:17:06'),
(5, 'Администратор', NULL, 'Сайта', 0, 'admin@site.com', NULL, 'a3fbf4bd85d227a9d1da5a7507e2c63e0d460cb7', 'ef236a5830128c472697cf97272c1a53', '', '', 'Admin', 1, '1970-01-01 03:00:00', '0', 'Адрес', 'Город', '', '000', '', 1, '2013-04-02 14:29:13', '0000-00-00 00:00:00'),
(8, 'Менеджер2', NULL, '', 0, 'good.hattrick@gmail.com', NULL, '0d50c89411c329e2bb606ba3c58f0ddc982dbe82', '0396eea2714967a8847d6a9c8606acef', '', '', 'Manager', 1, '0000-00-00 00:00:00', NULL, '', '', '', '', '', 0, '2013-07-03 08:29:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `user_socials`
--

CREATE TABLE IF NOT EXISTS `user_socials` (
  `user_id` int(11) NOT NULL,
  `social_id` varchar(64) NOT NULL,
  `type` varchar(32) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`social_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_socials`
--

INSERT INTO `user_socials` (`user_id`, `social_id`, `type`, `username`) VALUES
(12, '61687591', 'vkontakte', ''),
(11, '1848865103', 'facebook', 'maeandrew');
