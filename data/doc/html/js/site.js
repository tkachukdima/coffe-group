$(document).ready(function() {

/*------------simple menu-------------*/						   
	$("#menu_top li").hover(function(){
		$(this).addClass('hover');
		$(this).find("ul:first").fadeIn("fast");
		Cufon.replace('#menu_top > li.hover > a', {fontFamily: 'Myriad Pro Cond', color:"#929202" });			
	},function(){
		Cufon.replace('#menu_top > li.hover > a:not(#menu_top > li.active > a)', { fontFamily: 'Myriad Pro Cond',color:"#454545" });
		$(this).removeClass('hover');
		$(this).find("ul:first").fadeOut("fast");
	});
	
	$(".car_wr , .slider_wr").hover(function(){
		$(this).find('.car_btn , .slider_btn').show();
	},function(){
		$(this).find('.car_btn , .slider_btn').hide();
	});	
	
	$('#slider .hot_news').each(function(){
			$(this).find('.txt > span').css('margin-top',(69-$(this).find('.txt > span').height())/2);
		});
		
	$('.brend').each(function(){
			$(this).find('.txt').css('margin-top',(220-$(this).find('.txt').outerHeight(true))/2);
		});		

/*-----------static-------------*/
	$("div.text").append("<div class='clear'></div>");
	$("div.text ol li").wrapInner("<span></span>");
	$('tr th:first-child').addClass('tl_corner');
	$('tr th:last-child').addClass('tr_corner');
	

/*----jcarousel----------*/
	$('#jcarousel').jcarousel({
		'wrap':'circular'
	}).jcarouselAutoscroll({
		'interval': 4000
	});
	
	$('#slider').cycle({ 
			timeout: 4000,
			prev: '#slider_prev',
			next: '#slider_next'
	});
	
});
