<?php

class Guestbook_IndexControllerTest extends ControllerTestCase {

    public function setUp() {
        parent::setUp();
    }

    public function testIndexAction() {
        $params = array('action' => 'index', 'controller' => 'index', 'module' => 'guestbook');
        $url = $this->url($this->urlizeOptions($params));
        
        $this->dispatch($url);

        // assertions
        $this->assertModule($params['module']);
        $this->assertController($params['controller']);
        $this->assertAction($params['action']);
        $this->assertQueryContentContains('p', 'Please leave a comment');
        $this->assertQueryCount('form#guestbookForm', 1);
    }

//    public function testProcessAction() {
//        $testData = array(
//            'name' => 'testUser',
//            'email' => 'test@example.com',
//            'website' => 'http://www.example.com',
//            'message' => 'This is a test message',
//        );
//        $params = array('action' => 'process', 'controller' => 'index', 'module' => 'guestbook');
//        $url = $this->url($this->urlizeOptions($params));
//        $this->request->setMethod('post');
//        $this->request->setPost($testData);
//        $this->dispatch($url);
//
//        // assertions
//        $this->assertModule($params['module']);
//        $this->assertController($params['controller']);
//        $this->assertAction($params['action']);
//
//        $this->assertResponseCode(302);
//        $this->assertRedirectTo('/guestbook/index/success');
//
//        $this->resetRequest();
//        $this->resetResponse();
//        $this->dispatch('/guestbook/index/success');
////        $this->assertQueryContentContains('span#fullName', $testData['name']);
//    }
//
//

}

