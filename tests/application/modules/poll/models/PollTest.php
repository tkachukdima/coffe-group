<?php

/**
 * Poll_Model_PageTest
 *
 * @category Tests
 * @package  Poll
 */
class Poll_Model_PollTest extends ControllerTestCase
{

    protected $user;
    protected $object;

    public function setUp()
    {
        parent::setUp();
        parent::_doLogin('Admin');
        parent::_setUpLanguages();

        $auth = Zend_Auth::getInstance();
        $this->user = $auth->getIdentity();

        $this->object = new Poll_Model_Poll();
    }

    public function testCanCreateTestObject()
    {
        $this->assertNotNull($this->object);
    }

    
    public function testAddPoll()
    {
        $data = array(
            'date_start' => '2012-01-02 01:01:01',
            'date_end' => '2012-01-12 01:01:01',
            'status' => 1,
            'text_ru' => 'Опрос 1',
            'choices' => array(
                array(
                    'text_ru' => 'Вариант 1',
                    'votes' => 0,
                    'sort_order' => 1
                    ),
                array(
                    'text_ru' => 'Вариант 2',
                    'votes' => 0,
                    'sort_order' => 2
                ),
            )
        );

        $this->object->savePoll($data, 'add');
    }      

}

