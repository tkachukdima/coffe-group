<?php
/**
 * Menu_Model_PageTest
 *
 * @category Tests
 * @package  Menu
 */
class Menu_Model_MenuTest extends ControllerTestCase
{

    protected $user;
    protected $object;    
       
    public function setUp()
    {                       
        parent::setUp(); 
        parent::_doLogin('Admin');
        parent::_setUpLanguages();
        
        $auth = Zend_Auth::getInstance();
        $this->user =  $auth->getIdentity();
        
        $this->object = new Menu_Model_Menu();   

    }


    public function testCanCreateTestObject()
    {
        $this->assertNotNull($this->object);
    }
//
//    public function testCanCreateMenu()
//    {
//        $data = $this->getTestMenuData();
//        $menuId = $this->object->saveMenu($data);
//
//        $menu = $this->object->getMenuByIdForEdit($menuId);
//
//        $this->assertEquals($data['title_ru'], $menu['title_ru']);
//        $this->assertEquals($data['title_en'], $menu['title_en']);
//        $this->assertEquals($data['status'], $menu['status']);
//        $this->assertEquals($data['sort_order'], $menu['sort_order']);
//        $this->assertEquals($data['type'], $menu['type']);
//
//        $this->object->deleteMenu($menuId);
//
//    }
//
//     public function testCanUpdateMenu()
//    {
//        $data = $this->getTestMenuData();
//        $menuId = $this->object->saveMenu($data);
//
//        $newData = $data;
//
//        $newData['menu_id'] = $menuId;
//        $newData['title_ru'] = 'Новое главное меню';
//        $newData['title_en'] = 'New main menu';
//        $newData['type'] = 'new_main' . time();
//        $newData['sort_order'] =  2;
//        $newData['status'] =  0;
//
//        $menuId = $this->object->saveMenu($newData, 'edit');
//
//        $menu = $this->object->getMenuByIdForEdit($menuId);
//
//        $this->assertEquals($newData['title_ru'], $menu['title_ru']);
//        $this->assertEquals($newData['title_en'], $menu['title_en']);
//        $this->assertEquals($newData['status'], $menu['status']);
//        $this->assertEquals($newData['sort_order'], $menu['sort_order']);
//        $this->assertEquals($newData['type'], $menu['type']);
//
//        $this->object->deleteMenu($menuId);
//
//    }
//
//    public function testCanGetMenus()
//    {
//        $menuId = $this->object->saveMenu($this->getTestMenuData());
//        $this->assertEquals(1, count($this->object->getMenus()));
//        $this->object->deleteMenu($menuId);
//    }
//
//    public function testCanGetMenuByType()
//    {
//        $data = $this->getTestMenuData();
//        $menuId = $this->object->saveMenu($data);
//
//        $menu = $this->object->getMenuByType($data['type']);
//
//        $this->assertEquals($data['title_ru'], $menu->title);
//        $this->assertEquals($data['status'], $menu->status);
//        $this->assertEquals($data['sort_order'], $menu->sort_order);
//        $this->assertEquals($data['type'], $menu->type);
//
//        $this->object->deleteMenu($menuId);
//    }
//
//    /**
//     * @return array
//     */
//    private function getTestMenuData()
//    {
//        return array(
//            'title_ru' => 'Главное меню',
//            'title_en' => 'Main menu',
//            'type' => 'main' . rand(9999, 999999),
//            'sort_order' => 1,
//            'status' => 1
//        );
//    }
//    
     
    
    
}

