<?php

/**
 * @see ARTCMF_Filter_ImageSize_Strategy_Interface
 */
require_once 'ARTCMF/Filter/ImageSize/Strategy/Interface.php';

/**
 * Strategy for resizing the image by fitting the content into the given
 * dimensions.
 */
class ARTCMF_Filter_Imagesize_Strategy_FitStrain implements ARTCMF_Filter_ImageSize_Strategy_Interface
{

    /**
     * Return canvas resized according to the given dimensions.
     * @param resource $image GD image resource
     * @param int $width Output width
     * @param int $height Output height
     * @return resource GD image resource
     */
    public function resize($image, $width, $height, $coordinates)
    {
        $origWidth = imagesx($image);
        $origHeight = imagesy($image);

         if ($origWidth < $width) {
            $width = $origWidth;
        }

        if ($origHeight < $height) {
            $height = $origHeight;
        }

        $cropped = imagecreatetruecolor($width, $height);

        imageAlphaBlending($cropped, false);
        imageSaveAlpha($cropped, true);
        
        if ($origWidth > $origHeight) {

            imagecopyresampled($cropped, $image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
        }

        if ($origWidth <= $origHeight) {

            imagecopyresampled($cropped, $image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);
        }


        return $cropped;
    }

}