<?php

/**
* @see ARTCMF_Filter_ImageSize_Strategy_Interface
*/
require_once 'ARTCMF/Filter/ImageSize/Strategy/Interface.php';

/**
* Strategy for resizing the image by fitting the content into the given
* dimensions.
*/
class ARTCMF_Filter_ImageSize_Strategy_Fit
    implements ARTCMF_Filter_ImageSize_Strategy_Interface
{
    /**
* Return canvas resized according to the given dimensions.
* @param resource $image GD image resource
* @param int $width Output width
* @param int $height Output height
* @return resource GD image resource
*/
    public function resize($image, $width, $height, $coordinates)
    {
        $origWidth = imagesx($image);
        $origHeight = imagesy($image);
        
        if ($origWidth < $width) {
            $width = $origWidth;
        }

        if ($origHeight < $height) {
            $height = $origHeight;
        }


        $ratio = max(($origWidth / $width), ($origHeight / $height));

        $newWidth =  $origWidth / $ratio;
        $newHeight =  $origHeight / $ratio;
        

        $resized = imagecreatetruecolor($newWidth, $newHeight);
        
        imageAlphaBlending($resized, false);
        imageSaveAlpha($resized, true);
        
        imagecopyresampled($resized, $image, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);

        return $resized;
    }
}