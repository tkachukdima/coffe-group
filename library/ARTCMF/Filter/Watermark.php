<?php

class ARTCMF_Filter_Watermark
{

    function create_watermark($main_img_obj, $text, $font, $r = 128, $g = 128, $b = 128, $alpha_level = 100)
    {
        $width = imagesx($main_img_obj);
        $height = imagesy($main_img_obj);
        $angle = 0;

        $text = " " . $text . " ";

        $c = imagecolorallocatealpha($main_img_obj, $r, $g, $b, $alpha_level);
        $size = 14;
        $box = imagettfbbox($size, $angle, $font, $text);
        $x = $width - abs($box[4] - $box[0]);
        $y = $height - 10 ;

        imagettftext($main_img_obj, $size, $angle, $x, $y, $c, $font, $text);
        return $main_img_obj;
    }

}
