<?php

class ARTCMF_Controller_Plugin_Cache extends Zend_Controller_Plugin_Abstract
{

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ('production' == APPLICATION_ENV) {

            if (!$request->getParam('isAdmin')) {

                $frontendOptions = array(
                    'lifetime' => 60 * 60, // 1 hour
                    'debug_header' => false,
                    'default_options' => array(
                        'cache' => true
                    )
                );

                $backendOptions = array(
                    'cache_dir' => APPLICATION_PATH . '/../data/cache/html',
                );

                $cache_id = $_SERVER['REQUEST_URI'];

                $lastSymbol = $cache_id{strlen($cache_id) - 1};

                if ($lastSymbol == '/') {
                    $cache_id = substr($cache_id, 0, -1);
                }

                $cache = Zend_Cache::factory('Page', 'File', $frontendOptions, $backendOptions);


                $cache->start(md5($cache_id));
            }
        }
    }

}