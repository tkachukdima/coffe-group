<?php

class ARTCMF_Controller_Plugin_AccessCheck extends Zend_Controller_Plugin_Abstract
{

    /**
     * @var Zend_Acl
     */
    protected $_acl;

    /**
     * @var string
     */
    protected $_identity;

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {               
        $this->_acl = new ARTCMF_Acl();
        $module   = ('admin' != $request->getModuleName()) ? $request->getModuleName() : 'default';
        $resource = ('admin' != $request->getModuleName()) ? $request->getControllerName() : $request->getModuleName();
        $action   = ('admin' != $request->getModuleName()) ? $request->getActionName() : '';

        //print_r($module . ':' . $resource .':' .$action); exit;

        if (!$this->_acl->has($module . ':' . $resource)) {
            throw new ARTCMF_Exception_404(Zend_Registry::get('Zend_Translate')->_('404 page not found.'));
        }

        if (!$this->_acl->isAllowed($this->getIdentity(), $module . ':' . $resource, $action)) {
            if ('Guest' == $this->getIdentity()) 
            {
                if($request->getParam('isAdmin')) {
                    $request->setModuleName('user')
                        ->setControllerName('auth')
                        ->setActionName('login');
                } else {
                    $request->setModuleName('default')
                            ->setControllerName('index')
                            ->setActionName('index')
                            ->setParam('is_login', true);
                }
            } else
                throw new ARTCMF_Acl_Exception(Zend_Registry::get('Zend_Translate')->_('Insufficient rights'));
            }

        // Цепляем ACL к Zend_Navigator
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($this->_acl);
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($this->getIdentity());

        //$this->_acl->setDynamicPermissions();

        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();
        $view->addBasePath(APPLICATION_PATH . '/layouts');
        
        if ($request->getParam('isAdmin') AND !($this->getIdentity() == 'Guest' OR $this->getIdentity()->role == 'Member')) {            
            $layout->setLayout('admin');
        }
        
        if (!$request->getParam('isAdmin')) {
           $this->checkIpAccess();
        }
        
//        $auth = Zend_Auth::getInstance();
//        if($auth->hasIdentity()) {
//            $userModel = new User_Model_User();            
//            $user = $userModel->getUserById($this->getIdentity()->user_id);
//            $sessionModel = new User_Model_Session();
//            $sessionModel->clearSessionByUserId($user->user_id);
//            $auth->getStorage()->write($user);
//        }
    }

    /**
     * Set the identity of the current request
     *
     * @param array|string|null|Zend_Acl_Role_Interface $identity
     * @return ARTCMF_Controller_Helper_Acl
     */
    public function setIdentity($identity)
    {
        if (is_array($identity)) {
            if (!isset($identity['role'])) {
                $identity['role'] = 'Guest';
            }
            $identity = new Zend_Acl_Role($identity['role']);
        } elseif (is_scalar($identity) && !is_bool($identity)) {
            $identity = new Zend_Acl_Role($identity);
        } elseif (null === $identity) {
            $identity = new Zend_Acl_Role('Guest');
        } elseif (!$identity instanceof Zend_Acl_Role_Interface) {
            throw new ARTCMF_Model_Exception('Invalid identity provided');
        }
        $this->_identity = $identity;
        return $this;
    }

    /**
     * Get the identity, if no ident use guest
     *
     * @return string
     */
    public function getIdentity()
    {
        if (null === $this->_identity) {
            $auth = Zend_Auth::getInstance();
            if (!$auth->hasIdentity()) {
                return 'Guest';
            }
            $this->setIdentity($auth->getIdentity());
        }
        return $this->_identity;
    }
    
    
    private function checkIpAccess()
    {
        $ip = ARTCMF_ClientIp::getClientIp();
        $ipblockingModel = new Default_Model_Ipblocking();
        
        $ip_address = $ipblockingModel->getIp($ip);
        
        if(!is_null($ip_address)){
            throw new ARTCMF_Acl_Exception(Zend_Registry::get('Zend_Translate')->_('Insufficient rights'));
        }
        
    }

}
