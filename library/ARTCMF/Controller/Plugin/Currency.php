<?php

/**
 * ARTCMF_Controller_Plugin_Currency
 * 
 * @category   Default
 * @package    ARTCMF_Plugin
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Plugin_Currency extends Zend_Controller_Plugin_Abstract
{

    private $sessionNamespace;
    private $currencyModel;
    private $currency;
    private $currencies = array();

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if(Zend_Registry::isRegistered('Currency')) {
            return;
        }
        
        $this->loadSession();

        $this->currencyModel = new Catalog_Model_Currency();
       
        foreach ($this->currencyModel->getCurrencies() as $result) {
            $this->currencies[$result->currency_code] = array(
                'currency_id' => $result->currency_id,
                'title' => $result->title,
                'symbol_left' => $result->symbol_left,
                'symbol_right' => $result->symbol_right,
                'decimal_place' => $result->decimal_place,
                'value' => $result->value,
                'value_non_cash' =>  $result->value_non_cash
            );
        }

        if (!is_null($request->getParam('currency', null)) && (array_key_exists($request->getParam('currency', null), $this->currencies))) {
            $this->set($request->getParam('currency'));
        } elseif ((isset($this->getSessionNs()->currency)) && (array_key_exists($this->getSessionNs()->currency, $this->currencies))) {
            $this->set($this->getSessionNs()->currency);
        } else {
            $currency = array_keys($this->currencies);
            $this->set($currency[0]);
        }
        
        Zend_Registry::set('Currency', $this);
    }

    public function set($currency)
    {
        $this->currency = $currency;

        if ((!isset($this->getSessionNs()->currency)) || ($this->getSessionNs()->currency != $currency)) {
            $this->getSessionNs()->currency = $currency;
        }
    }

    public function format($number, $currency = '', $value = '', $format = true)
    {
        if ($currency && $this->has($currency)) {
            $symbol_left = $this->currencies[$currency]['symbol_left'];
            $symbol_right = $this->currencies[$currency]['symbol_right'];
            $decimal_place = $this->currencies[$currency]['decimal_place'];
        } else {
            $symbol_left = $this->currencies[$this->currency]['symbol_left'];
            $symbol_right = $this->currencies[$this->currency]['symbol_right'];
            $decimal_place = $this->currencies[$this->currency]['decimal_place'];

            $currency = $this->currency;
        }

        if ($value) {
            $value = $value;
        } else {
            $value = $this->currencies[$currency]['value'];
        }

        if ($value) {
            $value = $number * $value;
        } else {
            $value = $number;
        }

        $string = '';

        if (($symbol_left) && ($format)) {
            $string .= '<span class="symbol_left">' . $symbol_left . '</span>';
        }

        $decimal_point = '.';

        $thousand_point = ' ';


        $string .= '<span class="price_value">' .
                number_format(round($value, (int) $decimal_place), (int) $decimal_place, $decimal_point, $thousand_point)
                . '</span>';

        if (($symbol_right) && ($format)) {
            $string .= '<span class="symbol_right">' . $symbol_right . '</span>';
        }

        return $string;
    }    
    
    public function getConverted($value, $decimal_place = 2, $decimal_point = '.', $thousand_point = ' ')
    {
        return number_format(round($value * $this->currencies[$this->currency]['value'], (int) $decimal_place), (int) $decimal_place, $decimal_point, $thousand_point);
    }

    public function convert($value, $from, $to, $decimal_place = 2, $decimal_point = '.', $thousand_point = ' ')
    {
        if (isset($this->currencies[$from])) {
            $from = $this->currencies[$from]['value'];
        } else {
            $from = 1;
        }

        if (isset($this->currencies[$to])) {
            $to = $this->currencies[$to]['value'];
        } else {
            $to = 1;
        }

        return number_format(round($value * ($to / $from), (int) $decimal_place), (int) $decimal_place, $decimal_point, $thousand_point);
    }

    public function getId($currency = '')
    {
        if (!$currency) {
            return $this->currencies[$this->currency]['currency_id'];
        } elseif ($currency && isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['currency_id'];
        } else {
            return 0;
        }
    }

    public function getSymbolLeft($currency = '')
    {
        if (!$currency) {
            return $this->currencies[$this->currency]['symbol_left'];
        } elseif ($currency && isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['symbol_left'];
        } else {
            return '';
        }
    }

    public function getSymbolRight($currency = '')
    {
        if (!$currency) {
            return $this->currencies[$this->currency]['symbol_right'];
        } elseif ($currency && isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['symbol_right'];
        } else {
            return '';
        }
    }

    public function getDecimalPlace($currency = '')
    {
        if (!$currency) {
            return $this->currencies[$this->currency]['decimal_place'];
        } elseif ($currency && isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['decimal_place'];
        } else {
            return 0;
        }
    }

    public function getCode()
    {
        return $this->currency;
    }
    
    public function getCurrencies()
    {
        return $this->currencies;
    }

    public function getValue($currency = '')
    {
        if (!$currency) {
            return $this->currencies[$this->currency]['value'];
        } elseif ($currency && isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['value'];
        } else {
            return 0;
        }
    }

    public function has($currency)
    {
        return isset($this->currencies[$currency]);
    }

    /**
     * Setter for the session namespace
     * 
     * @param Zend_Session_Namespace $ns 
     */
    public function setSessionNs(Zend_Session_Namespace $ns)
    {
        $this->sessionNamespace = $ns;
    }

    /**
     * Getter for session namespace
     * 
     * @return  Zend_Session_Namespace
     */
    public function getSessionNs()
    {
        if (null === $this->sessionNamespace) {
            $this->setSessionNs(new Zend_Session_Namespace(__CLASS__));
        }
        return $this->sessionNamespace;
    }

    /**
     * Load any presisted data
     */
    public function loadSession()
    {
        if (isset($this->getSessionNs()->currency)) {
            $this->currency = $this->getSessionNs()->currency;
        }
    }

}