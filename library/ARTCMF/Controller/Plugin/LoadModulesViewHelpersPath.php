<?php

/**
 * ARTCMF_Plugin_LoadModulesViewHelpersPath
 * 
 * This plugin detects if we are in the admininstration area
 * and changes the layout to the admin template.
 * 
 * This relies on the admin route found in the initialization plugin
 *
 * @category   Default
 * @package    ARTCMF_Plugin
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Plugin_LoadModulesViewHelpersPath extends Zend_Controller_Plugin_Abstract
{

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();

        $modules = array_keys(Zend_Controller_Front::getInstance()->getControllerDirectory());
        foreach ($modules as $module) {
            $view->addHelperPath(
                    APPLICATION_PATH . '/modules/' . $module . '/views/helpers', ucfirst($module) . '_View_Helper'
            );
        }
    }

}