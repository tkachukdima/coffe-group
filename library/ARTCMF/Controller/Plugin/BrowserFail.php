<?php

/**
 * ARTCMF_Plugin_AdminContext
 *
 * This plugin detects if we are in the admininstration area
 * and changes the layout to the admin template.
 *
 * This relies on the admin route found in the initialization plugin
 *
 * @category   Default
 * @package    ARTCMF_Plugin
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Plugin_BrowserFail extends Zend_Controller_Plugin_Abstract
{

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (stripos($user_agent, 'MSIE 6.0') !== false && stripos($user_agent, 'MSIE 8.0') === false && stripos($user_agent, 'MSIE 7.0') === false) {
            if (!isset($HTTP_COOKIE_VARS["ie"])) {
                setcookie("ie", "yes", time() + 60 * 60 * 24 * 360);
                header("Location: /ie6/ie6.html");
            }
        }
    }

}
