<?php

/**
 * Translation plugin
 * 
 * Плагин для создания мультиязычности на сайте. 
 * 
 * Метод routeStartup используется для декорирования стандартных объектов маршрутов 
 *   от Zend с целью корректного распознавания языков в url без
 *   необходимости переписывания стандартных объектов маршрутов.
 *   Также мультиязычный декоратор корректно формирует
 *   мультиязычные url
 * Метод routeShutdown инициализирует объект Zend_Translate который используется
 *   для переводов при валидации и остального содержимого сайта
 */
class ARTCMF_Controller_Plugin_Translation extends Zend_Controller_Plugin_Abstract
{

    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        // force default routes if needed
        $routes = $router->addDefaultRoutes()->getRoutes();
        // rewrite each route with it's decorated copy
        foreach ($routes as $name => $route) {
            $router->addRoute($name, new ARTCMF_Controller_Router_Route_MultilingualDecorator($route));
        }
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $config = new Zend_Config_Ini(
                        APPLICATION_PATH . '/configs/application.ini',
                        'production');
        // язык по умолчанию
        $langDefault = $config->languages->default;
        //ключ языка
        $langKey = $config->languages->langKey;
        // текущий язык
        $lang = $request->getParam($langKey);
        $langFile = APPLICATION_PATH . '/languages/' . $lang . '.csv';
        // если файла с переводами нет, назначаем дефолтный файл
        if (!is_file($langFile)) {
            //TODO логировать ошибку нужно
            $lang = $langDefault;
            $langFile = APPLICATION_PATH . '/languages/' . $lang . '.csv';
        }
        $translate = new Zend_Translate(
                        array(
                            'adapter' => 'csv',
                            'content' => $langFile,
                            'locale' => $lang
                ));
        
        
        $langModel = new Default_Model_Language();
        $langList = $langModel->getLanguages();

        Zend_Registry::set('langList', $langList);
        Zend_Registry::set('Current_Lang', 'ru');
        
        
//        // файл с переводами для валидаторов
//        $validatorFile = APPLICATION_PATH . '/languages/' . $lang . '/' . 'Zend_Validate.php';
//        // если файл для валидаторов существует, то добавляем переводы к основным
//        // и для Валидаторов назначаем переводы
//        if (is_file($validatorFile)) {
//            $translateValidators = new Zend_Translate(
//                            array(
//                                'adapter' => 'array',
//                                'content' => $validatorFile,
//                                'locale' => $lang
//                    ));
//            Zend_Validate_Abstract::setDefaultTranslator($translateValidators);
//            $translate->addTranslation(
//                    array(
//                        'content' => $validatorFile,
//                        'locale' => $lang
//            ));
//        }
        Zend_Registry::set('Zend_Translate', $translate);
        
        
    }

}
