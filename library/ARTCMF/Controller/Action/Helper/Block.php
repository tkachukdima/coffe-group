<?php

/**
 *
 * @category   Default
 * @package    ARTCMF_Controller_Helper
 * @copyright  Copyright (c) 2011 Andrew Mae (mae_andrew@hotmail.com)
 * @license    New BSD License
 */
class ARTCMF_Controller_Action_Helper_Block extends Zend_Controller_Action_Helper_Abstract
{

    private $_view;
    private $_classPrefix = 'Block_';
    private $_layoutPrefix = 'blocks';

    public function add($blockName)
    {
        $helperClass = $this->_classPrefix . ucfirst($blockName);

        Zend_Controller_Action_HelperBroker::addHelper(
                new $helperClass(
                        $this->getView()
                )
        );

        //trigger helper init
        Zend_Controller_Action_HelperBroker::getStaticHelper(ucfirst($blockName))->init();
    }

    public function getView()
    {
        if (null !== $this->_view) {
            return $this->_view;
        }

        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view->setScriptPath(APPLICATION_PATH . '/layouts/scripts/' . $this->_layoutPrefix);
        
        $this->_view = $view;
        return $view;
    }

}