<?php

/**
 * Minimalist approach to store last visited url
 *
 * @category   Default
 * @package    ARTCMF_Controller_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Controller_Action_Helper_LastVisited extends Zend_Controller_Action_Helper_Abstract
{

    /**
     * Example:
     * ARTCMF_Controller_Action_Helper_LastVisited::saveThis($this->_request->getRequestUri());
     */
    public static function saveThis($url)
    {
        $lastPg = new Zend_Session_Namespace('history');
        $lastPg->last = $url;
        //echo $lastPg->last;// results in /controller/action/param/foo
    }

    /**
     * I typically use redirect:
     * $this->_redirect(ARTCMF_Controller_Action_Helper_LastVisited::getLastVisited());
     */
    public static function getLastVisited()
    {
        $lastPg = new Zend_Session_Namespace('history');
        if (!empty($lastPg->last)) {
            $path = $lastPg->last;
            $lastPg->unsetAll();
            return $path;
        }

        return ''; // Go back to index/index by default;
    }

}