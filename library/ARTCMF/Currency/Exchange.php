<?php

class ARTCMF_Currency_Exchange implements Zend_Currency_CurrencyInterface
{

    public function getRate($from, $to)
    {
        if ($from !== "USD") {
            throw new Exception('Мы можем обменять только с USD');
        }

        switch ($to) {
            case 'EUR':
                return 2;
            case 'UAH':
                return 8;    
        }

        throw new Exception('Нет возможности обмена $to');
    }

}