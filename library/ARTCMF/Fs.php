<?php

/**
 *  ARTCMF Fs
 * 
 * @category   Default
 * @package    ARTCMF_Debug
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Fs
{

    protected function __construct()
    {
        
    }

    public static function create_file($filename, $content = "", $rewrite = false, $rights = 0777)
    {

        $fileparts = explode(DIRECTORY_SEPARATOR, rtrim($filename, DIRECTORY_SEPARATOR));
        $pathparts = $fileparts;
        unset($pathparts[count($pathparts) - 1]);
        $path = rtrim(implode(DIRECTORY_SEPARATOR, $pathparts), DIRECTORY_SEPARATOR);

        if (ARTCMF_Fs::create_folder($path)) {
            if (file_exists($filename) && !$rewrite) {
                return false;
            } else {
                file_put_contents($filename, $content);
                chmod($filename, $rights);
            }
        } else {
            return false;
        }

        return true;
    }

    public static function create_folder($filename, $rights = 0777)
    {

        $fileparts = explode(DIRECTORY_SEPARATOR, rtrim($filename, DIRECTORY_SEPARATOR));
        $curdir = $fileparts[0] . DIRECTORY_SEPARATOR;
        unset($fileparts[0]);
        foreach ($fileparts as $part) {
            if (file_exists($curdir . $part)) {
                
            } else {
                if (is_writable($curdir)) {
                    mkdir($curdir . $part);
                    chmod($curdir . $part, $rights);
                } else {
                    return false;
                }
            }
            $curdir .= $part . DIRECTORY_SEPARATOR;
        }
        return true;
    }

}