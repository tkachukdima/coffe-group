<?php
/**
 * ARTCMF_View_Helper_SelectionInfo
 *
 * Access authentication data saved in the session
 *
 * @category   Default
 * @package    ARTCMF_View_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
require_once 'Zend/View/Interface.php';

/**
 * AuthInfo helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class ARTCMF_View_Helper_SelectionInfo extends Zend_View_Helper_Abstract
{
    public function SelectionInfo($id)
    {
        $catalogModel = new Default_Model_Catalog();
        return $catalogModel->getSelectionById($id);
    }
}
