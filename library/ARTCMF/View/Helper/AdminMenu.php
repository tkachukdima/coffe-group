<?php

class ARTCMF_View_Helper_AdminMenu extends Zend_View_Helper_Abstract
{

    public function adminMenu()
    {

        $modules = array_keys(Zend_Controller_Front::getInstance()->getControllerDirectory());
        sort($modules);
        $pages = array(
            array(
                'label' => _('Main'),
                'module' => 'default',
                'controller' => 'admin',
                'action' => 'index',
                'route' => 'admin',
                'resource' => 'default:admin',
                'privilege' => 'index',
                'class' => 'dashboard-home',
                'reset_params' => true
            )
        );
        $moduleMenuContainer = array();
        foreach ($modules as $module) {
            $navigationFile = APPLICATION_PATH . '/modules/' . $module . '/configs/navigation.php';
            if (file_exists($navigationFile)) {
                $moduleMenuContainer = include $navigationFile;

                foreach ($moduleMenuContainer as $section) {
                    $pages[] = array_merge($section);
                }
            }
        }


        $navContainerConfig = array(
            array(
                'label' => _('Admin Panel'),
                'module' => 'default',
                'controller' => 'admin',
                'action' => 'index',
                'route' => 'admin',
                'resource' => 'default:admin',
                'privilege' => 'index',
                'reset_params' => true,
                'pages' => $pages
            )
        );

        //    print_r($navContainerConfig);die;

        return new Zend_Navigation($navContainerConfig);
    }

}
