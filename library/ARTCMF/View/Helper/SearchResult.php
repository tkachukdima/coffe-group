<?php
/**
 * ARTCMF_View_Helper_SearchResult
 *
 * Display the search result
 *
 * @category   Default
 * @package    ARTCMF_View_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_View_Helper_SearchResult extends Zend_View_Helper_Abstract
{
    public function searchResult($hit)
    {
        $catalog = new Default_Model_Catalog();
        $product = $catalog->getProductById($hit->product_id);

        if (null !== $product) {
            $category = $catalog->getCategoryById($product->category_id);
            $this->view->product = $product;
            $this->view->category = $category;
            return $this->view->render('index/_result.phtml');
        }
    }
}