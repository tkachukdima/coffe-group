<?php
/**
 * ARTCMF_View_Helper_BaseUrl
 * 
 * 
 * @author     Andrew Mae
 * @category   Default
 * @package    Default_View_Helper
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_View_Helper_BaseUrl extends Zend_View_Helper_Abstract
{ 
    public function baseUrl($file = null) 
    { 
        // Get baseUrl 
        $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl(); 
 
        // Remove trailing slashes 
        $file = ($file !== null) ? ltrim($file, '/\\') : null; 
 
        // Build return 
        $return = rtrim($baseUrl, '/\\') . (($file !== null) ? ('/' . $file) : ''); 
 
        return $return; 
    } 
} 