<?php

class ARTCMF_View_Helper_StorageImage extends Zend_View_Helper_HtmlElement
{

    /**
     * @var int
     */
    protected $_image;

    /**
     * @var array|false
     */
    protected $_attribs;

    /**
     * storageImage
     *
     * @param array $attribs Attributes for the img tag
     * @return ARTCMF_View_Helper_StorageImage
     */
    public function storageImage($image_id = null, $attribs = false)
    {
        $modelImage = new Storage_Model_Image();
        $this->_image = $modelImage->getImageById($image_id);
        $this->_attribs = $attribs;
        return $this;
    }

    /**
     * Get the thumbnail image
     *
     * @return string The img tag
     */
    public function thumbnail()
    {
        if (null !== $this->_image) {
            return $this->_createImgTag($this->_image->thumbnail);
        }
    }

    /**
     * Get the preview image
     *
     * @return string The img tag
     */
    public function preview()
    {
        if (null !== $this->_image) {
            return $this->_createImgTag($this->_image->preview);
        }
    }

    /**
     * Get the detail image
     *
     * @return string The img tag
     */
    public function detail()
    {
        if (null !== $this->_image) {
            return $this->_createImgTag($this->_image->detail);
        }
    }

    /**
     * Get the full image
     *
     * @return string The img tag
     */
    public function full()
    {
        if (null !== $this->_image) {
            return $this->_createImgTag($this->_image->full);
        }
    }

    /**
     * Create the img tag
     *
     * @param string $file The filename to link to
     * @return string The img tag
     */
    protected function _createImgTag($file)
    {
        if ($this->_attribs) {
            $attribs = $this->_htmlAttribs($this->_attribs);
        } else {
            $attribs = '';
        }

        $tag = 'img src="' . $this->view->baseUrl($this->_image->path . '/' . $file) . '?tmp=' . strtotime($this->_image->updated_at) . '" ';
        return '<' . $tag . $attribs . $this->getClosingBracket() . self::EOL;
    }

}