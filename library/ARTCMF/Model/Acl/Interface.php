<?php

/**
 * ARTCMF_Model_Acl_Interface
 *
 * @category   Default
 * @package    ARTCMF_Model_Acl
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
interface ARTCMF_Model_Acl_Interface
{
    public function setIdentity($identity);

    public function getIdentity();

    public function checkAcl($action);

    public function setAcl(ARTCMF_Acl_Interface $acl);

    public function getAcl();
}
