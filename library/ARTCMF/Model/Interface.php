<?php

/**
 * ARTCMF_Model_Interface
 * 
 * All models use this interface
 * 
 * @category   Default
 * @package    ARTCMF_Model
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
interface ARTCMF_Model_Interface
{
    public function __construct($options = null);

    public function getResource($name);

    public function getForm($name);

    public function init();
}
