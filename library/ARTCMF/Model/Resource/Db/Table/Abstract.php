<?php

/**
 * ARTCMF_Model_Resource_Db_Table_Abstract
 * 
 * Provides some common db functionality that is shared
 * across our db-based resources.
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
abstract class ARTCMF_Model_Resource_Db_Table_Abstract extends Zend_Db_Table_Abstract implements ARTCMF_Model_Resource_Db_Interface
{

 
    /**
     * Save a row to the database
     *
     * @param array             $info The data to insert/update
     * @param Zend_DB_Table_Row $row Optional The row to use
     * @return mixed The primary key
     */
    public function saveRow($info, $row = null)
    {
        if (null === $row) {
            $row = $this->createRow();
        }

        $columns = $this->info('cols');

        //print_r($info);
        foreach ($columns as $column) {
            //echo $column;
            if (array_key_exists($column, $info)) {
                $row->$column = $info[$column];
                //echo " = ".$column."<br>";
            }
        }
        //die();

        return $row->save();
    }

}
