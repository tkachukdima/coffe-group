<?php

/**
 * Simple base form class to provide model injection
 *
 * @category   Default
 * @package    ARTCMF_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Form_Abstract extends Zend_Form
{

    /**
     * @var ARTCMF_Model_Interface
     */
    protected $_model;
    protected $_languagesList;

    /**
     * Model setter
     * 
     * @param ARTCMF_Model_Interface $model 
     */
    public function setModel(ARTCMF_Model_Interface $model)
    {
        $this->_model = $model;
    }

    /**
     * Model Getter
     * 
     * @return ARTCMF_Model_Interface 
     */
    public function getModel()
    {
        return $this->_model;
    }

}