<?php

class ARTCMF_Form_Element_BirthDate extends Zend_Form_Element_Xhtml
{

    public $helper = 'formBirthDate';

    public function isValid($value, $context = null)
    {
        if (is_array($value)) {
                        
            if(empty($value['year']) 
                    OR empty($value['month'])
                        OR empty($value['day'])) {
                 $value = null;
            }                
            
            $value = $value['year'] . '-' .
                    $value['month'] . '-' .
                    $value['day'];

            if ($value == '--') {
                $value = null;
            }
        }

        return parent::isValid($value, $context);
    }

    public function getValue()
    {
        if (is_array($this->_value)) {
            $value = $this->_value['year'] . '-' .
                    $this->_value['month'] . '-' .
                    $this->_value['day'];

            if ($value == '--') {
                $value = null;
            }
            $this->setValue($value);
        }

        return parent::getValue();
    }

}