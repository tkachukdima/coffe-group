<?php

/**
 * Description of ARTCMF_RepresentationMapper
 *
 * @category   Default
 * @package    ARTCMF_Core
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_RepresentationMapper {

    /**
     *
     * @var ARTCMF_SpreadsheetAdapter
     */
    public $spreadsheet;

    /**
     *
     * @var ARTCMF_GeocodingAdapter
     */
    public $geocoder;

    public function __construct(ARTCMF_GeocodingAdapter $geocoder, ARTCMF_SpreadsheetAdapter $spreadsheet) {
        $this->spreadsheet = $spreadsheet;
        $this->geocoder = $geocoder;
    }

    /**
     *
     * @param string $name
     * @param string $url
     * @param string $email
     * @param string $address
     * @return Zend_Gdata_Spreadsheets_ListEntry
     */
    function newRepresentation($name, $url, $email, $address) {

        $latAndLong = $this->geocoder->getGeocodedLatitudeAndLongitude($address);
        $payload = array(
            "name" => $fname,
            "url" => $lname,
            "email" => $email,
            "address" => $address,
            "latitude" => $latAndLong[1],
            "longitude" => $latAndLong[0]
        );


        return $this->spreadsheet->insertRow($payload);
    }

}