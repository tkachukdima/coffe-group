<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommentType
 *
 * @author admin
 */
class ARTCMF_Validate_CommentTypeIsset extends Zend_Validate_Abstract
{
    //put your code here
    const TYPE_EXISTS = 'typeExists';

    protected $_messageTemplates = array(
        self::TYPE_EXISTS => 'Тип комментария:"%value%", не существует в системе',
    );
    protected $_modelComment;
    protected $_commentType;
    
    public function __construct() 
    {
        $this->_modelComment = new Comment_Model_Comment();
        $this->_commentType = array_keys($this->_modelComment->getType());
    }
    
    public function isValid($value)
    {
        $this->_setValue($value);
        if (in_array($value, $this->_commentType))
        {
                return true;
        }        
        $this->_error(self::TYPE_EXISTS);
        return false;  
    }

}

