<?php

/**
 * Art-Creative CMF
 * 
 * 
 * This source code is ...

 * 
 * @license		CreativeCommons-Attribution-ShareAlike
 * @link 		http://creativecommons.org/licenses/by-sa/3.0/
 * @package		ARTCMF
 */

/**
 * ARTCMF_Validate_Fullname
 * 
 * Validates fullnames so people like Matthew Weier O'Phinney and D. Keith
 * Casey, Jr. won't have any problems providing comments on the website.
 * 
 * @author 	Andrew Mae <amey.pro@gmail.com>
 * @package	ARTCMF
 * @category	ARTCMF_Validate
 */
class ARTCMF_Validate_Fullname extends Zend_Validate_Abstract {

    const FULLNAME = 'fullname';

    /**
     * @var 	array Error messages used by this validator
     */
    protected $messageTemplates = array(
        self::FULLNAME => "'%value%' is not FULLNAME proof",
    );

    /**
     * Validates the provided name if it matches predefined conditions and 
     * returns TRUE if the name is valid, or returns FALSE if the name doesn't
     * match the conditions.
     * 
     * @param	string $fullname
     * @return	boolean
     * @see 	Zend_Validate_Interface::isValid()
     */
    public function isValid($fullname) {
        $this->_setValue($fullname);

        $check = preg_match('([\"\=\*\;\[\]\#\\\?]+)', $fullname);
        if (1 === $check) {
            $this->_error(self::FULLNAME, $fullname);
            return false;
        }
        return true;
    }

}