<?php

class ARTCMF_Validate_UniqueIdent extends Zend_Validate_Abstract
{
    const IDENT_EXISTS = 'identExists';

    protected $_messageTemplates = array(
        self::IDENT_EXISTS => 'The identifier "%value%" already exists in our system',
    );
    protected $_model;
    protected $_methodIdent;
    protected $_methodId;
    protected $_id;

    /**
     *
     * @param ARTCMF_Model_Interface $model  Модель
     * @param type $methodIdent Имя метода для выборки из базы по ident
     * @param type $methotId Имя метода для выборки из базы по id
     * @param type $id Имя первичного ключа таблицы *_id
     */
    public function __construct(ARTCMF_Model_Interface $model, $methodIdent, $methodId, $id)
    {
        $this->_model = $model;
        $this->_methodIdent = $methodIdent;
        $this->_methodId = $methodId;
        $this->_id = $id;

        if (!method_exists($this->_model, $methodIdent)) {
            throw new ARTCMF_Exception('Method ' . $methodIdent . ' does not exist in model');
        }

        if (!method_exists($this->_model, $methodId)) {
            throw new ARTCMF_Exception('Method ' . $methodId . ' does not exist in model');
        }
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue($value);

        // $found = call_user_func(array($this->_model, $this->_method), $value);
        $currentRow = isset($context[$this->_id]) ? call_user_func(array($this->_model, $this->_methodId), $context[$this->_id]) : null;

        //  $found = $this->_model->getCategoryByIdent($value, $currentRow);
        $found = call_user_func_array(array($this->_model, $this->_methodIdent), array($value, $currentRow));

        if (null === $found) {
            return true;
        }

        $this->_error(self::IDENT_EXISTS);
        return false;
    }

}
