<?php

/**
 * ARTCMF_Acl_Recources_Exception
 *
 * @category   Default
 * @package    ARTCMF_Acl
 * @copyright  Copyright (c) 2011 Andrew Mae (mae_andrew@hotmail.com)
 * @license    New BSD License
 */
class ARTCMF_Acl_Recources_Exception extends ARTCMF_Exception
{

}
