<?php

/**
 * Default_Bootstrap
 *
 * @category   Bootstrap
 * @package    Default_Bootstrap
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Application_Module_Bootstrap extends Zend_Application_Module_Bootstrap
{

    protected $_moduleDirectory = null;
    protected $_view = null;

    public function __construct($application)
    {
        $options = $application->getOptions();
        $this->setOptions($options);

        parent::__construct($application);
    }

    public function _initModuleResourceAutoloader()
    {
        $this->getResourceLoader()->addResourceTypes(array(
            'modelResource' => array(
                'path' => 'models/resources',
                'namespace' => 'Resource',
            ),
            'modelFilter' => array(
                'path' => 'models/filter',
                'namespace' => 'Filter',
            )
        ));
    }
   
    /**
     * Add custom module routes to the router
     */
    protected function _initRoutes()
    {
        $routes_file = $this->_getModuleDirectory() . '/configs/routes.php';

        $router = $this->frontController->getRouter();

        if (file_exists($routes_file)) {
            $routes_array = include $routes_file;
            foreach ($routes_array as $routeName => $route) {
                $router->addRoute($routeName, $route);
            }
        } else {
            $routes_config = $this->_getModuleDirectory() . '/configs/routes.ini';

            if (!file_exists($routes_config))
                return false;

            $config = new Zend_Config_Ini($routes_config, $this->getEnvironment());

            $router->addConfig($config, 'routes');
        }
    }

    protected function _getModuleDirectory()
    {
        if (is_null($this->_moduleDirectory)) {
            $this->bootstrap('frontController');
            $this->_moduleDirectory = $this->frontController->getModuleDirectory();
        }

        return $this->_moduleDirectory;
    }
}
