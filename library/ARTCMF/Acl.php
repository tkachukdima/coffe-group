<?php

/**
 *  ARTCMF Acl Class
 * 
 * @category   Default
 * @package    ARTCMF_Core
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_Acl extends Zend_Acl implements ARTCMF_Acl_Interface
{

    protected $_modules = null;
    protected $_roles = null;

    public function __construct()
    {

        $this->buildRoles();

        $this->buildResources();

        $this->buildPrivileges();
    }

    private function buildRoles()
    {
        $parentRole = null;

        $roles = $this->getRolesList();

        foreach ($roles as $role) {
            if (is_null($parentRole))
                $this->addRole(new Zend_Acl_Role($role));
            else
                $this->addRole(new Zend_Acl_Role($role), $parentRole);
            $parentRole = $role;
        }
    }

    private function buildResources()
    {
        $this->add(new Zend_Acl_Resource('Admin'));
        $this->add(new Zend_Acl_Resource('filemanager'));

        foreach ($this->getModulesList() as $module) {
            $resourcesFile = APPLICATION_PATH . '/modules/' . $module . '/configs/resources.php';
            if (file_exists($resourcesFile)) {
                $resourcesArray = include $resourcesFile;
                $this->add(new Zend_Acl_Resource($module));
                foreach ($resourcesArray as $controller) {
                    $this->add(new Zend_Acl_Resource($module . ':' . $controller), $module);
                }
            }
        }
    }

    private function buildPrivileges()
    {
        // Deny privileges by default; i.e., create a whitelist
        $this->deny();

        foreach ($this->getModulesList() as $module) {
            $privilegesFile = APPLICATION_PATH . '/modules/' . $module . '/configs/privileges.php';
            if (file_exists($privilegesFile)) {
                $privilegesArray = include $privilegesFile;
                foreach ($privilegesArray as $role => $accesses) {
                    foreach ($accesses as $access => $controllers) {
                        foreach ($controllers as $controller => $privileges) {
                            $this->$access($role, $module . ':' . $controller, $privileges);
                        }
                    }
                }
            }
        }


        $this->allow('Redactor', 'filemanager');
        $this->allow('Root');
    }

    private function getRolesList()
    {
        if (is_null($this->_roles)) {
            $rolesFile = APPLICATION_PATH . '/configs/roles.php';
            if (!file_exists($rolesFile))
                throw new ARTCMF_Exception('Roles file not found');

            $this->_roles = include $rolesFile;
        }
        return $this->_roles;
    }

    private function getModulesList()
    {
        if (is_null($this->_modules))
            $this->_modules = array_keys(Zend_Controller_Front::getInstance()->getControllerDirectory());
        sort($this->_modules);
        return $this->_modules;
    }

}
