<?php

/**
 * Description of ARTCMF_GetClientIp
 *
 * @category   Default
 * @package    ARTCMF_Core
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class ARTCMF_ClientIp
{

    public static function getClientIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}