//ArtCmf Library
//2012-11-08
	
$.cmf = {
	  
    langList : [],

    switchFeatureType:
    function() {
		
        $('#feature_type').live('change', function() {

            var feature_type = $(this).val();
                       
            switch(feature_type)
            {
                case 'M':
                case 'S':
                case 'N':
                    $('#variants-tab-button').show();
                    break;
                case 'C':
                case 'T':
                case 'O':
                case 'D':
                    $('#variants-tab-button').hide();                    
                    break;
                default:
                    $('#variants-tab-button').hide();      
            }


            return false;
        });		

    },
    
    deleteCategory: function(link){
      
        $('#$delete-feature-category').live('change', function() {
            $.ajax({
                url: $(this).attr('href'),  
                dataType : "json",                     
                success: function (data) { 
                    if(data.success){
                        link.closest('tr').remove();
                    } else {
                        alert(data.message);
                    }
                } 

            });
            return false;
        });
    },
    
    getNumberOfNewOrders: function(){
      
      $(function() {
            $.ajax({
                url: '/admin/shop/order/get-number/status/0',  
                dataType : "json",                     
                success: function (data) { 
                    if(data.success){
                        $('.dashboard-order').append(' (новых: ' + data.count + ')');
                        if(0 < parseInt(data.count)){
                            $('.dashboard-order').attr('style', 'color: #1399EE; font-weight: bold;')
                        }
                    } else {
                        alert(data.message);
                    }
                } 
            });
        });
    },
    
    deleteRow: function(){
             
        $('.delete-row').live('click', function() {
            var link = $(this);
            $.ajax({
                url: link.attr('href'),  
                dataType : "json",                     
                success: function (data) { 
                    if(data.success){
                        link.closest('tr').remove();
                    } else {
                        alert(data.message);
                    }
                } 
                
            });
            return false;
        });
    },


    markFeatureVariantAsDeleted: function(link, index){
        $(link).after('<input type="hidden" name="variants['+index+'][deleted]" value="true">');    
        $(link).closest('tr').hide();
        return false;
    },
    
    addNewFeatureVariant: function(){
        var new_index = $('#feature-categories-list tr').length;
        var row =  
        '<tr>' +
        '<td>' +
        '<input type="text" name="variants[' + new_index + '][position]" value="" size="4" class="input-text-short">' +
        '</td>' +
        '<td class=col-first">';
            
        for(var i=0; i<$.cmf.langList.length; i++){
            row += '<input type="text" name="variants[' + new_index + '][variant]['+$.cmf.langList[i]+']" value="" class="input-text-large cm-feature-value input-text-selected">';
        }
            
        row += '</td>'+
        '<td class="row-nav"> '+       
        '<a class="table-delete-link feature-delete-variant"  onclick="return $.cmf.markFeatureVariantAsDeleted(this, ' + new_index + ');" href="#">Удалить</a>'+
        '</td>'+
        '</tr>';
        $('#feature-categories-list').append(row);
        return false;
    },
    
    markOptionVariantAsDeleted: function(link, index){
        $(link).after('<input type="hidden" name="variants['+index+'][deleted]" value="true">');    
        $(link).closest('tr').hide();
        return false;
    },
    
    addNewOptionVariant: function(){
        var new_index = $('#options-variants-list tr').length;
        var row =  
        '<tr>' +
        '<td>' +
        '<input type="text" name="variants[' + new_index + '][position]" value="" size="4" class="input-text-short">' +
        '</td>' +
        '<td class=col-first">';
            
        for(var i=0; i<$.cmf.langList.length; i++){
            row += '<input type="text" name="variants[' + new_index + '][variant_name]['+$.cmf.langList[i]+']" value="" class="input-text-large cm-options-value input-text-selected">';
        }
            
        row += '</td>'+
       // '<td>' +
       // '<input type="text" name="variants[' + new_index + '][modifier]" value="" size="5" class="input-text">&nbsp;'+
       // '<select name="variants[' + new_index + '][modifier_type]">'+
       // '<option value="A">$</option>'+
       // '<option value="P">%</option>'+
       // '</select>'+
       // '</td>'+  
        '<td class="row-nav"> '+       
        '<a class="table-delete-link options-delete-variant"  onclick="return $.cmf.markOptionVariantAsDeleted(this, ' + new_index + ');" href="#">Удалить</a>'+
        '</td>'+
        '</tr>';
        $('#options-variants-list').append(row);
        return false;
    },
    
    switchOptionType:
    function() {
		
        $('#option_type').live('change', function() {

            var option_type = $(this).val();
                       
            switch(option_type)
            {
                case 'S':
                case 'R':
                    $('#variants-tab-button-many').show();
                    break;
                case 'C':
                   // $('#variants-tab-button-one').show();
                    $('#variants-tab-button-many').hide();                
                    break;
                case 'I':
                case 'T':
                    $('#variants-tab-button-many'/*, #variants-tab-button-one'*/).hide();                 
                    break;
                default:
                    $('#variants-tab-button-many'/*, #variants-tab-button-one'*/).hide();   
            }
            return false;
        });		

    }

	
}
