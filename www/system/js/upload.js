$(document).ready(function() { 
    
    var artUploader = new artUpload();
        
    $('#imagefile').live('change', function() {        
        artUploader.uploadImg();            
    });
        
    $('#deleteimage').live('click', function() {
        artUploader.deleteImg($('#deleteimage').attr('href'))
        return false;
    });
        
}); 

function artUpload () {   
    this.deleteImg = deleteImg;
    this.uploadImg = uploadImg;
}

$.ajaxSetup({
    cache: false // for ie
});

function uploadImg()
{
    $("#imageform").ajaxForm({
        beforeSubmit: function(a,f,o) {
            o.dataType = 'json';
            $("#preview").attr('src',"/layout/admin/loading.gif");
            $("#preview").show();
        },
        success: function(data) {
            if(data.success){
                $("#preview").attr('src',data.image);
                $("#preview").attr('alt',data.title);
                $("#deleteimage").attr('href','/storage/image/delete/image_id/'+data.image_id);
                $("#deleteimage").show();
                $("#image_id").val(data.image_id);
            }
        }
    }).submit();
}

function deleteImg(href)
{
    if('' != href) {
        $.ajax({
            url: href,  
            dataType : "json",                     
            success: function (data) { 
                if(data.success){
                    $("#preview").hide();                    
                    $("#preview").attr('src',"/layout/admin/loading.gif");
                    $("#deleteimage").hide();
                    $("#deleteimage").attr('href');
                    $("#image_id").val('');
                }
            } 
            
        });
    }
}