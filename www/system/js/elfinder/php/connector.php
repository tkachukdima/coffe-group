<?php

error_reporting(E_ALL); // Set E_ALL for debuging

$paths = array(
    realpath(dirname(__FILE__) . '/../../../../../library'),
    '.',
);
set_include_path(implode(PATH_SEPARATOR, $paths));


//определяем текущий год
defined('CURRENT_YEAR')
    || define('CURRENT_YEAR', date('Y'));

defined('APPLICATION_PATH')
        or define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../../../../application'));
defined('APPLICATION_ENV')
        or define('APPLICATION_ENV', getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production');


require_once APPLICATION_PATH . '/../library/ZendX/Loader/AutoloaderFactory.php';
ZendX_Loader_AutoloaderFactory::factory(array(
    'ZendX_Loader_ClassMapAutoloader' => array(
        APPLICATION_PATH . '/../library/autoload_classmap.php',
        APPLICATION_PATH . '/../application/autoload_classmap.php',
    ),
    'ZendX_Loader_StandardAutoloader' => array(
        'prefixes' => array(
            'ARTCMF' => APPLICATION_PATH . '/../library/ARTCMF',
        ),
        'fallback_autoloader' => true,
    ),
));


require_once 'Zend/Loader/Autoloader.php';
require_once 'Zend/Application.php';

$application = new Zend_Application(
                APPLICATION_ENV,
                APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap();

$acl  = new ARTCMF_Acl();

$auth = Zend_Auth::getInstance();
if (!$auth->hasIdentity()) {
    return 'Guest';
}



try {
    if ($acl->isAllowed($auth->getIdentity()->role, 'filemanager'))
        $allow = true;
} catch (Exception $e) {
    $allow = false;
}

if (!$allow)
        die('Пшел вон!');

include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderConnector.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinder.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeDriver.class.php';
include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeLocalFileSystem.class.php';
// Required for MySQL storage connector
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeMySQL.class.php';
// Required for FTP connector support
// include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeFTP.class.php';


/**
 * Simple function to demonstrate how to control file access using "accessControl" callback.
 * This method will disable accessing files/folders starting from  '.' (dot)
 *
 * @param  string  $attr  attribute name (read|write|locked|hidden)
 * @param  string  $path  file path relative to volume root directory started with directory separator
 * @return bool|null
 **/

function access($attr, $path, $data, $volume) {
	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
		:  null;                                    // else elFinder decide it itself
}

$opts = array(
    'debug' => true,
	'roots' => array(
		array(
			'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
			'path'          => '../../../../upload/',         // path to files (REQUIRED)
			'URL'           => '/upload/', // URL to files (REQUIRED)
			'accessControl' => 'access'             // disable and hide dot starting files (OPTIONAL)
		)
	)
);

// run elFinder
$connector = new elFinderConnector(new elFinder($opts));
$connector->run();

