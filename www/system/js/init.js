function checkSubmit(e)
{
    if(e && e.keyCode == 13)
    {
        serchSubmit();
    }
}

function serchSubmit()
{
    if (jQuery('input#price').val() == 'Поиск по цене...') 
    {
        jQuery('input#price').val('')
        }
    if (jQuery('input#name').val() == 'Поиск по названию...') 
    {
        jQuery('input#name').val('')
        }
    if (jQuery('input#article').val() == 'Поиск по артикулу...') 
    {
        jQuery('input#article').val('')
        }
          
    document.forms[0].submit();
    return true;    
}

$(function() {
    
    
    jQuery('h4.white').corner("top");
    jQuery('h4.yellow').corner("top");
    jQuery('div.box-container').corner("bottom");
    
    
    jQuery.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '<Пред',
        nextText: 'След>',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        changeMonth: true,                
        changeYear: true,
        showAnim: 'fold'
    };
    jQuery.datepicker.setDefaults(jQuery.datepicker.regional['ru']);

    jQuery.timepicker.regional['ru'] = {
        timeOnlyTitle: 'Выберите время',
        timeText: 'Время',
        hourText: 'Часы',
        minuteText: 'Минуты',
        secondText: 'Секунды',
        currentText: 'Теперь',
        closeText: 'Закрыть',
        ampm: false,
        timeFormat: 'hh:mm:ss'
    };
    jQuery.timepicker.setDefaults(jQuery.timepicker.regional['ru']);
    jQuery("#date_post").datetimepicker({});
    jQuery("#date_start").datetimepicker({});
    jQuery("#date_end").datetimepicker({});
    jQuery("#date").datetimepicker({});
    jQuery("#subscription_date_end").datetimepicker({});
    
    jQuery(".date_input").datetimepicker({});
    
    
    updateClock(); 
    setInterval('updateClock()', 1000 );
    
});


function updateClock()
{
    var currentTime = new Date();
    var currentHours = currentTime.getHours();
    var currentMinutes = currentTime.getMinutes();
    var currentSeconds = currentTime.getSeconds();
    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
    currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;   
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds;
    document.getElementById("today_clock").firstChild.nodeValue = currentTimeString;
}

/*
$(function() {
    $('a.lightbox').lightBox({
        imageLoading: '/layout/lightbox/loading.gif',
        imageBtnClose: '/layout/lightbox/close.gif',
        imageBtnPrev: '/layout/lightbox/prev.gif',
        imageBtnNext: '/layout/lightbox/next.gif'
    });
});*/

