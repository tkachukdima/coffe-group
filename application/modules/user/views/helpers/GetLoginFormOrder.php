<?php

class User_View_Helper_GetLoginFormOrder extends Zend_View_Helper_Abstract
{

    public function getLoginFormOrder()
    {
        $userModel = new User_Model_User();

        $form = $userModel->getForm('Login');
        $form->setAction('/user/auth/authenticate');
        $form->setMethod('post');
        $form->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/user/login_order.phtml')),
            'Form'
        ));

        return $form;
    }

}