<?php

class User_View_Helper_Subscriptions extends Zend_View_Helper_Abstract
{

    protected $subscriptions;
    protected $userSubscriptions = array();

    public function Subscriptions($user_id = null)
    {
        $subscriptionModel = new User_Model_Subscription();

        $this->subscriptions = $subscriptionModel->getSubscriptions();
        
        if (!is_null($user_id)) {
            foreach ($subscriptionModel->getSubscriptionsByUserId($user_id) as $value) {
                $this->userSubscriptions[] = $value->subscription_id;
            }
        }

        return $this;
    }

    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    public function getUserSubscriptions()
    {
        return $this->userSubscriptions;
    }

}