<?php

/**
 * User_Bootstrap
 *
 * @category   Bootstrap
 * @package    Email_Bootstrap
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Bootstrap extends ARTCMF_Application_Module_Bootstrap
{

    public function __construct($application)
    {
        $options = $application->getOptions();
        $this->setOptions($options);

        parent::__construct($application);
    }

    public function _initModuleResourceAutoloader()
    {
        $this->getResourceLoader()->addResourceTypes(array(
            'modelResource' => array(
                'path' => 'models/resources',
                'namespace' => 'Resource',
            ),
            'modelFilter' => array(
                'path' => 'models/filter',
                'namespace' => 'Filter',
            )
        ));
    }

    protected function _initRoutes()
    {
        $this->bootstrap('frontController');

        $router = $this->frontController->getRouter();

        $routes_file = APPLICATION_PATH . '/modules/user/configs/routes.php';
        if (file_exists($routes_file)) {
            $routes_array = include $routes_file;
            foreach ($routes_array as $routeName => $route) {
                $router->addRoute($routeName, $route);
            }
        } 
    }
    
    protected function _initSauth() {
        
        /**
         * @see application.ini
         */
        $siteDir = $this->getOption('siteDir');
        $siteUrl = $this->getOption('siteUrl');
        
        $sauthConf['google'] = array(
            'id' => 'https://www.google.com/accounts/o8/id',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/google',
            'exchangeExtension' => array(
                'openid.ns.ax' => 'http://openid.net/srv/ax/1.0',
                'openid.ax.mode' => 'fetch_request',
                'openid.ax.type.email' => 'http://axschema.org/contact/email',
                'openid.ax.required' => 'email',
            ),
        );
        
        $sauthConf['twitter'] = array(
            'consumerKey' => '',
            'consumerSecret' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/twitter',
        );
          
        $sauthConf['facebook'] = array(
            'consumerId' => '116284635202007',
            'consumerSecret' => 'eb4be43c7aa1237633af6e17607d9049',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/facebook',
            'display' => ARTCMF_Auth_Adapter_Facebook::DISPLAY_POPUP,
            'scope' => array(
                 'user_about_me', 'email',
            ),
        );
        
        $sauthConf['vkontakte'] = array(
            'consumerId' => '3266711',
            'consumerSecret' => 'cJkEGNO3SyM1PYIIdtSh',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/vkontakte',
            'userFields' => array('uid', 'first_name', 'last_name', 'nickname', 'sex')
        );
        
        $sauthConf['skyrock'] = array(
            'consumerKey' => '',
            'consumerSecret' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/skyrock',
        );
        
        $sauthConf['mailru'] = array(
            'consumerId' => '',
            'privateKey' => '',
            'consumerSecret' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/mailru',
        );
        
        $sauthConf['foursquare'] = array(
            'consumerSecret' => '',
            'consumerId' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/foursquare',
        );
        
        $sauthConf['flickr'] = array(
            'consumerKey' => '',
            'consumerSecret' => '',
            'userAuthorizationUrl' => 'http://flickr.com/services/auth/',
            'permission' => ARTCMF_Auth_Adapter_Flickr::PERMS_READ,
        );
        
        $sauthConf['gowalla'] = array(
            'consumerSecret' => '',
            'consumerId' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/gowalla',
        );
        
        $sauthConf['github'] = array(
            'consumerSecret' => '',
            'consumerId' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/github',
        );
        
        $sauthConf['linkedin'] = array(
            'consumerKey' => '',
            'consumerSecret' => '',
            'callbackUrl' => $siteUrl . $siteDir . '/user/auth/authenticate/by/linkedin',
            'scope' => array(
                 'r_fullprofile', 'r_emailaddress', 
            ), // list of the available permissions https://developer.linkedin.com/documents/authentication#granting
            'userFields' => array('id', 'first-name', 'last-name') // list of the available fields https://developer.linkedin.com/documents/profile-fields
        );
        
        Zend_Registry::set('sauthConf', $sauthConf);
        
    }    
}
