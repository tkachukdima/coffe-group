<?php

/**
 * User_Model_Session
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Model_Session extends ARTCMF_Model_Acl_Abstract
{

    /**
     * Get User by their id
     * 
     * @param  int $session_id
     * @return null|User_Resource_User_Item 
     */
    public function getSessionById($session_id)
    {
        return $this->getResource('Session')->getSessionById($session_id);
    }

    /**
     * Get User by their id
     * 
     * @param  int $id
     * @return null|User_Resource_User_Item 
     */
    public function getSessionByUserId($user_id)
    {
        return $this->getResource('Session')->getSessionByUserId((int) $user_id);
    }
    
    public function clearSessionByUserId($user_id)
    {
        $userSessions = $this->getResource('Session')->getSessionsByUserId((int) $user_id);
        if (0 < count($userSessions))
            foreach ($userSessions as $userSession) {
                $userSession->delete();
            }
    }

    
    
    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     * 
     * @return string The resource id 
     */
    public function getResourceId()
    {
        return 'Session';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     * 
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Guest', $this)
                    ->allow('Member', $this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     * 
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}