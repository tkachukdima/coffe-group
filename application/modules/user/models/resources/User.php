<?php

/**
 * User_Resource_User
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Resource_User extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name     = 'user';
    protected $_primary  = 'user_id';
    protected $_rowClass = 'User_Resource_User_Item';

    public function getUserById($id)
    {
        return $this->find($id)->current();
    }

    public function getUserByEmail($email, $ignoreUser = null)
    {
        $select = $this->select();
        $select->where('email = ?', $email);
         
        if (!is_null($ignoreUser) AND !empty($ignoreUser->email)) {
            $select->where('email <> ?', $ignoreUser->email);
        }
        
        return $this->fetchRow($select);
    }
    
     public function getUserByHash($hash)
    {
        $select = $this->select();
        $select->where('forgot_hash = ?', $hash);

        return $this->fetchRow($select);
    }
    
    public function getUserByConfirmationHash($confirmation_hash)
    {
        $select = $this->select();
        $select->where('confirmation_hash = ?', $confirmation_hash);

        return $this->fetchRow($select);
    }

    public function getUserBySubScription($subs_status)
    {
        $select = $this->select();
        $select->where('subscription = ?', $subs_status);

        return $this->fetchAll($select);
    }

    public function getUsers($paged = null, $order = null)
    {
        $select = $this->select();

        if (true === is_array($order)) {
            $select->order($order);
        }

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count   = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from('user', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'));
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage(30)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }
    
    public function getEndingSubscriptions($period)
    {
        $select = $this->select()
                ->where("subscription_date_end < CURRENT_TIMESTAMP + INTERVAL {$period} DAY")
                ->where("notified = 0")
                ->where("trial = 2");
         return $this->fetchAll($select);
    }
    
    public function getEndedSubscriptions()
    {
        $select = $this->select()
                ->where("subscription_date_end < CURRENT_TIMESTAMP + INTERVAL 0 DAY");
         return $this->fetchAll($select);
    }

}