<?php

/**
 * User_Resource_Profile
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Resource_Profile extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name     = 'profile';
    protected $_primary  = 'user_id';
    protected $_rowClass = 'User_Resource_Profile_Item';

    public function getProfileByUserId($user_id)
    {
        return $this->find($user_id)->current();
    }

}