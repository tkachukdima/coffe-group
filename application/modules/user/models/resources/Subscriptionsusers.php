<?php

class User_Resource_Subscriptionsusers extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'subscriptions_users';
    protected $_primary = array('subscription_id', 'user_id');
    protected $_rowClass = 'User_Resource_Subscriptionsusers_Item';

    public function getSubscriptionsUsers($subscription_id, $user_id)
    {
        return $this->find($subscription_id, $user_id)->current();
    }
    
    public function removeSubscriptionsByUserId($user_id)
    {
        $where = $this->getAdapter()->quoteInto("user_id = ?", (int) $user_id);
        $this->delete($where);
    }
    
    public function getEndingSubscriptions($period)
    {
        $select = $this->select()
                ->where("date_end < CURRENT_TIMESTAMP + INTERVAL {$period} DAY")
                ->where("notified = 0");
         return $this->fetchAll($select);
    }
    
    public function getFinishedSubscriptions()
    {
        $select = $this->select()
                ->where("date_end < CURRENT_TIMESTAMP");
         return $this->fetchAll($select);
    }

}
