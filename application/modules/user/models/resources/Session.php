<?php

/**
 * User_Resource_Session
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Resource_Session extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'session';
    protected $_primary = array('session_id', 'save_path', 'name');
    protected $_rowClass = 'User_Resource_Session_Item';

    public function getSessionById($session_id)
    {
        $select = $this->select()->where('session_id = ?', $session_id);

        return $this->fetchRow($select);
    }

    public function getSessionsByUserId($user_id)
    {
        $select = $this->select()->where('user_id = ?', (int) $user_id);

        return $this->fetchAll($select);
    }

    public function clearSessionByUserId($user_id)
    {
        $where = $this->getAdapter()->quoteInto("user_id = ?", (int) $user_id);
        $this->delete($where);
    }

}