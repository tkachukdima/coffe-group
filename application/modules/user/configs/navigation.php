<?php

return array(
    array(
        'label' => _('Users'),
        'module' => 'user',
        'controller' => 'management',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'user:management',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-user',
        'pages' => array(
            array(
                'label' => _('Users'),
                'module' => 'user',
                'controller' => 'management',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'user:management',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit user'),
                        'module' => 'user',
                        'controller' => 'management',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'user:management',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit user'),
                        'module' => 'user',
                        'controller' => 'management',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'user:management',
                        'privilege' => 'save',
                        'reset_params' => true
                    ),
                )
            ),
            array(
                'label' => _('Add User'),
                'module' => 'user',
                'controller' => 'management',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'user:management',
                'privilege' => 'add',
                'reset_params' => true
            ),
//            array(
//                'label' => _('Subscriptions'),
//                'module' => 'user',
//                'controller' => 'subscription',
//                'action' => 'list',
//                'route' => 'admin',
//                'resource' => 'user:subscription',
//                'privilege' => 'list',
//                'reset_params' => true,
//                'pages' => array(
//                    array(
//                        'label' => _('Edit subscription'),
//                        'module' => 'user',
//                        'controller' => 'subscription',
//                        'action' => 'edit',
//                        'route' => 'admin',
//                        'resource' => 'user:subscription',
//                        'privilege' => 'edit',
//                        'reset_params' => true,
//                    )
//                )
//            ),
//            array(
//                'label' => _('Add subscription'),
//                'module' => 'user',
//                'controller' => 'subscription',
//                'action' => 'add',
//                'route' => 'admin',
//                'resource' => 'user:subscription',
//                'privilege' => 'add',
//                'reset_params' => true
//            )
        )
    )
);