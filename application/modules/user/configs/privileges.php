<?php

return array(
    'Guest' => array(
        'allow' => array(
            'auth' => array(
                'forgot',
                'login',                
                'authenticate',
                'register-ajax',
                'register',
                'complete-registration',
                'check'
            )
        )
    ),
    'Member' => array(
        'allow' => array(
            'index' => array(
                'index',
                'save',
                'settings',
                'subscription'
            ),
            'auth' => array(
                'logout'
            ),
//            'subscription' => array(
//                'index',
//                'subscribe',
//                'subscribe-paypal'
//        ),
        ),
        'deny' => array(
            'auth' => array(
                'forgot',
                'login',
                'authenticate',
                'register'
            )
        )
    ),
    'Redactor' => array(
        'allow' => array(
            'management' => array(
                'index',
                'list',
                'edit',
                'add',
                'save',
                'delete'
            ),
//            'subscription' => array(
//                'list',
//                'edit'
//            ),
        )
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);