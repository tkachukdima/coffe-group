<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserLogin
 *
 * @author admin
 */
class User_Form_UserLogin extends User_Form_Login
{
    public function init()
    {
        //call the parent init
        parent::init();    
        $this->setAttrib('onsubmit', 'return SendForm(this);');
        $this->setAttrib('action', '/user/authenticateajax');
        $this->getElement('login')->removeDecorator('DtDdWrapper');
        $this->addElement('hash', 'no_csrf', array(
            'salt' => 'unique',
            'timeout' => 300,
            'ignore' => false,
            'required' => true,
        ));
        $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');
    }
}
