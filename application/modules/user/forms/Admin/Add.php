<?php

/**
 * User_Form_Admin
 *
 * The edit user form for admin user
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Admin_Add extends User_Form_Base
{

    public function init()
    {
        //call the parent init
        parent::init();

        // customize

//        $this->getElement('mobilephone')->setRequired(false);
        $this->getElement('passwd')->setRequired(false);
        $this->getElement('passwdVerify')->setRequired(false);
        $this->removeElement('user_id');
        $this->getElement('submit')->setLabel(_('Save'));
        
        
    }

}