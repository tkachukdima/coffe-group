<?php

/**
 * The base user form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators
        $this->addPrefixPath(
                'ARTCMF_Form', APPLICATION_PATH . '/../library/ARTCMF/Form/'
        );

        // add path to custom validators
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );
        $this->addElementPrefixPath(
                'User_Validate', APPLICATION_PATH . '/modules/user/models/validate/', 'validate'
        );
/*

        $this->addElement('radio', 'gender', array(
            'required'     => false,
            'label'        => _('Gender'),
            'multiOptions' => array(
                User_Model_User::GENDER_MALE   => _('Male'),
                User_Model_User::GENDER_FEMALE => _('Female')
            ),
            'value' => 1
        ));
*/
        $this->addElement('text', 'firstname', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty'),
            ),
            'required' => true,
            'label'    => _('Firstname'),
        ));
              
/*
        $this->addElement('text', 'patronymicname', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty')
            ),
            'required' => false,
            'label'    => _('Patronymic name'),
        ));
*/
        $this->addElement('text', 'lastname', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty')
            ),
            'required' => false,
            'label'    => _('Lastname'),
        ));
  
        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress'),
                array('UniqueEmail', false, array($this->getModel())),
            ),
            'required' => true,
            'label'    => _('Email')            
        ));

/*
        $this->addElement('BirthDate', 'date_of_birthday', array(
            'required' => false,
            'label'    => _('Date of birthday')
        ));
*/
        $this->addElement('password', 'passwd', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('StringLength', true, array(6, 128))
            ),
            'required' => true,
            'label'    => _('Password'),
           
        ));

        $this->addElement('password', 'passwdVerify', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                'PasswordVerification',
            ),
            'required' => true,
            'label'    => _('Password verification'),

        ));
/*
        $this->addElement('text', 'country', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label'    => _('Country'),
        ));
*/
        $this->addElement('text', 'city', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'label'    => _('City')
        ));
/*
        $this->addElement('text', 'zipcode', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label'    => _('ZIP code'),
        ));
*/
        $this->addElement('textarea', 'address', array(
            'filters' => array('StringTrim'),
            'cols'     => 30,
            'rows'     => 2,
            'required' => false,
            'label'    => _('Address')
        ));

        $this->addElement('text', 'telephone', array(
            'filters' => array('StringTrim'),
            'required' => false,
            'label' => _('Telephone'),
            'decorators' => array(array('ViewHelper')),
             'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Telephone'))
            )
        ));

//        $this->addElement('text', 'mobilephone', array(
//            'filters' => array('StringTrim'),
//            'required' => true,
//            'label'    => _('Mobile phone'),
//        ));





//
//        $this->addElement('checkbox', 'subscription', array(
//            //'required'   => true,
//            'label' => _('Subscription'),
//           ));

        $this->addElement('select', 'role', array(
            'required'     => true,
            'label'        => _('Role'),
            'multiOptions' => User_Model_User::getRolesOptions(),
        ));


        $this->addElement('select', 'status', array(
            'required'     => true,
            'label'        => _('Status'),
            'multiOptions' => array('0' => _('Inactive'), '1' => _('Active')),
        ));


        $this->addElement('submit', 'submit', array(
            'required' => false,
            'ignore'   => true
        ));

        $this->addElement('hidden', 'user_id', array(
            'filters' => array('StringTrim'),
            'required'   => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag'   => 'dd', 'class' => 'noDisplay')))
        ));


//        $this->addDisplayGroup(array('email', 'passwd', 'passwdVerify'), 'general_info', array('legend' => _('General info')));
//        $this->addDisplayGroup(array('gender', 'firstname', 'patronymicname', 'lastname', 'date_of_birthday'), 'personal_info', array('legend' => _('Personal info')));
//        $this->addDisplayGroup(array('country', 'city', 'zipcode', 'address', 'mobilephone', 'submit'), 'contact_info', array('legend' => _('Contact info')));
    }

}
