<?php

/**
 * User_Form_LoginAdmin
 * 
 * The login form
 * 
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Login_Admin extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $this->setAttrib('id', 'account_login_admin');

        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress'),
            ),
            'required' => true,
            'label'    => _('Email'),
        ));

        $this->addElement('password', 'passwd', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('StringLength', true, array(6, 128))
            ),
            'required' => true,
            'label'    => _('Password'),
        ));

        $this->addElement('captcha', 'captcha', array(
            'label'   => _('Enter the code'),
            'captcha' => array(
                'captcha'        => 'Image',
                'wordLen'        => 4,
                'font'           => 'fonts/arial.ttf',
                'imgDir'         => './images/captcha',
                'imgUrl'         => '/images/captcha',
                'fontsize'       => '14',
                'width'          => 70,
                'height'         => 35,
                'timeout'        => 120,
                'expiration'     => 0,
                'imgAlt'         => 'captcha',
                'DotNoiseLevel'  => 4,
                'LineNoiseLevel' => 4
            )
                )
        );
        $this->captcha->addDecorator('Label', array('tag'            => 'dt', 'requiredSuffix' => ' <span class="required">*</span> ', 'escape'         => false));


        $this->addElement('submit', 'login', array(
            'required' => false,
            'ignore'   => true,
            'label'    => _('Login'),
        ));
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag'   => 'dl', 'class' => 'zend_form')),
            array('Description', array('placement' => 'prepend', 'class'     => 'error')),
            'Form'
        ));
/*
        $this->addElement('hash', 'no_csrf', array(
            'salt'     => 'unique',
            'timeout'  => 300,
            'ignore'   => false,
            'required' => true,
        ));
        $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');*/
    }

}
