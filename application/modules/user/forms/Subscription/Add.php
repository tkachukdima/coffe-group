<?php

class User_Form_Subscription_Add extends ARTCMF_Form_Abstract
{

    public function init()
    {


        $this->addElement(new Zend_Form_Element_Text('name', array(
                    'label'   => _('Name'),
                    'filters' => array('StringTrim'),
                    'required' => true
                )));

        $this->addElement(new Zend_Form_Element_Textarea('description', array(
                    'label'   => _('Description'),
                    'filters' => array('StringTrim'),
                    'required' => true
                )));


        $this->addElement(new Zend_Form_Element_Text('cost', array(
                    'label'   => 'Цена',
                    'filters' => array('StringTrim'),
                    'required' => true,
                        // 'validators' => array('Float')
                )));


        $this->addElement(new Zend_Form_Element_Text('period', array(
                    'label'   => 'Период (месяцы)',
                    'filters' => array('StringTrim'),
                    'required'   => true,
                    'validators' => array('Digits')
                )));



        $this->addElement('submit', 'submit', array(
            'required' => true,
            'label'    => _('Save')
        ));
    }

}

