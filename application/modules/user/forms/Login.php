<?php

/**
 * Default_Form_Login
 * 
 * The login form
 * 
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class User_Form_Login extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $this->setAttrib('id', 'account_login');
        $this->setAttrib('class', 'login_form');
        $this->setAttrib('onsubmit', 'return $.cmf.submitForm(this);');
        //$this->setAttrib('action', '/user/authenticate');

        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress'),
            ),
            'required' => true,
            'label' => _('Email'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Email'))
            )
        ));

        $this->addElement('password', 'passwd', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('StringLength', true, array(6, 128))
            ),
            'required' => true,
            'class' => 'password',
            'label' => _('Password'),
            'decorators' => array(
               // array('Errors'),
                array('ViewHelper'),
            ),
            'attribs' => array(
                'id' => 'password',
                'placeholder' => $this->getView()->translate(_('Password'))
            )
        ));

//         $this->addElement('captcha', 'captcha', array(
//                    'label' => _('Enter the code'),
//                    'captcha' => array(
//                        'captcha' => 'Image',
//                        'wordLen' => 4,
//                        'font' => 'fonts/arial.ttf',
//                        'imgDir' => './images/captcha',
//                        'imgUrl' => '/images/captcha',
//                        'fontsize' => '10',
//                        'width' => 70,
//                        'height' => 35,
//                        'timeout' => 120,
//                        'expiration' => 0,
//                        'imgAlt' => 'captcha',
//                        'DotNoiseLevel' => 2,
//                        'LineNoiseLevel' => 1
//                    )
//            )
//        );
//         $this->captcha->addDecorator('Label', array('tag' => 'dt', 'requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));


        $this->addElement('submit', 'submit', array(
            'required' => false,
            'ignore' => true,
            'label' => _('Sign in'),
            'decorators' => array(
                array('ViewHelper'),
            )
        ));


        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/user/login.phtml')),
            'Form'
        ));
//
//        $this->addElement('hash', 'no_csrf', array(
//            'salt' => 'unique',
//            'timeout' => 300,
//            'ignore' => false,
//            'required' => true,
//        ));
//        $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');
    }



}
