<?php

class Default_View_Helper_GetPageByType extends Zend_View_Helper_Abstract
{

    public function getPageByType($type = 'about')
    {
        $pageModel = new Default_Model_Page();

        return $pageModel->getPageByType($type);

    }
}