<?php

class  Default_View_Helper_TimeStamp extends Zend_View_Helper_Abstract
{
    public function timeStamp($time, $format = null)
    {
       if ($format == NULL)
       {
            return date('d/m/Y', strtotime($time));
       }
       else
       {
            return date($format, strtotime($time));
       }
    }
}
