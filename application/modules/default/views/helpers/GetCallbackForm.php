<?php

class Default_View_Helper_GetCallbackForm extends Zend_View_Helper_Abstract
{

    public function getCallbackForm()
    {
        $modelContact = new Default_Model_Contact();
        $form =  new Default_Form_Contact_Callback(array('model' => $modelContact));       
        return $form;
    }

}

