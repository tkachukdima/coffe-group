<?php

/**
 * The Feedback form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Contact_Feedback extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setName('feedbackForm');
        $this->setAttrib('id', 'feedback');
        $this->setMethod('post');

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElement('text', 'fio', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty')
            ),
            'required' => true,
            'label' => _('FIO'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('FIO')) . "*"
            )
        ));
        //$this->firstname->addDecorator('Label', array('tag' => 'dt', 'requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));
        $this->fio->removeDecorator('Label');
/*
        $this->addElement('text', 'firstname', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty')
            ),
            'required' => true,
            'label' => _('Firstname'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Firstname'))
            )
        ));
        //$this->firstname->addDecorator('Label', array('tag' => 'dt', 'requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));
        $this->firstname->removeDecorator('Label');

        $this->addElement('text', 'lastname', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty')
            ),
            'required' => true,
            'label' => _('Lastname'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Lastname'))
            )
        ));
        //$this->lastname->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));        
        $this->lastname->removeDecorator('Label');
*/
        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', true, array(3, 128)),
                array('EmailAddress')
            ),
            'required' => true,
            'label' => _('Email'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Email')) . "*"
            )
        ));
        //$this->email->addDecorator('Label', array('tag' => 'dt', 'requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));
        $this->email->removeDecorator('Label');


        $this->addElement('text', 'phone', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => _('Phone'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Phone')) . "*"
            )
        ));
        $this->phone->removeDecorator('Label');


        $this->addElement('text', 'city', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => _('City'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('City')) . "*"
            )
        ));
        $this->phone->removeDecorator('Label');

        $this->addElement('text', 'subject', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => _('Subject'),
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Subject')) . "*"
            )
        ));
        //$this->subject->addDecorator('Label', array('tag' => 'dt', 'requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));
        $this->subject->removeDecorator('Label');


        $this->addElement('textarea', 'message', array(
            'label' => _('Message'),
            'filters' => array('StringTrim'),
            'rows' => 5,
            'cols' => 40,
            'required' => true,
            'decorators' => array(
                array('ViewHelper')
            ),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Message')) . "*"
            )
        ));
        //$this->message->addDecorator('Label', array('tag' => 'dt', 'requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));
        //$this->message->removeDecorator('Label');
        //$this->message->addDecorator('Errors');

        $this->addElement('captcha', 'captcha', array(
            'label' => _('Enter the code'),
            'attribs' => array(
                'placeholder' => $this->getView()->translate(_('Enter the code')) . "*"
            ),
            'captcha' => array(
                'captcha' => 'Image',
                'wordLen' => 4,
                'font' => 'fonts/arial.ttf',
                'imgDir' => './images/captcha',
                'imgUrl' => '/images/captcha',
                'fontsize' => '10',
                'width' => 70,
                'height' => 35,
                'timeout' => 120,
                'expiration' => 0,
                'imgAlt' => 'captcha',
                'DotNoiseLevel' => 2,
                'LineNoiseLevel' => 1
            )
                )
        );


        $this->captcha->setDecorators(array(
            array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
            array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
        ));


        $this->addElement('submit', 'submit', array(
            'required' => true,
            'label' => _('Send'),
            'class' => 'btn'
            //'decorators' => array('ViewHelper', array('HtmlTag', array('tag' => 'dd','class' => 'btn'))),
        ));

        $this->submit->removeDecorator('DtDdWrapper');


        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => '_forms/default/feedback.phtml')),
            'Form'
        ));

        /*
          $this->addElement('hash', 'no_csrf', array(
          'salt' => 'unique',
          'timeout' => 300,
          'ignore' => false,
          'required' => true,
          ));
          $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');
         */
    }

}
