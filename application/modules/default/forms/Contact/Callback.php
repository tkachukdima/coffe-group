<?php

/**
 * The Callback form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Contact_Callback extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setName('feedbackForm');
        $this->setAttrib('id', 'callback');
        $this->setAttrib('onsubmit', 'sendCallback(this); return false;');
        $this->setMethod('post');
        $this->setAction('/contact/callback');

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );



        $this->addElement('text', 'name', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => 'Ваше имя',
        ));
	$this->name->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));

        $this->addElement('text', 'phone', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'label' => 'Контактный телефон',
        ));
	 $this->phone->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));
        

        $this->addElement('text', 'subject', array(
            'filters' => array('StringTrim'),
            'label' => 'Тема разговора',
            'required' => false,
            'value' => 'Покупка товара'
        ));

        $this->addElement('captcha', 'captcha', array(
            'label' => _('Enter the code'),
            'captcha' => array(
                'captcha' => 'Image',
                'wordLen' => 3,
                'font' => 'fonts/arial.ttf',
                'imgDir' => './images/captcha',
                'imgUrl' => '/images/captcha',
                'fontsize' => '10',
                'width' => 70,
                'height' => 35,
                'timeout' => 120,
                'expiration' => 0,
                'imgAlt' => 'captcha',
                'DotNoiseLevel' => 0,
                'LineNoiseLevel' => 0
            )
                )
        );
        $this->captcha->getDecorator('label')->setOptions(array('requiredSuffix'=> ' <span class="required">*</span> ', 'escape'=> false));


        $this->addElement('submit', 'submit', array(          
            'label' => _('Send'),
            'decorators' => array('ViewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'clr')))            
        ));

        $this->addElement('hash', 'no_csrf', array(
            'salt' => 'unique',
            'timeout' => 300,
            'ignore' => false,
            'required' => true,
        ));
        $this->no_csrf->removeDecorator('DtDdWrapper')->removeDecorator('Label');
    }

}
