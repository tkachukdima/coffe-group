<?php

/**
 * Add new page post
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Ipblocking_Add extends ARTCMF_Form_Abstract
{

    public function init()
    {

        //call the parent init
        // parent::init();
        // $this->removeElement('delete_image');
        //$this->removeElement('page_id');
        // $this->removeElement('menu_item_id');

        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');


        $this->addElement('text', 'ip', array(
            'label' => _('IP Address'),
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty'),
                array('UniqueIp', true, array($this->getModel()))),
            'required' => true
        ));

        $this->addElement('text', 'note', array(
            'label' => _('Примечание'),
            'filters' => array('StringTrim'),
            'required' => false
        ));


        $this->addElement('submit', 'submit', array(
            'label' => _('Add')
        ));
    }

}

