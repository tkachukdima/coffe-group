<?php

/**
 * Menu Select
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Menu_Select extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');

        $menus = array();

        $menuRows = $this->getModel()->getMenus()->toArray();
        foreach ($menuRows as $menu) {
            $menus[$menu['menu_id']] = $menu['name'];
        }

        $this->addElement('select', 'menu_id', array(
            'label' => _('Select menu') . ': ',
            'decorators' => array('ViewHelper', 'Label'),
            'multiOptions' => $menus
        ));

        $this->addElement('submit', 'View', array(
            'decorators' => array('ViewHelper'),
        ));
    }

}
