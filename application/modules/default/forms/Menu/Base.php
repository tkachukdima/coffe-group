<?php

/**
 * Base Menu Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Menu_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );


        $this->setMethod('post');
        $this->setAction('');

        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));

       $this->addElement('select', 'type', array(
            'label' => _('Type'),
            'multiOptions' => array(
                'main' => _('Main menu') . ' [main]',
                'bottom_left' => _('Bottom menu') . ' [left]',
                'bottom_center' => _('Bottom menu') . ' [center]',
                'bottom_right' => _('Bottom menu') . ' [right]',
                //'social_top' => _('Social menu') . ' [top]',
                'social_bottom' => _('Social menu') . ' [bottom]',
            )
        ));


         $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => 1
        ));

        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

        $this->addElement('submit', 'submit', array(

        ));

        $this->addElement('hidden', 'menu_id', array(
            'filters'    => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper',array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

    }

}
