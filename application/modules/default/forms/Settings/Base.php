<?php

/**
 * Base Setting Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Settings_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->setMethod('post');
        $this->setAction('');

        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $this->addElement('textarea', 'value_' . $lang->code, array(
                'label' => _('Value'),
                'filters' => array('StringTrim'),
                'required' => false,
                //'validators' => array(array('NotEmpty')),
                'attribs'=> array('rows' => 4)
            ));
        }

        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'validators' => array(array('NotEmpty')),
            'required' => true
        ));
        
        
        $this->addElement('text', 'key', array(
            'label' => _('Key'),
            'filters' => array('StringTrim'),
            'validators' => array(array('NotEmpty')),
            'required' => true,
            'description' => _('Это системное поле. Не изменяйте его!')
        ));
        
        
        $this->addElement('text', 'module', array(
            'label' => _('Module'),
            'filters' => array('StringTrim'),
            'validators' => array(array('NotEmpty')),
            'required' => true,
            'value' => 'default',
            'description' => _('Это системное поле. Не изменяйте его!')
        ));
        
//        $this->addElement('select', 'setting_type', array(
//            'label' => 'Тип',
//            'required' => true,
//            'multiOptions' => array(
//                'input' => 'Строка',
//                'textarea' => 'Текст',
//                'checkbox' => 'Флажок'
//            )
//        ));


        $this->addElement('submit', 'submit', array());

        $this->addElement('hidden', 'setting_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'value_' . $lang->code
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }

        $this->addDisplayGroup(array('name','key',  'module', 'setting_type', 'submit', 'setting_id'), 'form_all', array('legend' => _('General Settings')));
    }

}
