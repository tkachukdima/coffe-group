<?php

/**
 * Base Setting Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Settings_Image_Resize_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );
        
        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $this->setMethod('post');
        $this->setAction('');

        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array($this->getModel(), 'getImageResizeSettingByIdent', 'getImageResizeSettingById', 'setting_image_resize_id'))
            ),
            'required' => true,
        ));




        $this->addElement('text', 'width_thumbnail', array(
            'label' => 'Миниатюра (ширина)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'height_thumbnail', array(
            'label' => 'Миниатюра (высота)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('select', 'strategy_thumbnail_id', array(
            'label' => 'Стратегия изменения миниатюры ',
            'required' => true,
            'multiOptions' => $this->getStrategies()
        ));



        $this->addElement('text', 'width_preview', array(
            'label' => 'Превью (ширина)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'height_preview', array(
            'label' => 'Превью (высота)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('select', 'strategy_preview_id', array(
            'label' => 'Стратегия изменения превью',
            'required' => true,
            'multiOptions' => $this->getStrategies()
        ));
        
        
        $this->addElement('text', 'width_detail', array(
            'label' => 'Детальное (ширина)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'height_detail', array(
            'label' => 'Детальное (высота)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('select', 'strategy_detail_id', array(
            'label' => 'Стратегия изменения детального изображения',
            'required' => true,
            'multiOptions' => $this->getStrategies()
        ));



        $this->addElement('text', 'width_full', array(
            'label' => 'Полное (ширина)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'height_full', array(
            'label' => 'Полное (высота)',
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        $this->addElement('select', 'strategy_full_id', array(
            'label' => 'Стратегия изменения полного изображения',
            'required' => true,
            'multiOptions' => $this->getStrategies()
        ));



        $this->addElement('submit', 'submit', array());

        $this->addElement('hidden', 'setting_image_resize_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
    }

    private function getStrategies()
    {
        $strategies = $this->getModel()->getImageResizeStrategies()->toArray();
        foreach ($strategies as $strategy) {
            $strategies_list[$strategy['setting_image_resize_strategy_id']] = $strategy['name'];
        }

        return $strategies_list;
    }

}
