<?php

/**
 * Add new page post
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 * 
 *  */
class Default_Form_Settings_Edit extends Default_Form_Settings_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        
        // $this->getElement('ident')->setAttrib('readOnly', true);
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
