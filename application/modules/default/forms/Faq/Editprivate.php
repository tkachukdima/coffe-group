<?php

/**
 * Edit Faq
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Editprivate extends Default_Form_Faq_Edit {

    public function init() {

        //call the parent init
        parent::init();
        $this->removeElement('status');
        $this->removeElement('category_faq_id');
        $this->removeElement('sort_order');
        
    }

}
