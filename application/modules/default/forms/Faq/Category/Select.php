<?php

/**
 * CategoryFaq Select
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Category_Select extends ARTCMF_Form_Abstract
{

    protected $authService;
    protected $role;

    public function init()
    {
        $this->authService = new User_Service_Authentication();
        $this->role = $this->authService->getIdentity()->role;

        $this->setMethod('post');

        $categories = $this->getCategoriesFaq(0, 0);
        $cats       = array(0 => _('Root level'));
        foreach ($categories as $category) {
            $cats[$category['category_faq_id']] = $category['name'];
        }

        $this->addElement('select', 'category_faq_id', array(
            'label' => _('Select category'),
            'multiOptions' => $cats
        ));

        $this->addElement('submit', 'View', array(
            'decorators' => array('ViewHelper'),
        ));
    }

    private function getCategoriesFaq($parent_id, $level = 0)
    {
        $level++;

        $data = array();
        $results = $this->getModel()->getCategoriesFaqByParentId($parent_id)->toArray();

        foreach ($results as $result) {

            if ($this->role == 'Redactor' AND $result['category_faq_id'] == 1)
                continue;

            $data[] = array(
                'category_faq_id' => $result['category_faq_id'],
                'name'            => str_repeat(' - ', $level) . $result['name']
            );

            $children = $this->getCategoriesFaq($result['category_faq_id'], $level);

            if ($children) {
                $data = array_merge($data, $children);
            }
        }

        return $data;
    }

}
