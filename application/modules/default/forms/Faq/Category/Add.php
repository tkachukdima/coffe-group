<?php

/**
 * Add new Categoryfaq
 *
 * @CategoryFaq   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Category_Add extends Default_Form_Faq_Category_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('category_faq_id')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
