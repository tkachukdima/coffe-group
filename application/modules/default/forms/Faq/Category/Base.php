<?php

/**
 * Base category Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Form_Faq_Category_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );
        
        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');


        $this->setMethod('post');
        $this->setAction('');

        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));



        // get the select
        $form = new Default_Form_Faq_Category_Select(
                        array('model' => $this->getModel())
        );
        $element = $form->getElement('category_faq_id');
        $element->clearDecorators()->loadDefaultDecorators();
        $element->setName('parent_id')
                ->setRequired(true)
                ->setLabel('Select Parent');
        $this->addElement($element, 'parent_id');




        $this->addElement('textarea', 'description', array(
            'label' => _('Description'),
            'filters' => array('StringTrim'),
            'required' => true,
            'rows' => 3,
        ));
        
         
         $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(                
                array('UniqueIdent', true, array($this->getModel(), 'getCategoryFaqByIdent', 'getCategoryFaqById', 'category_faq_id'))                
            ),
            'required' => false,
        ));
         
        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            //'required' => true,
        ));

        $this->addElement('submit', 'submit', array(
        ));

        $this->addElement('hidden', 'category_faq_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
    }

}
