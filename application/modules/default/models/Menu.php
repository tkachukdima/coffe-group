<?php

/**
 * Default_Menu
 *
 * @menu   Default
 * @package    Default_Model
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Model_Menu extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get menu by ident
     *
     * @param string $type The type string
     * @return Default_Resource_Menu_Item|null
     */
    public function getMenuByType($type)
    {
        return $this->getResource('Menu')->getMenuByType($type);
    }

    /**
     * Get all menus
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getMenus()
    {
        return $this->getResource('Menu')->getMenus();
    }

    /**
     * Get the menu item by its id
     *
     * @param int $id
     * @return Default_Resource_Menu_Item|null
     */
    public function getMenuById($id)
    {
        return $this->getResource('Menu')->getMenuById($id);
    }

    /**
     * Get a menu_item by its id
     *
     * @param  int $id The id
     * @return Default_Resource_Menuitem_Item
     */
    public function getMenuitemById($id)
    {
        $id = (int) $id;

        return $this->getResource('Menuitem')->getMenuitemById($id);
    }

    /**
     * Get a menu_item by its id for edit
     *
     * @param  int $id The id
     * @return Default_Resource_Menuitem_Item
     */
    public function getMenuitemByIdForEdit($id)
    {
        $id = (int) $id;

        $menuitem = $this->getResource('Menuitem')->getMenuitemByIdForEdit($id)->toArray();

        $data = $menuitem[0];

        foreach ($menuitem as $value) {
            $data['name_' . $value['language_code']] = $value['name'];
            $data['title_' . $value['language_code']] = $value['title'];
        }

        if (stripos($data['uri'], 'http://') !== false) /* если абсолютная ссылка то заполняем поле */
            $data['uri_absolut'] = $data['uri'];


        return $data;
    }

    /**
     * Fetch all the menu_items
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getAllMenuitems()
    {
        return $this->getResource('Menuitem')->getAllMenuitems();
    }

    /**
     * Get menu_items in a menu
     *
     * @param int|string  $menu The menu name or id
     * @param int|boolean $paged    Whether to page results
     * @param array       $order    Order results
     * @param boolean     $deep     Get all menu_items below this menu?
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getMenuitemsByMenu($menu, $paged = false, $order = null, $deep = true)
    {
        if (is_string($menu)) {
            $cat = $this->getResource('Menu')->getMenuByIdent($menu);
            $menu_id = null === $cat ? 0 : $cat->menu_id;
        } else {
            $menu_id = $menu;
        }

        if (true === $deep) {
            $ids = $this->getMenuChildrenIds($menu_id, true);
            $ids[] = $menu_id;
            $menu_id = null === $ids ? $menu_id : $ids;
        }

        return $this->getResource('Menuitem')->getMenuitemsByMenu($menu_id, $paged, $order);
    }

    /**
     * Get all categories tree
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getMenuTree($type = 'main')
    {
        $lang = Zend_Registry::get('Current_Lang');

        if (is_string($type)) {
            $menu = $this->getResource('Menu')->getMenuByType($type);
        } elseif (is_integer($type)) {
            $menu = $this->getResource('Menu')->getMenuById($type);
        } else {
            $menu = $this->getResource('Menu')->getMenuById(0);
        }

        $row_set = $this->getResource('Menuitem')->getMenuitemsForTree($menu->menu_id, null)->toArray();
        
        $tree = array(0 => array('menu_item_id' => 0, 'parent_id' => 0, 'value' => 'root'));
        $temp = array(0 => &$tree[0]);
        foreach ($row_set as $val) {
            $parent = &$temp[$val['parent_id']];

            if (!isset($parent['pages'])) {
                $parent['pages'] = array();
            }

            $parent['pages'][$val['menu_item_id']]['menu_item_id'] = $val['menu_item_id'];
            $parent['pages'][$val['menu_item_id']]['parent_id'] = $val['parent_id'];
            $parent['pages'][$val['menu_item_id']]['id'] = $val['menu_item_id'];
            $parent['pages'][$val['menu_item_id']]['label'] = $val['name'];
            $parent['pages'][$val['menu_item_id']]['title'] = $val['title'];
            $parent['pages'][$val['menu_item_id']]['image'] = isset($val['image']) ? $val['image'] : null; /*victor fix коли аблолютка ссилка то зображення пропадає*/

            if (strstr($val['uri'], 'http://') !== false) {/* если абсолютная ссылка то оставляем как есть */
                $parent['pages'][$val['menu_item_id']]['uri'] = $val['uri'];
            } else {

                $request = new Zend_Controller_Request_Http('http://site.com' . $val['uri']); //$_SERVER['HTTP_HOST']
                Zend_Controller_Front::getInstance()->getRouter()->route($request);


                $userParams = $request->getUserParams();

                //   print_r($userParams);

                $parent['pages'][$val['menu_item_id']]['module'] = $request->getModuleName();
                $parent['pages'][$val['menu_item_id']]['controller'] = $request->getControllerName();
                $parent['pages'][$val['menu_item_id']]['action'] = $request->getActionName();
                $parent['pages'][$val['menu_item_id']]['reset_params'] = true;
                $parent['pages'][$val['menu_item_id']]['params'] = array('lang' => $lang);
                $parent['pages'][$val['menu_item_id']]['image'] = isset($val['image']) ? $val['image'] : null;
                

                $parent['pages'][$val['menu_item_id']]['route'] = 'default';
                //$parent['pages'][$val['menu_item_id']]['route'] = $request->getUserParams();
                // $parent['pages'][$val['menu_item_id']]['reset_params'] = true;

                switch ($request->getModuleName()) {
                    case 'file':
                        $parent['pages'][$val['menu_item_id']]['route'] = 'default';
                        $parent['pages'][$val['menu_item_id']]['params'] = array_merge($parent['pages'][$val['menu_item_id']]['params'], array('group_id' => $userParams['group_id']));
                        break;

                    case 'publication':
                        //        print_r($userParams);die;
                        $parent['pages'][$val['menu_item_id']]['route'] = 'default_publication';
                        $parent['pages'][$val['menu_item_id']]['params'] = array_merge($parent['pages'][$val['menu_item_id']]['params'], array('groupIdent' => $userParams['groupIdent']));
                        break;
                }

                switch ($request->getControllerName()) {
                                
                    case 'page':
                        $parent['pages'][$val['menu_item_id']]['route'] = 'default_page';
                        $parent['pages'][$val['menu_item_id']]['params'] = array_merge($parent['pages'][$val['menu_item_id']]['params'], array('pageIdent' => $userParams['pageIdent']));
                        break;

                    default:
                        break;
                }



                //тут подкючаем категории для второго уровня
                //  if (1 == $val['dropdown']) {

                switch ($request->getModuleName()) {
                    case 'catalog':
                        $catalogModel = new Catalog_Model_Catalog();
                        $catalog_container = $catalogModel->getCategoriesTree(true);

                        if (null != $catalog_container) {
                            $parent['pages'][$val['menu_item_id']]['pages'] = $catalog_container;
            }

                        break;

                    default:
                        break;
                }
            }

            if (isset($val['pages']))
                $parent['pages'][$val['menu_item_id']]['pages'] = array_merge($parent['pages'][$val['menu_item_id']]['pages'], $val['pages']);

            $temp[$val['menu_item_id']] = &$parent['pages'][$val['menu_item_id']];
        }
        unset($row_set, $temp, $val, $parent);
        //print_r($tree[0]['pages']);
        return $tree[0]['pages'];
    }

    /**
     * Get a menus children menu_id values
     *
     * @param int     $menu_id The menu to get children from
     * @param boolean $recursive  Get the entire menu branch?
     * @return array An array of ids
     */
    public function getMenuChildrenIds($menu_id, $recursive = false)
    {
        $menus = $this->getMenusByParentId($menu_id);
        $cats = array();

        foreach ($menus as $menu) {
            $cats[] = $menu->menu_id;

            if (true === $recursive) {
                $cats = array_merge($cats, $this->getMenuChildrenIds($menu->menu_id, true));
            }
        }

        return $cats;
    }

    /**
     * Get a menus parents
     *
     * @param Default_Resource_Menu_Item $menu
     * @param boolean Append the parent to the cats array?
     * @return array
     */
    public function getParentMenus($menu, $appendParent = true)
    {
        $cats = $appendParent ? array($menu) : array();

        if (0 == $menu->parent_id) {
            return $cats;
        }

        $parent = $menu->getParentMenu();
        $cats[] = $parent;

        if (0 != $parent->parent_id) {
            $cats = array_merge($cats, $this->getParentMenus($parent, false));
        }

        return $cats;
    }

    /**
     * Save a menu
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveMenu($data, $validator = null)
    {
        if (!$this->checkAcl('saveMenu')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Menu' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $menu = array_key_exists('menu_id', $data) ?
                $this->getResource('Menu')->getMenuById($data['menu_id']) : null;

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('menu')
        );

        return $this->getResource('Menu')->saveRow($data, $menu);
    }

    /**
     * Save a menu_item
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveMenuitem($data, $validator = null)
    {
        if (!$this->checkAcl('saveMenuitem')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('menuItem' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        if ($data['uri_absolut'] != '') {
            $data['uri'] = $data['uri_absolut'];
        }
        unset($data['uri_absolut']);





        $menu_item = array_key_exists('menu_item_id', $data) ?
                $this->getResource('Menuitem')->getMenuitemById($data['menu_item_id']) : null;

        $arrUrl = explode('_', $data['page']);
        $data['controller'] = $arrUrl[0];
        $data['action'] = $arrUrl[1];
        $data['params'] = $arrUrl[2];
        $data['route'] = 'default';
        if (is_null($data['image']))
            unset($data['image']);
        $menu_item_id = $this->getResource('Menuitem')->saveRow($data, $menu_item);

        /* Добавляем изображение ссылки */
        if (isset($data['image'])) {

            $filter = new ARTCMF_Filter_ImageSize();
            $menuitem = $this->getResource('Menuitem')->getMenuitemById($menu_item_id);
            $path_parts = pathinfo($data['image']);
            $new_file_name = 'linc-' . $menuitem->menu_item_id . '.' . $path_parts['extension'];

            $fileDestination = realpath(APPLICATION_PATH . '/../www/images/linc');
            rename($fileDestination . '/' . $data['image'], $fileDestination . '/' . $new_file_name);

            $settingModel = new Default_Model_Settings();
            $image_resize_setting = $settingModel->getImageResizeSettingForModel('menuItem');

            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);
            $menuitem->image = basename($thumbnail);
            $menuitem->save();
        }
        /* изображение добавлено */

        $this->getResource('Menuitem')->deleteTranslatedRows($menu_item_id);
        
        foreach (Zend_Registry::get('langList') as $lang) {

            $translate_data = array(
                'menu_item_id' => (int) $menu_item_id,
                'language_code' => $lang->code,
                'name' => $data['name_' . $lang->code],
                'title' => $data['title_' . $lang->code] == '' ? $data['name_' . $lang->code] : $data['title_' . $lang->code]
            );

            $this->getResource('Menuitem')->saveTranslatedRows($translate_data, 'insert');
        }

        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_ALL, array('menu')
        );

        return $menu_item_id;
    }

    public function deleteMenu($menu)
    {
        if (!$this->checkAcl('deleteMenu')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($menu instanceof Default_Resource_Menu_Item) {
            $menu_id = (int) $menu->menu_id;
        } else {
            $menu_id = (int) $menu;
        }

        $menu = $this->getMenuById($menu_id);

        if (null !== $menu) {
            $menu->delete();
            return true;
        }

        return false;
    }

    public function deleteMenuitem($menu_item)
    {
        if (!$this->checkAcl('deleteMenuitem')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if ($menu_item instanceof Default_Resource_Menuitem_Item) {
            $menu_item_id = (int) $menu_item->menu_item_id;
        } else {
            $menu_item_id = (int) $menu_item;
        }

        $menu_item = $this->getMenuitemById($menu_item_id);
        if (null !== $menu_item) {
            $this->getResource('Menuitem')->deleteTranslatedRows($menu_item_id);
            $menu_item->delete();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('menu')
            );
            return true;
        }

        //remove from the index


        return false;
    }

    public function moveMenuItem($id, $move)
    {
        $menu_item = $this->getMenuitemById($id);

        $near_sort = $this->getResource('Menuitem')->getNearSort($menu_item->sort_order, $move, $menu_item->menu_id, $menu_item->parent_id);

        if (count($near_sort) != 0) {

            $old_sort_order = $menu_item->sort_order;
            $menu_item->sort_order = $near_sort[0]->sort_order;
            $menu_item->save();


            $menu_item_near = $this->getMenuitemById($near_sort[0]->menu_item_id);

            $menu_item_near->sort_order = $old_sort_order;
            $menu_item_near->save();
        }
        $this->getCached()
                ->getCache()
                ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('menu')
        );
    }

    public function setNewSortOrder($id, $sort_order)
    {
        foreach ($id as $key => $id) {
            $menu_item = $this->getMenuitemById($id);
            $menu_item->sort_order = (int) $sort_order[$key];
            $menu_item->save();
            $this->getCached()
                    ->getCache()
                    ->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('menu')
            );
        }
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Menu';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
