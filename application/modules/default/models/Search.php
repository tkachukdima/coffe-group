<?php

class Default_Model_Search extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    protected $_searchIndexPath; //path to initial data folder
    protected $_db;    //database adapter

    /**
     * Создает новый поисковой индекс
     */

    public function updateIndex()
    {
        if (!$this->checkAcl('updateIndex')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        setlocale(LC_ALL, 'ru_RU.UTF-8');

        $this->_searchIndexPath = APPLICATION_PATH . '/../data/search-index';
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
        set_time_limit(900);
        Zend_Search_Lucene_Analysis_Analyzer::setDefault(
                new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive());

        //удаляем существующий индекс, в большинстве случае эта операция с последующий созданием нового индекса работает гораздо быстрее
        $this->recursive_remove_directory($this->_searchIndexPath, TRUE);

        try {
            $index = Zend_Search_Lucene::create($this->_searchIndexPath);
        } catch (Zend_Search_Lucene_Exception $e) {
            echo "<a class=\"ui-bad-message\">Error: {$e->getMessage()}</p>";
        }

        try {
            foreach (Zend_Registry::get('langList') as $lang) {

                //выбираем все Страницы  из БД
                $rows = $this->_db->fetchAll(
                        "SELECT * FROM page 
                        LEFT JOIN `page_translation` ON page.page_id = page_translation.page_id 
                        WHERE page.status = 1 AND page_translation.language_code = '{$lang->code}'"
                );
                foreach ($rows as $row) {
                    $doc = new Zend_Search_Lucene_Document();
                    $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/page/' . $row['ident'] . '.html'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['title'], 'UTF-8'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['body'], 'UTF-8'));
                    $index->addDocument($doc);
                }

                //выбираем все Публикации  из БД
                $rows = $this->_db->fetchAll(
                        "SELECT `publication`.*, `publication_translation`.*, `publication_group`.`ident` AS `publication_group_ident` 
                    FROM `publication` 
                    LEFT JOIN `publication_group` ON publication.publication_group_id = publication_group.publication_group_id 
                    LEFT JOIN `publication_translation` ON publication.publication_id = publication_translation.publication_id                          
                    WHERE (publication_translation.language_code = '{$lang->code}')
                    ORDER BY `publication`.`date_post` DESC");

                //print_r($rows);exit;
                foreach ($rows as $row) {
                    $doc = new Zend_Search_Lucene_Document();
                    $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/publication/view/' . $row['publication_group_ident'] . '/' . $row['ident'] . '.html'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['title'], 'UTF-8'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['body'], 'UTF-8'));
                    $index->addDocument($doc);
                }

                //магазин категории
                $rows = $this->_db->fetchAll("
                SELECT `catalog_category`.ident, `catalog_category_translation`.`name`, `catalog_category_translation`.`description`
                FROM `catalog_category`
                LEFT JOIN `catalog_category_translation` ON catalog_category.category_id = catalog_category_translation.category_id
                WHERE catalog_category_translation.language_code = '{$lang->code}'");
                 
                foreach ($rows as $row) {
                    $doc = new Zend_Search_Lucene_Document();
                    $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/catalog/' . $row['ident']));
                    $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['name'], 'UTF-8'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['description'], 'UTF-8'));
                    $index->addDocument($doc);
                }

                //магазин
                $rows = $this->_db->fetchAll("
                SELECT `catalog_product`.ident, `catalog_category`.`ident` AS `category_ident`, `catalog_product_translation`.`name`, `catalog_product_translation`.`description`
                FROM `catalog_product` 
                LEFT JOIN `catalog_products_categories` ON catalog_products_categories.product_id  = catalog_product.product_id
                LEFT JOIN `catalog_category` ON catalog_category.category_id = catalog_products_categories.category_id
                LEFT JOIN `catalog_product_translation` ON catalog_product.product_id = catalog_product_translation.product_id
                WHERE catalog_product_translation.language_code = '{$lang->code}'");

                 
                    
                foreach ($rows as $row) {
                    $doc = new Zend_Search_Lucene_Document();
                    $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/catalog/view/' . $row['category_ident'] . '/' . $row['ident'] . '.html'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['name'], 'UTF-8'));
                    $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['description'], 'UTF-8'));
                    $index->addDocument($doc);
                }


//            //каталог категории
//            $rows = $this->_db->fetchAll("
//                SELECT `catalog_category`.* FROM `catalog_category`
//                LEFT JOIN `catalog_category_translation` ON catalog_category.category_id =catalog_category_translation.category_id
//                 WHERE catalog_category_translation.language_code = '{$lang->code}'");
//
//            foreach ($rows as $row) {
//                $doc = new Zend_Search_Lucene_Document();
//                $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/catalog/' . $row['ident']));
//                $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['name'], 'UTF-8'));
//                $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['description'], 'UTF-8'));
//                $index->addDocument($doc);
//            }
//
//            //каталог
//            $rows = $this->_db->fetchAll("
//                SELECT `catalog_product`.*, `catalog_category`.`ident` AS `category_ident` 
//                FROM `catalog_product` 
//                INNER JOIN `catalog_category` ON `catalog_category`.category_id = `catalog_product`.category_id
//                LEFT JOIN `catalog_product_translation` ON catalog_product.product_id = catalog_product_translation.product_id
//                WHERE catalog_product_translation.language_code = '{$lang->code}'");
//                        
//            foreach ($rows as $row) {
//                $doc = new Zend_Search_Lucene_Document();
//                $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/catalog/view/' . $row['category_ident'] . '/' . $row['ident']));
//                $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['name'], 'UTF-8'));
//                $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['description'], 'UTF-8'));
//                $index->addDocument($doc);
//            }
                //Галерея
//            $rows = $this->_db->fetchAll("SELECT * FROM album
//                LEFT JOIN `album_translation` ON album.album_id = album_translation.album_id
//                WHERE album_translation.language_code = '{$lang->code}'");
//            foreach ($rows as $row) {
//                $doc = new Zend_Search_Lucene_Document();
//                $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/' . $lang->code . '/gallery/view/' . $row['ident']));
//                $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['name'], 'UTF-8'));
//                $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['name'], 'UTF-8'));
//                $index->addDocument($doc);
//            }
//            //Прайсы
//            $rows = $this->_db->fetchAll("SELECT * FROM price WHERE status = 1");
//            foreach ($rows as $row) {
//                $doc = new Zend_Search_Lucene_Document();
//                $doc->addField(Zend_Search_Lucene_Field::Keyword('url', '/price/'));
//                $doc->addField(Zend_Search_Lucene_Field::Text('title', $row['name'], 'UTF-8'));
//                $doc->addField(Zend_Search_Lucene_Field::Text('body', $row['description'], 'UTF-8'));
//                $index->addDocument($doc);
//            }
            }
        } catch (Zend_Search_Lucene_Exception $e) {
            echo "<a class=\"ui-bad-message\">Error: {$e->getMessage()}</p>";
        }

        //let's clean up some
        $index->optimize();
    }

    /**
     * recursive_remove_directory( directory to delete, empty )
     * expects path to directory and optional TRUE / FALSE to empty
     *
     * @param $directory
     * @param $empty TRUE - just empty directory
     */
    protected function recursive_remove_directory($directory, $empty = FALSE)
    {
        if (substr($directory, -1) == '/') {
            $directory = substr($directory, 0, -1);
        }
        if (!file_exists($directory) || !is_dir($directory)) {
            return FALSE;
        } elseif (is_readable($directory)) {
            $handle = opendir($directory);
            while (FALSE !== ($item = readdir($handle))) {
                if ($item != '.' && $item != '..') {
                    $path = $directory . '/' . $item;
                    if (is_dir($path)) {
                        $this->recursive_remove_directory($path);
                    } else {
                        unlink($path);
                    }
                }
            }
            closedir($handle);
            if ($empty == FALSE) {
                if (!rmdir($directory)) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * Search by query
     *
     * @param $query search query
     * @return array Zend_Search_Lucene_Search_QueryHit
     */
    public function search($query)
    {

        setlocale(LC_ALL, 'ru_RU.UTF-8');

        $this->_searchIndexPath = APPLICATION_PATH . '/../data/search-index';
        $this->_db = Zend_Db_Table_Abstract::getDefaultAdapter();
        set_time_limit(900);
        Zend_Search_Lucene_Analysis_Analyzer::setDefault(
                new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive());
        Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('utf-8');
                
        try {
            $index = Zend_Search_Lucene::open($this->_searchIndexPath);
        } catch (Zend_Search_Lucene_Exception $e) {
            echo "Error:{$e->getMessage()}";
        }
        
        
        $userQuery = Zend_Search_Lucene_Search_QueryParser::parse($query, 'utf-8');
      
        $results = $index->find($userQuery);
        $data = array();
        foreach ($results as $key => $result) {
            $data[$key]["url"] = $result->url;
            $data[$key]["title"] = $result->title;
            $data[$key]["body"] = $userQuery->htmlFragmentHighlightMatches(strip_tags($result->body), 'utf-8');
            //$data[$key]["body"] = $result->body;
        }
        return $data;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Search';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}