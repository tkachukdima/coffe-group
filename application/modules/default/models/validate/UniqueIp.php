<?php

class Default_Validate_UniqueIp extends Zend_Validate_Abstract
{

    const IP_EXISTS = 'ipExists';

    protected $_messageTemplates = array(
        self::IP_EXISTS => "Ip '%value%' already exists in our system",
    );

    public function __construct(Default_Model_Ipblocking $model)
    {
        $this->_model = $model;
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue((string) $value);

        $ip = $this->_model->getIp($context['ip']);

        if (null === $ip) {
            return true;
        }

        $this->_error(self::IP_EXISTS);
        return false;
    }

}
