<?php

/**
 * Default_Resource_Settingimageresize
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Settingimageresize extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'setting_image_resize';
    protected $_primary = 'setting_image_resize_id';
    protected $_rowClass = 'Default_Resource_Settingimageresize_Item';

    public function getImageResizeSettingById($id)
    {
        return $this->find($id)->current();
    }  
    
    
     public function getImageResizeSettingByIdent($ident, $ignoreRow = null)
    {
        $select = $this->select()
                       ->where('ident = ?', $ident);

        if (null !== $ignoreRow) {
            $select->where('ident < ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }
    
    
    /**
     * Get a list of setting
     *
     * @param  boolean   $settingd      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getImageResizeSettings()
    {
        $select = $this->select()
                        ->setIntegrityCheck(false) // отключаем режим целосности, это значит, что save(), delete() и методы для установки значений полей в "заблокированном" состоянии
                        ->from('setting_image_resize')
                        ->joinLeft('setting_image_resize_strategy', 
                                'setting_image_resize.strategy_preview_id = setting_image_resize_strategy.setting_image_resize_strategy_id', 
                                array('strategy_name' => 'setting_image_resize_strategy.name'))
                ->order('name');

        return $this->fetchAll($select);
    }


}