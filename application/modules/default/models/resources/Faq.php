<?php

/**
 * Default_Resource_Faq
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Faq extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'faq';
    protected $_primary = 'faq_id';
    protected $_rowClass = 'Default_Resource_Faq_Item';
    
    /**
     * Get a faq by its id
     *
     * @param int $id The id to search for
     * @return Default_Resource_Product_Item|null
     */
 
    public function getFaqById($id)
    {
        return $this->find($id)->current();         
    }
       
    public function getFaqs()
    {
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from($this->_name)
                ->joinUsing('category_faq', 'category_faq_id', array('category_name' => 'category_faq.name'))
                ->order('category_faq.sort_order')
                ->order($this->_name . '.sort_order');
                
        return $this->fetchAll($select);
    }

    public function getFaqByIdent($ident, $ignoreRow = null)
    {
        $select = $this->select()
                       ->where('ident = ?', $ident);

        if (null !== $ignoreRow) {
            $select->where('ident < ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }

     public function getFaqsByCategory($category_id, $paged, $order, $status)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->where("category_faq_id = ?", $category_id);

        if ($status!=0)
        {
           $select->where("status = ?", $status);
        }
        
        
        if (null !== $order) {
            $select->order($order);
        }

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from('faq', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'));
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage(5)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }

    
    public function updateFaq($faq, $data)
    {
        if ($faq instanceof Default_Resource_Faq_Item) {
            $faq_id = $faq->faq_id;
        } else {
            $faq_id = (int) $faq;
        }
       
        $where = $this->getAdapter()->quoteInto('faq_id = ?', $faq_id);

        return $this->update($data, $where);
    }

}