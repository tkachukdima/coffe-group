<?php

/**
 * Default_Resource_Ipblocking
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Ipblocking extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'blocking_ip';
    protected $_primary = 'ip';
    protected $_rowClass = 'Default_Resource_Ipblocking_Item';

    public function getIp($ip)
    {
        return $this->find($ip)->current();
    }

    public function getList($paged = null)
    {
        $select = $this->select()->from($this->_name);

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from($this->_name, new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'));
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage(50)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }

}
