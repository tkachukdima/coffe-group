<?php

/**
 * Default_Resource_Categoryfaq_Item
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Categoryfaq_Item extends ARTCMF_Model_Resource_Db_Table_Row_Abstract
{

    /**
     * Get the Parent CategoryFaq
     *
     * @return string
     */
    public function getParentCategoryFaq()
    {
        return $this->findParentRow('Default_Resource_Categoryfaq', 'SubCategoryfaq');
    }

}