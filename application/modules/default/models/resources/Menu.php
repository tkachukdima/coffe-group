<?php
/**
 * Default_Resource_Menu
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Menu extends ARTCMF_Model_Resource_Db_Table_Abstract
{
    protected $_name = 'menu';
    protected $_primary = 'menu_id';
    protected $_rowClass = 'Default_Resource_Menu_Item';
    
       
    public function getMenuByType($type)
    {
        $select = $this->select()
                       ->where('type = ?', $type);

        return $this->fetchRow($select);
    }

    
    public function getMenuById($id)
    {
        $select = $this->select()
                       ->where('menu_id = ?', (int)$id);
                       
        return $this->fetchRow($select);
    }

    public function getMenus()
    {
        $select = $this->select()
                       ->order('sort_order');

        return $this->fetchAll($select);
    }
}
