<?php

/**
 * Default_Resource_Page
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Resource_Page extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'page';
    protected $_primary = 'page_id';
    protected $_rowClass = 'Default_Resource_Page_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }
    
    public function getPageById($id)
    {
        return $this->find($id)->current();
    }
    
    public function getPageByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.page_id = {$this->_name}_translation.page_id")
                ->where($this->_name . '.page_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getPageByIdent($ident, $ignoreRow = null)
    {        
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.page_id = {$this->_name}_translation.page_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.ident = ?', $ident);

        if (!is_null($ignoreRow)) {
            $select->where($this->_name . '.ident <> ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }
    
    public function getPageByType($type)
    {
        $type = $type ? $type : 'static';
        
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.page_id = {$this->_name}_translation.page_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.type = ?', $type)
                ->order($this->_name . '.date_post');

        return $this->fetchRow($select);
    }

    public function getPagesByType($type)
    {
        $type = $type ? $type : 'static';
        
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.page_id = {$this->_name}_translation.page_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.type = ?', $type)
                ->limit(3)
                ->order($this->_name . '.date_post');

        return $this->fetchAll($select);
    }

    /**
     * Get a list of page
     *
     * @param  boolean   $paged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getPages($paged=null, $limit=null, $per_page = 20)
    {        
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.page_id = {$this->_name}_translation.page_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang);
                
        if (null !== $limit) {
            $select->limit($limit);
        }

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from('page', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'))
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.page_id = {$this->_name}_translation.page_id")
                    
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang);
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage($per_page)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }
    
    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'page_id' => $translate_data['page_id'],
                    'language_code' => $translate_data['language_code'],
                    'title' => $translate_data['title'],
                    'body' => $translate_data['body'],
                    'page_title' => $translate_data['page_title'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords'],
                    'description_img' => $translate_data['description_img'],
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'title' => $translate_data['title'],
                    'body' => $translate_data['body'],
                    'page_title' => $translate_data['page_title'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords'],
                    'description_img' => $translate_data['description_img'],
                        ), "`page_id` = {$translate_data['page_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "page_id = {$id}");
    }

}