<?php

class Default_Model_Ipblocking extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a language post by its id
     *
     * @param  string $ident The ident
     * @return Default_Resource_Ipblocking_Item
     */
    public function getIp($ip)
    {
        return $this->getResource('Ipblocking')->getIp($ip);
    }

    /**
     * Get language
     *
     * @param int|boolean   $languaged    Whether to language results
     * @param integer|null  $limit    Order results
     * @param integer       $per_language    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getList($paged = null)
    {
        return $this->getResource('Ipblocking')->getList($paged);
    }

    /**
     * Save a language post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveIp($data)
    {
        if (!$this->checkAcl('saveIp')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }


        $validator = $this->getForm('IpblockingAdd');

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $ip = $this->getResource('Ipblocking')->getIp($data['ip']);

        return $this->getResource('Ipblocking')->saveRow($data, $ip);
    }

    public function deleteIp($ip)
    {
        if (!$this->checkAcl('deleteIpblocking')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $ip_address = $this->getIp($ip);

        if (null !== $ip_address) {
            $ip_address->delete();
            return true;
        }

        return false;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Ipblocking';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
