<?php

/*
 * MenusController
 *
 * @menu   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class MenusController extends Zend_Controller_Action
{

    /**
     * @var Default_Model_Menu
     */
    protected $_modelMenu;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->view->headTitle($this->view->translate(_('Menu')), 'PREPEND');
        $this->_modelMenu = new Default_Model_Menu();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function listAction()
    {

        $this->view->menus = $this->_modelMenu->getMenus();
    }

    public function listMenuItemAction()
    {


        if (!$this->_getParam('menu_id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Menu not found')) . ' ' . $this->_getParam('menu_id'));
        }


        if ($this->_getParam('menu_id', 0))
            $this->view->menu_id = (int) $this->_getParam('menu_id');

        $this->view->menu = $this->_modelMenu->getMenuById($this->view->menu_id);


        $this->view->menu_items = $this->_modelMenu->getMenuitemsByMenu(
                $this->view->menu_id, null, 'sort_order', false
        );
    }

    public function sortAction()
    {

        $redirector = new Zend_Controller_Action_Helper_Redirector();

        if ($this->_getParam('id') != '' AND $this->_getParam('move') != '') {
            $id = (int) $this->_getParam('id');
            $move = $this->_getParam('move');

            $this->_modelMenu->moveMenuItem($id, $move);
        }

        if ($this->getRequest()->isPost()) {
            if (is_array($this->getRequest()->getPost('sort_order')) AND is_array($this->getRequest()->getPost('id'))) {

                $id = $this->getRequest()->getPost('id');
                $sort_order = $this->getRequest()->getPost('sort_order');

                $this->_modelMenu->setNewSortOrder($id, $sort_order);
            }
        }

        $redirector->gotoRoute(array(
            'controller' => 'menus',
            'action' => 'list-menu-item',
            'menu_id' => $this->_getParam('menu_id')), 'admin', true
        );
    }

    public function addMenuItemAction()
    {


        $this->view->menuItemForm = $this->_getMenuitemForm('add');
    }

    public function addAction()
    {
        $this->view->menuForm = $this->_getMenuForm('add');
    }

    public function editAction()
    {


        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Menu not found')) . ' ' . $this->_getParam('id'));
        }

        $menu_id = (int) $this->_getParam('id');

        $menu = $this->_modelMenu->getMenuById($menu_id);

        $this->view->menuForm = $this->_getMenuForm('edit')->populate($menu->toArray());
    }

    public function editMenuItemAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Menu item not found')) . ' ' . $this->_getParam('id'));
        }

        $menu_item_id = (int) $this->_getParam('id');

        $menu_item = $this->_modelMenu->getMenuitemByIdForEdit($menu_item_id);

        $this->view->image = $menu_item['image'];

        $this->view->menuItemForm = $this->_getMenuitemForm('edit')->populate($menu_item);
    }

    public function saveMenuAction()
    {


        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelMenu->saveMenu($request->getPost(), $type)) {
            $this->view->сategoryForm = $this->_getMenuForm($type);
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Menu added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Menu updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'menus',
                    'module' => 'default'), 'admin', true);
    }

    public function saveMenuItemAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelMenu->saveMenuitem($request->getPost(), $type)) {
            $this->view->menuItemForm = $this->_getMenuitemForm($type);
            return $this->render($type . '-menu-item');
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Menu item added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Menu item updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list-menu-item',
                    'controller' => 'menus',
                    'module' => 'default',
                    'menu_id' => $request->getPost('menu_id')), 'admin', true);
    }

    public function deleteMenuItemAction()
    {
        if (false === ($id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Menu item not found')) . ' ' . $id);
        }

        $this->_modelMenu->deleteMenuitem($id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list-menu-item',
                    'controller' => 'menus',
                    'module' => 'default',
                    'menu_id' => $this->_getParam('menu_id')), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($menu_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Menu not found')) . ' ' . $menu_id);
        }

        $this->_modelMenu->deleteMenu($menu_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'menus',
                    'module' => 'default'), 'admin', true);
    }

    protected function _getMenuForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['Menu' . ucfirst($type)] = $this->_modelMenu->getForm('Menu' . ucfirst($type));
        $this->_forms['Menu' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module' => 'default',
                    'controller' => 'menus',
                    'action' => 'save-menu',
                    'type' => $type), 'admin'));
        $this->_forms['Menu' . ucfirst($type)]->setMethod('post');

        return $this->_forms['Menu' . ucfirst($type)];
    }

    protected function _getMenuitemForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['MenuItem' . ucfirst($type)] = $this->_modelMenu->getForm('MenuItem' . ucfirst($type));
        $this->_forms['MenuItem' . ucfirst($type)]->setAction($urlHelper->url(array(
                    'module' => 'default',
                    'controller' => 'menus',
                    'action' => 'save-menu-item',
                    'type' => $type), 'admin'));
        $this->_forms['MenuItem' . ucfirst($type)]->setMethod('post');

        return $this->_forms['MenuItem' . ucfirst($type)];
    }

}