<?php

/**
 * AdminController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class AdminController extends Zend_Controller_Action
{

    /**
     * @var Default_Model_Settings
     */
    protected $_modelSetting;

    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelSetting = new Default_Model_Settings();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
        
    }

    public function helpAction()
    {

        $modelFaq = new Default_Model_Faq();

        $this->view->faq_category_id = (int) $this->_getParam('id', 0);

        $this->view->category = $modelFaq->getCategoryFaqById($this->view->faq_category_id);

        $this->view->faqs = $modelFaq->getFaqsByCategory($this->view->faq_category_id);
    }

   

}
