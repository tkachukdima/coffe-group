<?php

/*
 * IndexController
 *
 * @menu   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class IndexController extends Zend_Controller_Action
{

    protected $_forms = array();

    public function init()
    { 
        $this->view->currentLang = Zend_Registry::get('Current_Lang');        
    }

    public function indexAction()
    {
        $this->_helper->getHelper('layout')->setLayout('layout_main');

        $pageModel = new Default_Model_Page();
        $page = $pageModel->getPageByType('main');

        if(!is_null($page)) {
            $this->view->headTitle(strip_tags($page->page_title), 'PREPEND');
            $this->view->headMeta()->appendName('description',  strip_tags($page->meta_description));
            $this->view->headMeta()->appendName('keywords', strip_tags($page->meta_keywords));
            $this->view->page = $page;
        }      
        
        $this->view->is_login = (boolean) $this->getRequest()->getParam('is_login', false);
     }
      
     public function sitemapAction()
    {           
        $this->view->title = $this->view->translate(_('Site map'));
        $this->view->headTitle($this->view->title, 'PREPEND');
        $this->view->headMeta()->appendName('description', $this->view->title);
        $this->view->headMeta()->appendName('keywords', $this->view->title);

        $menuModel = new Default_Model_Menu();
        //$pages_container = $menuModel->getMenuTree('main');
        //$secondary_container = $menuModel->getMenuTree('secondary');

        //$sitemap_container = array_merge($secondary_container, $pages_container);
        $sitemap_container = $menuModel->getMenuTree('secondary');
        
        $this->_publicationModel = new Publication_Model_Publication();

        $publicationGroups = $this->_publicationModel->getPublicationGroupsInMenu();

        $publication_groups_container = array();
        foreach ($publicationGroups as $key => $group) {
            $publication_groups_container[$group->ident] = array(
                'title' => $group->name,
                'label' => $group->name,
                'route' => 'default_publication',
                'params' => array('groupIdent' => $group->ident, 'lang' => $this->view->currentLang));
                
            $publications = $this->_publicationModel->getPublicationsByGroupIdent(null, null, 100, $group->ident);
            $publications_container = array();
            foreach ($publications as $key2 => $publication) {
                $publications_container[$key2] = array(
                    'title' => $publication->title,
                    'label' => $publication->title,
                    'module' => 'default',
                    'controller' => 'publication',
                    'action' => 'view',
                    'route' => 'default_publication_detail',
                    'params' => array('groupIdent' => $publication->publication_group_ident,
                        'publicationIdent' => $publication->ident,
                        'lang' => $this->view->currentLang));
            }

            $publication_groups_container[$group->ident]['pages'] = $publications_container;
        }
//print_r($publication_groups_container); die;       
//     
//
//        $this->_catalogModel = new Catalog_Model_Catalog();
//        $catalog_container = $this->_catalogModel->getCategoriesTree($withProduct = true);
//                
//        $this->_modelGallery = new Gallery_Model_Gallery();
//        $gallery_albums  = $this->_modelGallery->getAlbumsByParentId(0);
//        $gallery_albums_container = array();
//        foreach ($gallery_albums as $key => $album) {
//            $gallery_albums_container[$key] = array('title' => $album->name,
//                'label' => $album->name,
//                'module' => 'default',
//                'controller' => 'gallery',
//                'action' => 'index',
//                'route' => 'default_gallery',
//                'params' => array('albumIdent' => $album->ident));
//        } 
        
        $this->_catalogModel = new Catalog_Model_Catalog();
        $catalog_container = $this->_catalogModel->getCached('product')->getCategoriesTree($withProduct = true);
        
        $new_container = array();
        
         foreach ($sitemap_container as $key => $item) {
             $new_container[$key] = $item;
             if($item['controller'] == 'publication' AND !is_null($publication_groups_container)) {
                  $new_container[$key]['pages'] = $publication_groups_container[$item['params']['groupIdent']]['pages'];
             }            
             
//             if($item['controller'] == 'publication' AND $item['action'] == 'all' AND !is_null($publication_groups_container)) {
//                  $new_container[$key]['pages'] = $publication_groups_container[];
//             }            
//             if($item['controller'] == 'index' AND $item['action'] == 'index' AND $item['module'] == 'catalog'  AND !is_null($catalog_container)){
//                 $new_container[$key]['pages'] = $catalog_container;                 
//             }
//             if($item['controller'] == 'gallery' AND $item['action'] == 'index'  AND !is_null($gallery_albums_container)){
//                 $new_container[$key]['pages'] = $gallery_albums_container;
//             }
             if ($item['controller'] == 'index' AND $item['action'] == 'index' AND $item['module'] == 'catalog' AND !is_null($catalog_container)) {
                $new_container[$key]['pages'] = $catalog_container;
            }
         }


                
         if ($this->getRequest()->getParam('xml')) {
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->getHelper('layout')->disableLayout();
              $this->getResponse()->setHeader('Content-Type', 'text/xml; charset=utf-8')
            ->setBody($this->view->navigation()->sitemap(new Zend_Navigation($new_container))->setFormatOutput(true));
        } else {
            //  print_r($res); die();
            $this->view->container = new Zend_Navigation($new_container);
        }
    }
    
 
    public function skypeIconAction()
    {
        $site_settings = Zend_Registry::get('site_settings'); 

        return $this->_helper->json(array(
                    'status' => 'success',
                    'icon_img' => $this->view->getSkypeIcon($site_settings['skype'])
                ));
    }

}
