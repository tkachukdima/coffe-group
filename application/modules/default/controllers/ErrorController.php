<?php

/*
 * IndexController
 *
 * @menu   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
    
        $this->_helper->getHelper('layout')->setLayout('layout_ns');

        $this->view->currentLang = Zend_Registry::get('Current_Lang');
        
        $this->view->isAdmin = $this->_getParam('isAdmin', null);

        $errors = $this->_getParam('error_handler');

        switch (get_class($errors->exception)) {
            case 'Zend_Controller_Dispatcher_Exception':
                // send 404
                $this->getResponse()
                        ->setRawHeader('HTTP/1.0 404 Not Found')
                        ->setRawHeader('HTTP/1.1 404 Not Found')
                        ->setRawHeader('Status: 404 Not Found');

                $this->view->message = $this->view->translate('404 page not found.');
                break;
            case 'ARTCMF_Exception_404':
                // send 404
                $this->getResponse()
                        ->setRawHeader('HTTP/1.0 404 Not Found')
                        ->setRawHeader('HTTP/1.1 404 Not Found')
                        ->setRawHeader('Status: 404 Not Found');
                $this->view->message = $errors->exception->getMessage();
                break;
            case 'ARTCMF_Exception_Paid':
                // send 404
                $this->getResponse()
                        ->setRawHeader('HTTP/1.0 404 Not Found')
                        ->setRawHeader('HTTP/1.1 404 Not Found')
                        ->setRawHeader('Status: 404 Not Found');
                $this->view->message = $errors->exception->getMessage();
                return $this->render('paid');
                break;
            case 'ARTCMF_Acl_Exception':
                $this->view->message = $errors->exception->getMessage();
                break;
            default:
                // application error
                $this->view->message = $errors->exception->getMessage();
                break;
        }

        // Log exception, if logger available
        if ($log = $this->getLog()) {
            $log->log($this->view->message, $priority, $errors->exception);
            $log->log('Request Parameters', $priority, $errors->request->getParams());
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request = $errors->request;         
    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (!$bootstrap->hasResource('Log')) {
            return false;
        }
        $log = $bootstrap->getResource('Log');
        return $log;
    }

}
