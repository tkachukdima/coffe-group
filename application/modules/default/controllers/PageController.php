<?php

/*
 * PageController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class PageController extends Zend_Controller_Action
{

    /**
     * @var Default_Model_Page
     */
    protected $_modelPage;
    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelPage = new Default_Model_Page();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
        
        $request = $this->getRequest();       
       
        if($this->_getParam('print', 0))
            $this->view->layout()->disableLayout();

        $this->view->page = $this->_modelPage->getPageByIdent($request->getParam('pageIdent', 0));

        /* ******************* */
        switch ($this->view->page->type) {
            case 'products':
                $this->_helper->getHelper('layout')->setLayout('layout_nb');
                // get product publication
                $_modelPublication = new Publication_Model_Publication();
                $this->view->products = $_modelPublication->getPublicationsByGroupIdent(null, null, 10, 'products', $rand = false, $status = null);
                break;
            
            default:
                $this->_helper->getHelper('layout')->setLayout('layout_ns');
                break;
        }
        /* ******************* */

        if (null === $this->view->page) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')) . ' ' . $request->getParam('pageIdent'));
        }
        
        $this->view->headTitle($this->view->page->page_title, 'PREPEND');
        $this->view->headMeta()->appendName('description', $this->view->page->meta_description);
        $this->view->headMeta()->appendName('keywords', $this->view->page->meta_keywords);
       
    }

    public function listAction()
    {

        $this->view->pages = $this->_modelPage->getPages();
    }

    public function addAction()
    {

        $this->view->pageForm = $this->_getForm('add');
    }

    public function editAction()
    {
        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')) . ' ' . $this->_getParam('id'));
        }

        $page_id = (int) $this->_getParam('id');
        $this->view->page = $this->_modelPage->getPageByIdForEdit($page_id);
        $this->view->pageForm = $this->_getForm('edit')->populate($this->view->page);
    }

    public function saveAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelPage->savePage($request->getPost(), $type)) {
            $this->view->pageForm = $this->_getForm($type)->populate($request->getPost());
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Page added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Page updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'list',
            'controller' => 'page',
            'module' => 'default'), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($page_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Page not found')) . ' ' . $page_id);
        }

        $this->_modelPage->deletePage($page_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'list',
            'controller' => 'page',
            'module' => 'default'), 'admin', true);
    }

    protected function _getForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type] = $this->_modelPage->getForm('Page' . ucfirst($type));
        $this->_forms[$type]->setAction($urlHelper->url(array(
                    'controller' => 'page',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms[$type]->setMethod('post');

        return $this->_forms[$type];
    }

}