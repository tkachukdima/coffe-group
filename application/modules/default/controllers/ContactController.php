<?php

/**
 * Contact Controller
 *
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class ContactController extends Zend_Controller_Action
{

    protected $_forms = array();
    protected $_modelContact;

    public function init()
    {
        $this->_modelContact = new Default_Model_Contact();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {
        $this->_helper->getHelper('layout')->setLayout('layout_ns');
         
        $this->view->contactSetting = $this->_modelContact->getAllContactSettingById(1);

        if ($this->view->contactSetting) {
            $this->view->headTitle($this->view->contactSetting->title, 'PREPEND');
            $this->view->headMeta()->appendName('description', $this->view->contactSetting->meta_description);
            $this->view->headMeta()->appendName('keywords', $this->view->contactSetting->meta_keywords);
        }

        $request = $this->getRequest();

        $this->view->feedbackForm = $this->_getFeedbackForm();

        if ($request->isPost()) {
            if (false !== $this->_modelContact->sendFeedback($request->getPost())) {
                $this->view->message = $this->view->translate('Thank you! Your message is received.');
                $this->view->feedbackForm->reset();
            } else {
//                print_r( $this->view->feedbackForm->getMessages());
                $this->view->message = $this->view->translate('Error! Maybe you made a mistake when filling out form.');
            }
        }
    }
    
    public function callbackAction()
    {
        $request = $this->getRequest();
        

        if (!$request->isPost()) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not found')));
        }
        
        $callbackForm = new Default_Form_Contact_Callback(array('model' => $this->_modelContact));
        
        $data = $this->_modelContact->sendCallback($request->getPost(), $callbackForm);
        $out = array('status'=>'error');
        if (is_array($data))
        {
            $out['error'] = $data;
        }
        else 
        {
            $out = array('status' => 'success', 'message' => $this->view->translate('Your request has been accepted, wait for the call!')); 
        }
               
        $this->_helper->json($out);
    }


    public function captchaAction()
    {
        if ($this->_request->isPost()) {
            $data = array();

            $feedbackForm = $this->_getFeedbackForm();

            $captcha = $feedbackForm->getElement('captcha')->getCaptcha();

            $data['id'] = $captcha->generate();
            $data['src'] = $captcha->getImgUrl() . $captcha->getId() . $captcha->getSuffix();


            $this->_helper->json($data);
        }
    }

    
    public function settingsAction()
    {        
        $this->view->contactSetting = $this->_modelContact->getContactSettingByIdForEdit(1);
                             
        $this->view->settingsForm = $this->_getContactSettingForm()->populate($this->view->contactSetting);
        
        if(!$this->view->contactSetting['map_image_full'] )
            $this->view->settingsForm->removeElement('delete_image');
       
    }

    public function saveSettingAction()
    {

        $request = $this->getRequest();
      
        if (!$request->isPost()) {
            return $this->_helper->redirector('settings');
        }

        if (false === $this->_modelContact->saveContactSettings($request->getPost())) {           
            $this->view->settingsForm = $this->_getContactSettingForm();
            $this->view->settingsForm->setDescription($this->view->translate(_('Error! Maybe you made a mistake when filling out form.')));
            return $this->render('settings');
        }

        $message = $this->view->translate(_('Settings updated'));
   
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'settings',
            'controller' => 'contact',
            'module' => 'default'), 'admin', true);
    }
   
 
    protected function _getFeedbackForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['ContactFeedback'] = $this->_modelContact->getForm('ContactFeedback');
        $this->_forms['ContactFeedback']->setAction($urlHelper->url(array(
                    'controller' => 'contact',
                    'action' => 'index'
                        ), 'default'
                ) . '#feedback_form');
        $this->_forms['ContactFeedback']->setMethod('post');

        return $this->_forms['ContactFeedback'];
    }
    
   protected function _getContactSettingForm()
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['ContactSetting'] = $this->_modelContact->getForm('ContactSetting');
        $this->_forms['ContactSetting']->setAction($urlHelper->url(array(
                    'controller' => 'contact',
                    'action' => 'save-setting'
                        ), 'admin', true
                ));
        $this->_forms['ContactSetting']->setMethod('post');

        return $this->_forms['ContactSetting'];
    }

   
}