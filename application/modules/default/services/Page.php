<?php
/**
 * Default_Service_Page
 *
 * @category   Default
 * @package    Default_Service
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Service_Page
{
    protected $_pageModel;

    public function __construct()
    {
        $this->_pageModel = new Default_Model_Page();
    }

    public function getPageById($id)
    {
        return $this->_pageModel->getPageById($id);
    }
}
