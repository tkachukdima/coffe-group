<?php

/**
 * Default_Service_Search
 *
 * Provides search on the index
 *
 * @category   Default
 * @package    Default_Service
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Default_Service_Search
{

    protected $_index;

    public function __construct($index)
    {
        $this->_index = $index;
    }

    public function query(ARTCMF_Search_Searcher_Interface $searcher)
    {
        $hits = $this->_index->find($searcher->parse());
        return $hits;
    }

}