<?php

return array(
    'Guest' => array(
        'allow' => array(
            'contact' => array(
                'index',
                //'representations',
                'captcha',
                //'connect',
                'callback'
            ),
            'error' => array(
                'error'
            ),
            'index' => array(
                'index',
                'sitemap',
                'skype-icon'
            ),
            'page' => array(
                'index'
            ),
           /* 'faq' => array(
                'index',
                'submit-ajax'
            ),*/
            'cron' => array(
                'index'
            ),
            'search' => array(
                'index'
            ),
        )
    ),
    'Member' => array(
        'allow' => array(
            
        )
    ),
    'Redactor' => array(
        'allow' => array(
            'admin' => array(
                'index',
                'help'               
            ),
            'search' => array(
                'update-index'
            ),
        )
    ),
    'Manager' => array(
        'allow' => array(            
            'contact' => array(
                'list-feedback',
                'settings',
                'save-setting'
            ),
			'settings' => array(
				'index',
				'save',
				'edit'
			),
            'page' => array(
                'list',
                'add',
                'edit',
                'save',
                'delete'
            ),
//            'representation' => array(
//                'list',
//                'add',
//                'edit',
//                'save',
//                'delete'
//            ),
           /* 'faq' => array(
                'list',
                'list-categories',
                'edit-category',
                'edit-faq',
                'add-category',
                'add-faq',
                'save-category',
                'save-faq',
                'delete-category',
                'delete-faq',
                'sort'
            ),*/             
            
        )
    ),
    'Admin' => array(
        'allow' => array(            
            'menus' => array(
                'list',
                'list-menu-item',
                'add-menu-item',
                'edit-menu-item',
                'save-menu-item',
                'delete-menu-item',
                'sort'
            ),
            'settings' => array(
                'index',
                //'add',
                'edit',
                'save',
                //'delete',
                'sysinfo'
            ),
             'ipblocking' => array(
                'list',              
                'delete'
            )
        )
    )
);