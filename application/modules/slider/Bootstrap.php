<?php

/**
 * Slider_Bootstrap
 *
 * @category   Bootstrap
 * @package    Slider_Bootstrap
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Slider_Bootstrap extends ARTCMF_Application_Module_Bootstrap
{

    public function _initModuleResourceAutoloader()
    {
        $this->getResourceLoader()->addResourceTypes(array(
            'modelResource' => array(
                'path' => 'models/resources',
                'namespace' => 'Resource',
            ),
            'modelFilter' => array(
                'path' => 'models/filter',
                'namespace' => 'Filter',
            )
        ));
    }

    protected function _initRoutes()
    {
        $this->bootstrap('frontController');

        $router = $this->frontController->getRouter();

        $routes_file = APPLICATION_PATH . '/modules/slider/configs/routes.php';
        if (file_exists($routes_file)) {
            $routes_array = include $routes_file;            
            foreach ($routes_array as $routeName => $route) {
                $router->addRoute($routeName, $route);
            }
        }
    }

}
