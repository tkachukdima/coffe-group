<?php

/**
 * Slider_Resource_Slider
 *
 * @category   Default
 * @package    Slider_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Slider_Resource_Slider extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'slider';
    protected $_primary = 'slider_id';
    protected $_rowClass = 'Slider_Resource_Slider_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getSliderById($id)
    {
        return $this->find($id)->current();
    }
    
    public function getSlidersByArrayId($array_id)
    { 
       $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.slider_id = {$this->_name}_translation.slider_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.slider_id IN (?)', $array_id)
                ->order($this->_name . '.sort_order');

                
        return $this->fetchAll($select);
    }
    
    public function getSliderByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.slider_id = {$this->_name}_translation.slider_id")
                
                ->where($this->_name . '.slider_id = ?', $id);

        return $this->fetchAll($select);
    }

    /**
     * Get a list of slider
     *
     * @param  boolean   $sliderd      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getSliders($status)
    { 
       $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.slider_id = {$this->_name}_translation.slider_id")
                
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.sort_order');

        if(null !== $status){
            $select->where($this->_name . '.status = ?', $status);
        }

                
        return $this->fetchAll($select);
    }

    public function getNearSort($sort_order, $move)
    {

        $select = $this->select();

        switch ($move) {
            case 'up':
                $select->where('sort_order < ?', $sort_order)
                        ->order('sort_order DESC')
                        ->limit(1, 0);
                break;

            case 'down':
                $select->where('sort_order > ?', $sort_order)
                        ->order('sort_order ASC')
                        ->limit(1, 0);
                break;

            default:
                break;
        }


        return $this->fetchAll($select);
    }
    
    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'slider_id' => $translate_data['slider_id'],
                    'language_code' => $translate_data['language_code'],
                    'title' => $translate_data['title'],
                    'url' => $translate_data['url'],
                    'description' => $translate_data['description']                    
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'title' => $translate_data['title'],
                    'url' => $translate_data['url'],
                    'description' => $translate_data['description']
                        ), "`slider_id` = {$translate_data['slider_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "slider_id = {$id}");
    }
    

}