<?php

/**
 * Slider_Resource_SliderGroup
 *
 * @category   Default
 * @package    Slider_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Slider_Resource_SliderGroup extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'slider_group';
    //protected $_primary = 'slider_id';
    protected $_rowClass = 'Slider_Resource_SliderGroup_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }
    
    public function deleteRows($slider_id)
    {
        $this->delete("slider_id = ".$slider_id);
    }
    
    public function getGroupsBySliderId($slider_id)
    {
        $select = $this->select();
        $select->where("slider_id = ?", $slider_id);
        return $this->fetchAll($select);
    }
    
    public function getSlidersByGroupId($module_id)
    {
        $select = $this->select();
        $select->where("module_id = ?", $module_id);
        return $this->fetchAll($select);
    }
    
}