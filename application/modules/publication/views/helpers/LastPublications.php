<?php

class Publication_View_Helper_LastPublications extends Zend_View_Helper_Abstract
{

    protected $publications;
    protected $count;

    public function lastPublications($group = null, $count = null)
    {
        $publicationModel = new Publication_Model_Publication();

        $this->publications = $publicationModel->getLastPublicationsByGroupIdent($group, $count, false, 1);
        $this->count = $publicationModel->getCountLastPublicationsByGroupIdent($group, 1);
        /*
          if(null !== $is_menu)
          return new Zend_Navigation($publications);
         */
        return $this;
    }

    public function getPublications()
    {
        return $this->publications;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function rdate($param, $time=0){
        
        if(intval($time) == 0) $time = time();
        $MonthNames = array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
        if(strpos($param,'M')===false) return date($param, $time);
            else return date(str_replace('M',$MonthNames[date('n',$time)-1],$param), $time);
    }

}