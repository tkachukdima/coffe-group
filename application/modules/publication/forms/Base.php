<?php

/**
 * Base publication Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Base extends ARTCMF_Form_Abstract
{
    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/publication');

        $this->setMethod('post');
        $this->setAction('');

        // get group select
        $form = new Publication_Form_Group_Select(
                        array('model' => $this->getModel())
        );

        $element = $form->getElement('publication_group_id');
        $element->clearDecorators()->loadDefaultDecorators();
        $element->setRequired(true);
        $this->addElement($element);

        //get category
        $form = new Publication_Form_Category_Selectcategory(
                        array('model' => $this->getModel())
        );

        $element = $form->getElement('publication_category_id');
        $element->clearDecorators()->loadDefaultDecorators();
        $element->setRequired(true);
 
        $this->addElement($element);


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));                       

            $element_name = 'body_' . $lang->code;
            $this->addElement('textarea', $element_name, array(
                'label' => _('Full Text'),
                'filters' => array('StringTrim'),
                'required' => true,
                'class' => "ckeditor"
            ));

            //$this->$element_name->addDecorator(new ARTCMF_Form_Decorator_CKEditor);

            $this->addElement('textarea', 'page_title_' . $lang->code, array(
                'label' => _('Page Title'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 2,
                'required' => false
            ));

            $this->addElement('textarea', 'meta_description_' . $lang->code, array(
                'label' => _('META Description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true
            ));

            $this->addElement('textarea', 'meta_keywords_' . $lang->code, array(
                'label' => _('META Keywords'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true,
            ));
            
            $this->addElement('text', 'description_img_' . $lang->code, array(
            'label' => _('Description of the image'),
            'filters' => array('StringTrim'),
            'required' => false,
            ));            
        }

        $this->addElement('text', 'date_post', array(
            'label' => _('Date post'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => date('Y-m-d H:i:s')
        ));

        $this->addElement('text', 'date_format', array(
            'label' => _('Date format'),
            'filters' => array('StringTrim'),
            'required' => true,
            'value' => 'd.m.Y'
        ));

        $this->addElement('Checkbox', 'delete_image', array(
            'label' => _('Delete Image')
        ));

        $this->addElement('file', 'full', array(
             'label' => _('Image'),
            'required' => false,
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg','jpeg','png','gif')),
            ),
        ));



        /*
          $this->addElement('text', 'video', array(
          'label' => 'Код видео с YouTube',
          'filters' => array('StringTrim'),
          'required' => false,
          'description' => 'Если необходимо добавить несколько видео, разделяйте коды точкой с запятой ( например: dfghD43FDSft;FDdshvsdft )'
          )); */


        $this->addElement('select', 'status', array(
            'label' => _('Status'),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

        $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array($this->getModel(), 'getPublicationByIdent', 'getPublicationById', 'publication_id'))
            ),
            'required' => false,
        ));

        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'value' => 0
        ));


        $this->addElement('submit', 'submit', array(
        ));

        $this->addElement('hidden', 'publication_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            
            $this->addDisplayGroup(array(
                'title_' . $lang->code,
                'body_' . $lang->code,
                'page_title_' . $lang->code,
                'meta_description_' . $lang->code,
                'meta_keywords_' . $lang->code,
                //'description_img_' . $lang->code
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }



            $filds = array('delete_image', 'full'/* , 'video' */);
            foreach (Zend_Registry::get('langList') as $key => $lang) {
                $filds[] = 'description_img_' . $lang->code;
            }

            $this->addDisplayGroup($filds, 'image', array('legend' => _('Image Settings')));


        $this->addDisplayGroup(array('publication_group_id', 'publication_category_id', 'ident', 'status', 'date_post', 'date_format', 'sort_order', 'submit', 'publication_id'), 'form_all', array('legend' => _('General Settings')));
    }

}
