<?php

/**
 * Base category Form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Group_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');

        $this->setMethod('post');
        $this->setAction('');


        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addElement('text', 'name_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
            ));

            $this->addElement('textarea', 'description_' . $lang->code, array(
                'label' => _('Description'),
                'filters' => array('StringTrim'),
                'required' => true,
                'cols' => 40,
                'rows' => 6,
            ));
            
             $this->addElement('textarea', 'page_title_' . $lang->code, array(
                'label' => _('Page Title'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 2,
                'required' => false
            ));

            $this->addElement('textarea', 'meta_description_' . $lang->code, array(
                'label' => _('META Description'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true
            ));

            $this->addElement('textarea', 'meta_keywords_' . $lang->code, array(
                'label' => _('META Keywords'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => true,
            ));
        }

        $this->addElement('text', 'items_per_page', array(
            'label' => _('Items on the page'),
            'filters' => array('StringTrim'),
            'required' => true,
            'description' => _('Items on the page'),
            'value' => '10'
        ));

        $this->addElement('Checkbox', 'image_status', array(
            'label' => _('Image status'),
        ));

        $this->addElement('Checkbox', 'image_resize', array(
            'label' => _('Image resize'),
        ));
        
        /*
        $this->addElement('text', 'period_newest', array(
            'label' => _('Период для новых (в днях)'),
            'filters' => array('StringTrim'),
            'required' => true,
            'description' => _('Период, за который публикация считается "новой"'),
            'value' => '7'
        ));*/
        
         $this->addElement('text', 'count_on_main', array(
            'label' => _('Сколько выводить на главной'),
            'filters' => array('StringTrim'),
            'required' => true,            
            'value' => '8'
        ));


        $this->addElement('text', 'ident', array(
            'label' => _('Character code'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array($this->getModel(), 'getPublicationGroupByIdent', 'getPublicationGroupById', 'publication_group_id'))
            ),
            'required' => false,
        ));
        
        
        // ALTER TABLE  `publication_group` ADD  `in_menu` INT( 1 ) NOT NULL
        /*$this->addElement('Checkbox', 'in_menu', array(
            'label' => _('Show on the publications')
        ));*/
        
        $this->addElement('submit', 'submit', array());

        $this->addElement('hidden', 'publication_group_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));

        foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'name_' . $lang->code,
                'description_' . $lang->code,
                'page_title_' . $lang->code,
                'meta_description_' . $lang->code,
                'meta_keywords_' . $lang->code,
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }


        $this->addDisplayGroup(array('image_status','image_resize', 'items_per_page', 'ident',/* 'period_newest', */'count_on_main', 'submit', 'publication_group_id'), 'form_all', array('legend' => _('General Settings')));
    }    

}
