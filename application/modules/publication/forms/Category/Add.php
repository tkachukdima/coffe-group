<?php

/**
 * Add new PublicationCategory
 *
 * @PublicationCategory   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Category_Add extends Publication_Form_Category_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        
        $this->removeElement('publication_category_id');
        $this->removeElement('delete_image');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
