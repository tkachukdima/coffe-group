<?php

/**
 * Category_Selectgroup
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Form_Category_Selectgroup extends ARTCMF_Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');

        $groups = $this->getModel()->getPublicationGroups()->toArray();
        
        foreach ($groups as $group) {
            $cats[$group['publication_group_id']] = $group['name'];
        }

        $this->addElement('select', 'publication_group_id', array(
            'label' => _('Select group'),
            'multiOptions' => $cats
        ));

        $this->addElement('submit', 'View', array(
            'decorators' => array('ViewHelper'),
        ));
    }

}
