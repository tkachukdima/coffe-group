<?php

/**
 * Publication_Resource_PublicationGroup
 * 
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Resource_Publicationgroup extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'publication_group';
    protected $_primary = 'publication_group_id';
    protected $_rowClass = 'Publication_Resource_Publicationgroup_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getPublicationGroupByIdent($ident, $ignoreRow = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_group_id = {$this->_name}_translation.publication_group_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.ident = ?', $ident);

        if (null !== $ignoreRow) {
            $select->where($this->_name . '.ident < ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }

    public function getPublicationGroupByIdForSave($id)
    {
        return $this->find($id)->current();
    }

    public function getPublicationGroupById($id)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_group_id = {$this->_name}_translation.publication_group_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.publication_group_id = ?', $id);


        return $this->fetchRow($select);
    }

    public function getPublicationGroupByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_group_id = {$this->_name}_translation.publication_group_id")
                ->where($this->_name . '.publication_group_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getPublicationGroups()
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_group_id = {$this->_name}_translation.publication_group_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '_translation' . '.name');
        return $this->fetchAll($select);
    }

    public function getPublicationGroupsInMenu($in_menu)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_group_id = {$this->_name}_translation.publication_group_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                // ->where($this->_name.' . in_menu = ?',$in_menu)
                ->order($this->_name . '_translation' . '.name');
        return $this->fetchAll($select);
    }

    public function saveTranslatedRows($translate_data)
    {

        $this->getAdapter()->insert($this->_name . '_translation', array(
            'publication_group_id' => $translate_data['publication_group_id'],
            'language_code' => $translate_data['language_code'],
            'name' => $translate_data['name'],
            'description' => $translate_data['description'],
            'page_title' => $translate_data['page_title'],
            'meta_description' => $translate_data['meta_description'],
            'meta_keywords' => $translate_data['meta_keywords']
        ));
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "publication_group_id = {$id}");
    }

}
