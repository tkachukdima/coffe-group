<?php

/**
 * Publication_Resource_Publication
 *
 * @category   Default
 * @package    Default_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Publication_Resource_Publication extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'publication';
    protected $_primary = 'publication_id';
    protected $_rowClass = 'Publication_Resource_Publication_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getPublicationById($id, $with_translate = false)
    {
        if ($with_translate) {
            $select = $this->select();
            $select->from($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                    ->where($this->_name . '.publication_id = ?', $id);

            return $this->fetchRow($select);
        } else {
            return $this->find($id)->current();
        }
    }

    public function getPublicationByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                ->where($this->_name . '.publication_id = ?', $id);

        return $this->fetchAll($select);
    }

    public function getPublicationByIdent($ident, $ignoreRow)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where($this->_name . '.ident = ?', $ident);

        if (null !== $ignoreRow) {
            $select->where($this->_name . '.ident <> ?', $ignoreRow->ident);
        }

        return $this->fetchRow($select);
    }

    public function getRandomPublicationImage($limit = NULL, $publicationGrourIdent = NULL)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id");

        if ($publicationGrourIdent != NULL) {
            $select->joinLeft('publication_group', "publication_group.publication_group_id = {$this->_name}.publication_group_id", array('groupIdent' => 'ident'))
                    ->where('publication_group.ident = ?', $publicationGrourIdent);
        };
        $select->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->where("{$this->_name}.thumbnail <> ' '")
                ->order('rand()');
        if ($limit != NULL)
            $select->limit($limit);
//      echo $select->__toString();  die;
        return $this->fetchAll($select);
    }

    /**
     * Get a list of publication
     *
     * @param  boolean   $paged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getPublications($paged = null, $limit = null, $per_page = 10, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.sort_order ASC')
                ->order($this->_name . '.date_post DESC');

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        if (null !== $limit) {
            $select->limit($limit);
        }

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from('publication', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'))
                    ->from($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang);
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage($per_page)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }

    /**
     * Get a list of publication ByGroup
     *
     * @param  boolean   $paged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getPublicationsByGroupId($paged = null, $limit = null, $per_page = 10, $publication_group_id = null, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.sort_order ASC')
                ->order($this->_name . '.date_post DESC');

        if (null !== $limit) {
            $select->limit($limit);
        }

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        if (null !== $publication_group_id) {
            $select->where($this->_name . '.publication_group_id = ?', $publication_group_id);
        }

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from($this->_name, new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'))
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->where($this->_name . '.publication_group_id = ?', $publication_group_id)
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang);
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage($per_page)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }

    public function getCountPublicationsByGroupIdent($publication_group_ident, $status = null)
    {
        $select = $this->select();
        $select->from($this->_name, 'COUNT(*) AS num')
                ->setIntegrityCheck(false)
                ->joinLeft('publication_group', $this->_name . '.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'));

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        if ($publication_group_ident)
            $select->where('publication_group.ident = ?', $publication_group_ident);

        // echo $select->__toString();die;

        return $this->fetchRow($select);
    }

    /**
     * Get a list of publication By Category
     *
     * @param  boolean   $paged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getPublicationsByCategoryId($paged = null, $limit = null, $per_page = 10, $publication_category_id = null, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.sort_order ASC')
                ->order($this->_name . '.date_post DESC');

        if (null !== $limit) {
            $select->limit($limit);
        }

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        if (null !== $publication_category_id) {
            $select->where($this->_name . '.publication_category_id = ?', $publication_category_id);
        }

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);
            $count->from('publication', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'))
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->where($this->_name . '.publication_category_id = ?', $publication_category_id)
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang);
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage($per_page)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }
        return $this->fetchAll($select);
    }

    public function getCountPublicationsByCategoryIdent($publication_category_ident, $status = null)
    {
        $select = $this->select();
        $select->from($this->_name, 'COUNT(*) AS num')
                ->setIntegrityCheck(false)
                ->joinLeft('publication_group', $this->_name . '.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'));

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        if ($publication_category_ident)
            $select->where('publication_group.ident = ?', $publication_category_ident);

        return $this->fetchRow($select);
    }

    /**
     * Get a list of publication ByGroup
     *
     * @param  
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getPublicationsByGroupIdent($paged = null, $limit = null, $per_page = 10, $publication_group_ident = null, $rand = false, $status = null, $order = null)
    {
        $select = $this->select();

        if (null !== $limit) {
            $select->limit($limit);
        }
        //die("=".$status);

        if (null !== $order)
            $select->order($order);


        if (null !== $publication_group_ident) {
            $select->setIntegrityCheck(false) // отключаем режим целосности, это значит, что save(), delete() и методы для установки значений полей в "заблокированном" состоянии
                    ->from($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->joinLeft('publication_group', 'publication.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'))
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                    ->where('publication_group.ident = ?', $publication_group_ident)
                    ->order($this->_name . '.sort_order ASC')
                    ->order($this->_name . '.date_post DESC');
        }

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        //print_r($select->__toString());die;

        if (null !== $paged) {
            $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
            $count = clone $select;
            $count->reset(Zend_Db_Select::COLUMNS);
            $count->reset(Zend_Db_Select::FROM);

            $count->from('publication', new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`'))
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->joinLeft('publication_group', 'publication.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'))
                    ->where('publication_group.ident = ?', $publication_group_ident)
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang);
            $adapter->setRowCount($count);

            $paginator = new Zend_Paginator($adapter);
            $paginator->setItemCountPerPage($per_page)
                    ->setCurrentPageNumber((int) $paged);
            return $paginator;
        }

        if ($rand)
            $select->order(new Zend_Db_Expr('RAND()'));

        //echo $select->__toString(); die;
        return $this->fetchAll($select);
    }

    public function getLastPublication()
    {

        return $this->fetchRow(
                        $this->select()
                                ->from($this, array(new Zend_Db_Expr('max(publication_id) as maxId')))
        );
    }

    /**
     * Get a list of publication ByGroup
     *
     * @param  
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getLastPublicationsByGroupIdent($publication_group_ident = null, $limit = null, $rand = false, $status = null, $order = null)
    {
        $select = $this->select();


        if (null !== $order)
            $select->order($order);


        if (null !== $publication_group_ident) {
            $select->setIntegrityCheck(false) // отключаем режим целосности, это значит, что save(), delete() и методы для установки значений полей в "заблокированном" состоянии
                    ->from($this->_name)
                    ->setIntegrityCheck(false)
                    ->joinLeft($this->_name . '_translation', "{$this->_name}.publication_id = {$this->_name}_translation.publication_id")
                    ->joinLeft('publication_group', 'publication.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'))
                    ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                    ->where('publication_group.ident = ?', $publication_group_ident)
                    ->order($this->_name . '.sort_order ASC')
                    ->order($this->_name . '.date_post DESC');
        }

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', $status);
        }

        if (null !== $limit) {
            $select->limit($limit);
        }

        if ($rand)
            $select->order(new Zend_Db_Expr('RAND()'));

        // echo $select->__toString();die;
        return $this->fetchAll($select);
    }

    public function getCountLastPublicationsByGroupIdent($publication_group_ident, $status = null, $period = null)
    {
        $select = $this->select();
        $select->from($this->_name, 'COUNT(*) AS num')
                ->setIntegrityCheck(false)
                ->joinLeft('publication_group', $this->_name . '.publication_group_id = publication_group.publication_group_id', array('publication_group_ident' => 'publication_group.ident'));

        if (null !== $status) {
            $select->where($this->_name . '.status = ?', (int) $status);
        }

        if ($publication_group_ident)
            $select->where('publication_group.ident = ?', $publication_group_ident);

        if (!is_null($period)) {
            $select->where('DATEDIFF(SYSDATE(), publication.date_post) <= ?', (int) $period);
        }
        // echo $select->__toString();die;
        return $this->fetchRow($select);
    }

    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'publication_id' => $translate_data['publication_id'],
                    'language_code' => $translate_data['language_code'],
                    'title' => $translate_data['title'],
                    'body' => $translate_data['body'],
                    'page_title' => $translate_data['page_title'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords'],
                    'description_img' => $translate_data['description_img'],
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'title' => $translate_data['title'],
                    'body' => $translate_data['body'],
                    'page_title' => $translate_data['page_title'],
                    'meta_description' => $translate_data['meta_description'],
                    'meta_keywords' => $translate_data['meta_keywords'],
                    'description_img' => $translate_data['description_img']
                        ), "`publication_id` = {$translate_data['publication_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "publication_id = {$id}");
    }

}
