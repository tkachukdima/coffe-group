<?php

return array(
    'Guest' => array(// role
        'allow' => array(// permission (allow/deny)
            'index' => array(
                'index',
                'view',
                'rss'
            ), // privilege (action)
        )
    ),
    'Member' => array(
        'allow' => array(
            'index' => array(// resource (controller)
//                'all',                
            )
        )
    ),
    'Redactor' => array(
        'allow' => array(
            'management' => array(
                'list',
                'all',
                'catalog',
                'add-publication',
                'edit-publication',
                'save-publication',
                'delete-publication',
//                'add-publication-category',
//                'edit-publication-category',
                'edit-publication-group',
                'save-publication-group',
                'list-publication-group',
//              'add-publication-group',
//                'save-publication-category',
//                'list-publication-category',
//                'delete-publication-category',
                'get-categories-group-ajax',
                'get-group-image-status-ajax',
                'crop',
                'crop-category'
            )
        )
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);