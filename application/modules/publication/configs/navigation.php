<?php

return array(
    array(
        'label' => _('Publications'),
        'module' => 'publication',
        'controller' => 'management',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'publication:management',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-publication',
        'pages' => array(
            array(
                'label' => _('Publications'),
                'module' => 'publication',
                'controller' => 'management',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'publication:management',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit Post'),
                        'module' => 'publication',
                        'controller' => 'management',
                        'action' => 'edit-publication',
                        'route' => 'admin',
                        'resource' => 'publication:management',
                        'privilege' => 'edit-publication',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit Post'),
                        'module' => 'publication',
                        'controller' => 'management',
                        'action' => 'save-publication',
                        'route' => 'admin',
                        'resource' => 'publication:management',
                        'privilege' => 'save-publication',
                        'reset_params' => true
                    ),
                )
            ),
            array(
                'label' => _('Add publication'),
                'module' => 'publication',
                'controller' => 'management',
                'action' => 'add-publication',
                'route' => 'admin',
                'resource' => 'publication:management',
                'privilege' => 'add-publication',
                'reset_params' => true,
            ),
            array(
                'label' => '---------------------------------------',
                'uri' => '#'
            ),
            array(
                'label' => _('Groups'),
                'module' => 'publication',
                'controller' => 'management',
                'action' => 'list-publication-group',
                'route' => 'admin',
                'resource' => 'publication:management',
                'privilege' => 'list-publication-group',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit group'),
                        'module' => 'publication',
                        'controller' => 'management',
                        'action' => 'edit-publication-group',
                        'route' => 'admin',
                        'resource' => 'publication:management',
                        'privilege' => 'edit-publication-group',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit group'),
                        'module' => 'publication',
                        'controller' => 'management',
                        'action' => 'save-publication-group',
                        'route' => 'admin',
                        'resource' => 'publication:management',
                        'privilege' => 'save-publication-group',
                        'reset_params' => true
                    ),
                )
            ),
            array(
                'label' => _('Add group'),
                'module' => 'publication',
                'controller' => 'management',
                'action' => 'add-publication-group',
                'route' => 'admin',
                'resource' => 'publication:management',
                'privilege' => 'add-publication-group',
                'reset_params' => true
            ),
            array(
                'label' => _('Categories'),
                'module' => 'publication',
                'controller' => 'management',
                'action' => 'list-publication-category',
                'route' => 'admin',
                'resource' => 'publication:management',
                'privilege' => 'list-publication-category',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit category'),
                        'module' => 'publication',
                        'controller' => 'management',
                        'action' => 'edit-publication-category',
                        'route' => 'admin',
                        'resource' => 'publication:management',
                        'privilege' => 'edit-publication-category',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit category'),
                        'module' => 'publication',
                        'controller' => 'management',
                        'action' => 'save-publication-category',
                        'route' => 'admin',
                        'resource' => 'publication:management',
                        'privilege' => 'save-publication-category',
                        'reset_params' => true
                    ),
                )

            ),
            array(
                'label' => _('Add category'),
                'module' => 'publication',
                'controller' => 'management',
                'action' => 'add-publication-category',
                'route' => 'admin',
                'resource' => 'publication:management',
                'privilege' => 'add-publication-category',
                'reset_params' => true
            ),
        )
    )
);