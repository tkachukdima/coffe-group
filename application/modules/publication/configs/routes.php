<?php

return array(
    'default_publication_detail' => new Zend_Controller_Router_Route_Regex(
            '([a-z]{2})/publication/view/([a-zA-Z-_0-9]+)/([a-zA-Z-_0-9]+)\.html',
            array(
                'module' => 'publication',
                'controller' => 'index',
                'action' => 'view',
                'lang' => 'ru',
                'groupIdent' => '',
                'publicationIdent' => ''
            ),
            array(
                1 => 'lang',
                2 => 'groupIdent',
                3 => 'publicationIdent',
            ),
            '%s/publication/view/%s/%s.html'
    ),
    'default_publication_category' => new Zend_Controller_Router_Route(
            ':lang/publication/category/:groupIdent/:categoryIdent',
            array(
                'module' => 'publication',
                'controller' => 'index',
                'action' => 'index',
                'lang' => 'ru',
                'groupIdent' => '',
                'categoryIdent' => ''
            ),
            array(
                'lang' => '[a-z]{2}',
                'groupIdent' => '[a-zA-Z-_0-9]+',
                'categoryIdent' => '[a-zA-Z-_0-9]+'
            )
    ),
    'default_publication' => new Zend_Controller_Router_Route(
            ':lang/publication/:groupIdent/:page',
            array(
                'module' => 'publication',
                'controller' => 'index',
                'action' => 'index',
                'lang' => 'ru',
                'groupIdent' => '',
                'page' => 1
            ),
            array(
                'lang' => '[a-z]{2}',
                'groupIdent' => '[a-zA-Z-_0-9]+',
                'page' => '\d+'
            )
    ),
    'default_publication_all' => new Zend_Controller_Router_Route(
            ':lang/publication/all',
            array(
                'module' => 'publication',
                'controller' => 'index',
                'action' => 'all',
                'lang' => 'ru'
            ),
            array(
                'lang' => '[a-z]{2}'
            )
    ),
    'default_publication_rss' => new Zend_Controller_Router_Route(
            ':lang/publication/rss/:groupIdent',
            array(
                'module' => 'publication',
                'controller' => 'index',
                'action' => 'rss',
                'lang' => 'ru',
                'groupIdent' => ''
            ),
            array(
                'lang' => '[a-z]{2}',
                'groupIdent' => '[a-zA-Z-_0-9]+'
            )
    )
);