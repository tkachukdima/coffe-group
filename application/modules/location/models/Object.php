<?php

class Location_Model_Object extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a object post by its id
     *
     * @param  string $ident The ident
     * @return Location_Resource_Object_Item
     */
    public function getObjectById($id)
    {
        $id = (int) $id;
        return $this->getResource('Object')->getObjectById($id);
    }

    /**
     * Get object
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getObjects($status = null)
    {
        return $this->getResource('Object')->getObjects($status);
    }

    /**
     * Save a object post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveObject($data, $validator = null)
    {
        if (!$this->checkAcl('saveObject')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/object');

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Object' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        // get post data
        $data = $validator->getValues();

        $object = array_key_exists('object_id', $data) ?
                $this->getResource('Object')->getObjectById($data['object_id']) : null;


//        print_r($object);
//        print_r($data);
//        exit;
//
//        if ($object AND $data['delete_image'] == 0 AND $data['image'] == '') {
//            $data['image'] = $object->image;
//            $data['thumbnail'] = $object->thumbnail;
//            $data['preview'] = $object->preview;
//        } elseif ($data['delete_image'] == 1) {
//            unlink($fileDestination . '/' . $data['image']);
//            unlink($fileDestination . '/' . $data['thumbnail']);
//            unlink($fileDestination . '/' . $data['preview']);
//        }


        $new = $this->getResource('Object')->saveRow($data, $object);

        $data['object_id'] = $new;

        return $new;
        
        if (!$data['image']) {
            return $new;
        } else {


            $filter = new ARTCMF_Filter_ImageSize();


            $path_parts = pathinfo($data['image']);
            $new_file_name = 'object-' . $new . '.' . $path_parts['extension'];
            rename($fileDestination . '/' . $data['image'], $fileDestination . '/' . $new_file_name);
            $data['image'] = $new_file_name;

            $settingModel = new Default_Model_Settings();
            $image_resize_setting = $settingModel->getImageResizeSettingForModel('object');


            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name);


            $data['thumbnail'] = basename($thumbnail);
            $data['preview'] = basename($preview);


            $new_object = $this->getResource('Object')->getObjectById($new);

            return $this->getResource('Object')->saveRow($data, $new_object);
        }
    }

    public function deleteObject($id)
    {
        if (!$this->checkAcl('deleteObject')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $object = $this->getObjectById($id);
        if (null !== $object) {

            $fileDestination = realpath(APPLICATION_PATH . '/../www/images/object');
            @unlink($fileDestination . '/' . $object->thumbnail);
            @unlink($fileDestination . '/' . $object->preview);
            @unlink($fileDestination . '/' . $object->image);
            $object->delete();
            return true;
        }

        return false;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Object';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Guest', $this, array())
                    ->allow('Member', $this, array())
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
