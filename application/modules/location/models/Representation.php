<?php

class Location_Model_Representation extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    /**
     * Get a representation post by its id
     *
     * @param  string $ident The ident
     * @return Location_Resource_Representation_Item
     */
    public function getRepresentationById($id)
    {
        $id = (int) $id;
        return $this->getResource('Representation')->getRepresentationById($id);
    }

    /**
     * Get representation
     *
     * @param int|boolean   $paged    Whether to page results
     * @param integer|null  $limit    Order results
     * @param integer       $per_page    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getRepresentations($status = null)
    {
        return $this->getResource('Representation')->getRepresentations($status);
    }

    /**
     * Save a representation post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveRepresentation($data, $validator = null)
    {
        if (!$this->checkAcl('saveRepresentation')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/representation');

        if (null === $validator) {
            $validator = 'add';
        }

        $validator = $this->getForm('Representation' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        // get post data
        $data = $validator->getValues();

        $representation = array_key_exists('representation_id', $data) ?
                $this->getResource('Representation')->getRepresentationById($data['representation_id']) : null;


//        print_r($representation);
//        print_r($data);
//        exit;

        if ($representation AND $data['delete_image'] == 0 AND $data['image'] == '') {
            $data['image'] = $representation->image;
            $data['thumbnail'] = $representation->thumbnail;
            $data['preview'] = $representation->preview;
        } elseif ($data['delete_image'] == 1) {
            unlink($fileDestination . '/' . $data['image']);
            unlink($fileDestination . '/' . $data['thumbnail']);
            unlink($fileDestination . '/' . $data['preview']);
        }


        $new = $this->getResource('Representation')->saveRow($data, $representation);

        $data['representation_id'] = $new;

        if (!$data['image']) {
            return $new;
        } else {


            $filter = new ARTCMF_Filter_ImageSize();


            $path_parts = pathinfo($data['image']);
            $new_file_name = 'representation-' . $new . '.' . $path_parts['extension'];
            rename($fileDestination . '/' . $data['image'], $fileDestination . '/' . $new_file_name);
            $data['image'] = $new_file_name;

            $settingModel = new Location_Model_Settings();
            $image_resize_setting = $settingModel->getImageResizeSettingForModel('representation');


            $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                    ->setHeight($image_resize_setting['height_thumbnail'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                    ->filter($fileDestination . '/' . $new_file_name);

            $preview = $filter->setWidth($image_resize_setting['width_preview'])
                    ->setHeight($image_resize_setting['height_preview'])
                    ->setQuality(100)
                    ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                    ->setThumnailDirectory($fileDestination)
                    ->setStrategy(new $image_resize_setting['strategy_preview']())
                    ->filter($fileDestination . '/' . $new_file_name);


            $data['thumbnail'] = basename($thumbnail);
            $data['preview'] = basename($preview);


            $new_representation = $this->getResource('Representation')->getRepresentationById($new);

            return $this->getResource('Representation')->saveRow($data, $new_representation);
        }
    }

    public function deleteRepresentation($id)
    {
        if (!$this->checkAcl('deleteRepresentation')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $representation = $this->getRepresentationById($id);
        if (null !== $representation) {
            
            $fileDestination = realpath(APPLICATION_PATH . '/../www/images/representation');
            @unlink($fileDestination . '/' . $representation->thumbnail);
            @unlink($fileDestination . '/' . $representation->preview);
            @unlink($fileDestination . '/' . $representation->image);
            $representation->delete();
            return true;
        }

        return false;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Representation';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Guest', $this, array())
                    ->allow('Member', $this, array())
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
