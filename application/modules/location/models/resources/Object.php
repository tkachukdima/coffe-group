<?php

/**
 * Location_Resource_Object
 *
 * @category   Location
 * @package    Location_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Location_Resource_Object extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'location_object';
    protected $_primary = 'object_id';
    protected $_rowClass = 'Location_Resource_Object_Item';

    public function getObjectById($id)
    {
        return $this->find($id)->current();
    }

       /**
     * Get a list of objects
     *
     * @param  boolean   $paged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getObjects($status = null)
    {
        $select = $this->select();        
        $select->order(array('sort_order ASC'));
        
        if(!is_null($status)) {
            $select->where('status = ?', (int) $status);
        }
                
        return $this->fetchAll($select);
    }

}