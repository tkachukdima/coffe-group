<?php

/**
 * Location_Resource_Representation
 *
 * @category   Location
 * @package    Location_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Location_Resource_Representation extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'representation';
    protected $_primary = 'representation_id';
    protected $_rowClass = 'Location_Resource_Representation_Item';

    public function getRepresentationById($id)
    {
        return $this->find($id)->current();
    }

       /**
     * Get a list of representation
     *
     * @param  boolean   $paged      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getRepresentations($status = null)
    {
        $select = $this->select();        
        $select->order(array('city ASC','sort_order ASC'));
        
        if(!is_null($status)) {
            $select->where('status = ?', (int) $status);
        }
                
        return $this->fetchAll($select);
    }

}