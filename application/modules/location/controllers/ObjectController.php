<?php

/*
 * ObjectController
 *
 * @category   Location
 * @package    Location_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Location_ObjectController extends Zend_Controller_Action
{

    protected $_forms = array();
    protected $_modelObject;

    public function init()
    {
        $this->_modelObject = new Location_Model_Object();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {

        $pageModel = new Default_Model_Page();

        if (null === $this->view->page = $pageModel->getPageByType('object')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not created')));
        }
        if ($this->view->page) {
            $this->view->headTitle($this->view->page->page_title, 'PREPEND');
            $this->view->headMeta()->appendName('description', $this->view->page->meta_description);
            $this->view->headMeta()->appendName('keywords', $this->view->page->meta_keywords);
        }

        $this->view->objects = $this->_modelObject->getObjects($status = 1);
     
    }
    
    public function viewAction()
    {
        $request = $this->getRequest();
        
        if ($request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
        
        $object_id = $request->getParam('object_id', null);

        if (is_null($object_id)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Object not found')));
        }          
        
        $object = $this->_modelObject->getObjectById((int)$object_id);
        
        if (is_null($object)) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Object not found')));
        }    
        
        if (0 == $object->status) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Object not found')));
        } 

        $this->view->headTitle($object->name, 'PREPEND');
        $this->view->headMeta()->appendName('description', $object->name);
        $this->view->headMeta()->appendName('keywords', $object->name);
                
        $this->view->object = $object;
        
    }

    public function listAction()
    {

        $this->view->objects = $this->_modelObject->getObjects();
    }

    public function addAction()
    {

        $this->view->objectForm = $this->_getObjectForm('add');
    }

    public function editAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Representative not found')) . ' ' . $this->_getParam('id'));
        }

        $object_id = (int) $this->_getParam('id');

        $this->view->object = $this->_modelObject->getObjectById($object_id);

        $this->view->objectForm = $this->_getObjectForm('edit')->populate($this->view->object->toArray());
    }

    public function saveAction()
    {


        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }


        if (false === $this->_modelObject->saveObject($request->getPost(), $type)) {
            $this->view->objectForm = $this->_getObjectForm($type);
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Representative added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Representative updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'object',
                    'module' => 'location'), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Representative not found')) . ' ' . $id);
        }

        $this->_modelObject->deleteObject($id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'object',
                    'module' => 'location'), 'admin', true);
    }

    protected function _getObjectForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type . 'Object'] = $this->_modelObject->getForm('Object' . ucfirst($type));
        $this->_forms[$type . 'Object']->setAction($urlHelper->url(array(
                    'module' => 'location',
                    'controller' => 'object',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms[$type . 'Object']->setMethod('post');

        return $this->_forms[$type . 'Object'];
    }

}