<?php

/*
 * RepresentationController
 *
 * @category   Location
 * @package    Location_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Location_RepresentationController extends Zend_Controller_Action
{

    protected $_forms = array();
    protected $_modelRepresentation;

    public function init()
    {
        $this->_modelRepresentation = new Location_Model_Representation();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function indexAction()
    {

        $gdata_options = $this->getFrontController()->getParam('bootstrap')->getApplication()->getOption('gdata');
        $this->view->googleMapsKey = $gdata_options['googleMapsKey'];

        $pageModel = new Default_Model_Page();

        if (null === $this->view->page = $pageModel->getPageByType('representation')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Page not created')));
        }
        if ($this->view->page) {
            $this->view->headTitle($this->view->page->page_title, 'PREPEND');
            $this->view->headMeta()->appendName('description', $this->view->page->meta_description);
            $this->view->headMeta()->appendName('keywords', $this->view->page->meta_keywords);
        }

        $this->view->representations = $this->_modelRepresentation->getRepresentations($status = 1);

        $representations_city = array();
        foreach ($this->view->representations as $representation) {
            $representations_city[$representation->city][] = $representation;
        }
        $this->view->representations_city = $representations_city;
    }

    public function listAction()
    {

        $this->view->representations = $this->_modelRepresentation->getRepresentations();
    }

    public function addAction()
    {

        $this->view->representationForm = $this->_getRepresentationForm('add');
    }

    public function editAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Representative not found')) . ' ' . $this->_getParam('id'));
        }

        $representation_id = (int) $this->_getParam('id');

        $this->view->representation = $this->_modelRepresentation->getRepresentationById($representation_id);

        $this->view->representationForm = $this->_getRepresentationForm('edit')->populate($this->view->representation->toArray());
    }

    public function saveAction()
    {


        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }


        if (false === $this->_modelRepresentation->saveRepresentation($request->getPost(), $type)) {
            $this->view->representationForm = $this->_getRepresentationForm($type);
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Representative added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Representative updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'representation',
                    'module' => 'location'), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Representative not found')) . ' ' . $id);
        }

        $this->_modelRepresentation->deleteRepresentation($id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
                    'action' => 'list',
                    'controller' => 'representation',
                    'module' => 'location'), 'admin', true);
    }

    protected function _getRepresentationForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type . 'Representation'] = $this->_modelRepresentation->getForm('Representation' . ucfirst($type));
        $this->_forms[$type . 'Representation']->setAction($urlHelper->url(array(
                    'module' => 'location',
                    'controller' => 'representation',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms[$type . 'Representation']->setMethod('post');

        return $this->_forms[$type . 'Representation'];
    }

}