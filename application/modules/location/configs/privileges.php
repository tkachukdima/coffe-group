<?php

return array(
    'Guest' => array(
        'allow' => array(
            'object' => array(
                'index',
                'view'
            ),
            'representation' => array(
                'index',
            )
        )
    ),
    'Member' => array(
        'allow' => array(
        )
    ),
    'Redactor' => array(
        /*'allow' => array(
            'object' => array(
                'list',
                'add',
                'edit',
                'save',
                'delete'
            ),
            'representation' => array(
                'list',
                'add',
                'edit',
                'save',
                'delete'
            ),
        )*/
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);