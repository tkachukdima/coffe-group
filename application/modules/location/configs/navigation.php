<?php

return array(
    array(
        'label' => _('Objects'),
        'module' => 'location',
        'controller' => 'object',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'location:object',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-object',
        'pages' => array(
            array(
                'label' => _('Objects'),
                'module' => 'location',
                'controller' => 'object',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'location:object',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit object'),
                        'module' => 'location',
                        'controller' => 'object',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'location:object',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit object'),
                        'module' => 'location',
                        'controller' => 'object',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'location:object',
                        'privilege' => 'save',
                        'reset_params' => true
                    ),
                )
            ),
            array(
                'label' => _('Add object'),
                'module' => 'location',
                'controller' => 'object',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'location:object',
                'privilege' => 'add',
                'reset_params' => true
            ),
        )
    ),
    array(
        'label' => _('Representatives'),
        'module' => 'location',
        'controller' => 'representation',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'location:representation',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-representation',
        'pages' => array(
            array(
                'label' => _('Representatives'),
                'module' => 'location',
                'controller' => 'representation',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'location:representation',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit representative'),
                        'module' => 'location',
                        'controller' => 'representation',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'location:representation',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit representative'),
                        'module' => 'location',
                        'controller' => 'representation',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'location:representation',
                        'privilege' => 'save',
                        'reset_params' => true
                    ),
                )
            ),
            array(
                'label' => _('Add representative'),
                'module' => 'location',
                'controller' => 'representation',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'location:representation',
                'privilege' => 'add',
                'reset_params' => true
            ),
        )
    )
);