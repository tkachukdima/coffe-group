<?php

/**
 * Base representation Form
 *
 * @category   Location
 * @package    Location_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Location_Form_Representation_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Location_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Location_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/representation');

        $this->setMethod('post');
        $this->setAction('');

        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'city', array(
            'label' => 'Город',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'address', array(
            'label' => 'Адрес',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'phone', array(
            'label' => 'Телефон',
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('text', 'email', array(
            'label' => 'Email',
            'filters' => array('StringTrim'),
            'required' => false,
        ));
         

        $this->addElement('text', 'url', array(
            'label' => 'Сайт',
            'filters' => array('StringTrim'),
            'required' => false,
        ));
        $this->addElement('text', 'work_time', array(
            'label' => 'Время работы',
            'filters' => array('StringTrim'),
            'required' => false,
        ));

        $this->addElement('file', 'image', array(
            'label' => 'Фотография',
            'destination' => $fileDestination,           
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg,jpeg,png,gif')),
            ),
        ));
        
        $this->addElement('checkbox', 'delete_image', array(
            'label' => 'Удалить фото',
            'decorators' => array('ViewHelper', 'Label'),
            'required' => false,
        ));

        $this->addElement('text', 'longitude', array(
            'label' => 'Долгота',
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        $this->addElement('text', 'latitude', array(
            'label' => 'Широта',
            'filters' => array('StringTrim'),
            'required' => true,
        ));


        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        
        $this->addElement('select', 'status', array(
            'label' => 'Статус: ',
            'multiOptions' => array(
                '1' => 'Активный',
                '0' => 'Неактивный'
            )
        ));

        $this->addElement('submit', 'submit', array(
            
        ));

        $this->addElement('hidden', 'representation_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
    }

}
