<?php

/**
 * Base object Form
 *
 * @category   Location
 * @package    Location_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Location_Form_Object_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {
        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Location_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Location_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/object');

        $this->setMethod('post');
        $this->setAction('');

        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));

        $this->addElement('textarea', 'announce', array(
            'label' => 'Аннонс',
            'filters' => array('StringTrim'),
            'required' => true,
            'cols' => 40,
             'rows' => 2,
        ));

        $this->addElement('textarea', 'description', array(
            'label' => 'Описание',
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => "ckeditor"
        ));
        
        //$this->description->addDecorator(new ARTCMF_Form_Decorator_CKEditor);
       
        /*$this->addElement('file', 'image', array(
            'label' => 'Изображение',
            'destination' => $fileDestination,           
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576 * 5)),
                array('Extension', false, array('jpg','jpeg','png','gif')),
            ),
        ));
        
        $this->addElement('checkbox', 'delete_image', array(
            'label' => 'Удалить фото',
            'decorators' => array('ViewHelper', 'Label'),
            'required' => false,
        ));*/

        $this->addElement('text', 'longitude', array(
            'label' => 'Долгота',
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        $this->addElement('text', 'latitude', array(
            'label' => 'Широта',
            'filters' => array('StringTrim'),
            'required' => true,
        ));


        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));
        
        $this->addElement('select', 'status', array(
            'label' => 'Статус: ',
            'multiOptions' => array(
                '1' => 'Активный',
                '0' => 'Неактивный'
            )
        ));

        $this->addElement('submit', 'submit', array(
            
        ));

        $this->addElement('hidden', 'object_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
    }

}
