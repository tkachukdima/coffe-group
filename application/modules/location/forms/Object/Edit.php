<?php

/**
 * Edit object
 *
 * @category   Location
 * @package    Location_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Location_Form_Object_Edit extends Location_Form_Object_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
