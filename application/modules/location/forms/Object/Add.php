<?php

/**
 * Add new object post
 *
 * @category   Location
 * @package    Location_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Location_Form_Object_Add extends Location_Form_Object_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->removeElement('object_id');
        $this->removeElement('delete_image');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
