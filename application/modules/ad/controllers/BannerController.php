<?php

/*
 * Ad_BannerController
 * 
 * @category   Default
 * @package    Ad_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */

class Ad_BannerController extends Zend_Controller_Action
{

    /**
     * @var Ad_Model_Banner
     */
    protected $_modelBanner;
    /**
     * @var array
     */
    protected $_forms = array();

    public function init()
    {
        $this->_modelBanner = new Ad_Model_Banner();
        $this->view->currentLang = Zend_Registry::get('Current_Lang');
    }

    public function listAction()
    {

        $this->view->banners = $this->_modelBanner->getBanners();
    }

    public function listTypesAction()
    {

        $this->view->bannerTypes = $this->_modelBanner->getBannerTypes($onlyActive = false);
    }

    public function sortAction()
    {

        $redirector = new Zend_Controller_Action_Helper_Redirector();

        if ($this->_getParam('id') != '' AND $this->_getParam('move') != '') {
            $id = (int) $this->_getParam('id');
            $move = $this->_getParam('move');

            $this->_modelBanner->moveBannerItem($id, $move);
        }

        if ($this->getRequest()->isPost()) {
            if (is_array($this->getRequest()->getPost('sort_order')) AND is_array($this->getRequest()->getPost('id'))) {

                $id = $this->getRequest()->getPost('id');
                $sort_order = $this->getRequest()->getPost('sort_order');

                $this->_modelBanner->setNewSortOrder($id, $sort_order);
            }
        }

        $redirector->gotoRoute(array(
            'module' => 'ad',
            'controller' => 'banner',
            'action' => 'list'), 'admin', true
        );
    }

    public function sortTypeAction()
    {

        $redirector = new Zend_Controller_Action_Helper_Redirector();

        if ($this->_getParam('id') != '' AND $this->_getParam('move') != '') {
            $id = (int) $this->_getParam('id');
            $move = $this->_getParam('move');

            $this->_modelBanner->moveBannerTypeItem($id, $move);
        }

        if ($this->getRequest()->isPost()) {
            if (is_array($this->getRequest()->getPost('sort_order')) AND is_array($this->getRequest()->getPost('id'))) {

                $id = $this->getRequest()->getPost('id');
                $sort_order = $this->getRequest()->getPost('sort_order');

                $this->_modelBanner->setNewSortOrderBannerType($id, $sort_order);
            }
        }

        $redirector->gotoRoute(array(
            'module' => 'ad',
            'controller' => 'banner',
            'action' => 'list-types'), 'admin', true
        );
    }

    public function addAction()
    {

        $this->view->bannerForm = $this->_getForm('add');
    }

    public function addTypeAction()
    {

        $this->view->bannerTypeForm = $this->_getTypeForm('add');
    }

    public function editAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Banner not found')) . $this->_getParam('id'));
        }

        $banner_id = (int) $this->_getParam('id');

        $banner = $this->_modelBanner->getBannerByIdForEdit($banner_id);

        $this->view->bannerForm = $this->_getForm('edit')->populate($banner);
    }

    public function editTypeAction()
    {

        if (!$this->_getParam('id')) {
            throw new ARTCMF_Exception_404($this->view->translate(_('Banner type can not be found')) . $this->_getParam('id'));
        }

        $banner_type_id = (int) $this->_getParam('id');

        $bannerType = $this->_modelBanner->getBannerTypeById($banner_type_id);

        $this->view->bannerTypeForm = $this->_getTypeForm('edit')->populate($bannerType->toArray());
    }

    public function saveAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelBanner->saveBanner($request->getPost(), $type)) {
            $this->view->bannerForm = $this->_getForm($type);
            $this->view->bannerForm->setDescription($this->view->translate('Error! Completed form is incorrect.'));
            return $this->render($type);
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Banner added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Banner updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'list',
            'controller' => 'banner',
            'module' => 'ad'), 'admin', true);
    }

    public function saveTypeAction()
    {

        $request = $this->getRequest();

        $type = $request->getParam('type');

        if (!$request->isPost()) {
            return $this->_helper->redirector($type);
        }

        if (false === $this->_modelBanner->saveBannerType($request->getPost(), $type)) {
            $this->view->bannerTypeForm = $this->_getTypeForm($type);
            $this->view->bannerTypeForm->setDescription($this->view->translate('Error! Completed form is incorrect.'));
            return $this->render($type . '-type');
        }

        switch ($type) {
            case 'add':
                $message = $this->view->translate(_('Banner type is added'));
                break;

            case 'edit':
                $message = $this->view->translate(_('Banner type has been updated'));
                break;

            default:
                break;
        }
        $this->_helper->FlashMessenger->setNamespace('success')->addMessage($message);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'list-types',
            'controller' => 'banner',
            'module' => 'ad'), 'admin', true);
    }

    public function deleteAction()
    {
        if (false === ($banner_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Banner not found')) . $banner_id);
        }

        $this->_modelBanner->deleteBanner($banner_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'list',
            'controller' => 'banner',
            'module' => 'ad'), 'admin', true);
    }

    public function deleteTypeAction()
    {
        if (false === ($banner_type_id = $this->_getParam('id', false))) {
            throw new ARTCMF_Exception($this->view->translate(_('Banner type can not be found')) . $banner_type_id);
        }

        $this->_modelBanner->deleteBannerType($banner_type_id);

        $redirector = $this->getHelper('redirector');
        return $redirector->gotoRoute(array(
            'action' => 'list-types',
            'controller' => 'banner',
            'module' => 'ad'), 'admin', true);
    }

    protected function _getForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms[$type] = $this->_modelBanner->getForm('Banner' . ucfirst($type));
        $this->_forms[$type]->setAction($urlHelper->url(array(
                    'module' => 'ad',
                    'controller' => 'banner',
                    'action' => 'save',
                    'type' => $type), 'admin'));
        $this->_forms[$type]->setMethod('post');

        return $this->_forms[$type];
    }

    protected function _getTypeForm($type = 'add')
    {
        $urlHelper = $this->_helper->getHelper('url');

        $this->_forms['BannerType' . $type] = $this->_modelBanner->getForm('Bannertype' . ucfirst($type));
        $this->_forms['BannerType' . $type]->setAction($urlHelper->url(array(
                    'module' => 'ad',
                    'controller' => 'banner',
                    'action' => 'save-type',
                    'type' => $type), 'admin'));
        $this->_forms['BannerType' . $type]->setMethod('post');

        return $this->_forms['BannerType' . $type];
    }

}