<?php

class Ad_Model_Banner extends ARTCMF_Model_Acl_Abstract {

    /**
     * Get a banner post by its id
     *
     * @param  string $ident The ident
     * @return Ad_Resource_Banner_Item
     */
    public function getBannerById($id) {
        $id = (int) $id;
        return $this->getResource('Banner')->getBannerById($id);
    }

    /**
     * Get a Banner by its id for edit
     *
     * @param  int $id The id
     * @return Ad_Resource_Banner_Item
     */
    public function getBannerByIdForEdit($id) {
        $id = (int) $id;

        $page = $this->getResource('Banner')->getBannerByIdForEdit($id)->toArray();

        $data = $page[0];

        foreach ($page as $key => $value) {
            $data['title_' . $value['language_code']] = $value['title'];
            $data['url_' . $value['language_code']] = $value['url'];
            $data['description_' . $value['language_code']] = $value['description'];
        }

        return $data;
    }

    /**
     * Get banners
     *
     * @return Zend_Db_Table_Rowset|null
     */
    public function getBanners($typeIdent = null, $status = null) {
        return $this->getResource('Banner')->getBanners($typeIdent, $status);
    }
    
  
    /**
     * Save a banner post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveBanner($data, $validator = null) {
        if (!$this->checkAcl('saveBanner')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }


        $validator = $this->getForm('Banner' . ucfirst($validator));

        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();

        $banner = array_key_exists('banner_id', $data) ?
                $this->getResource('Banner')->getBannerById($data['banner_id']) : null;

        if ($banner->full != '' AND $data['full'] == '') {
            $data['full'] = $banner->full;
            $data['thumbnail'] = $banner->thumbnail;           
        }


        $new = $this->getResource('Banner')->saveRow($data, $banner);

        foreach (Zend_Registry::get('langList') as $key => $lang) {

            $translate_data = array(
                'banner_id' => (int) $new,
                'language_code' => $lang->code,
                'title' => $data['title_' . $lang->code],
                'url' => $data['url_' . $lang->code],
                'description' => $data['description_' . $lang->code]
            );
            if (null == $banner)
                $method = 'insert';
            else
                $method = 'update';

            $this->getResource('Banner')->saveTranslatedRows($translate_data, $method);
        }

        $data['banner_id'] = $new;

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/banner');

        $filter = new ARTCMF_Filter_ImageSize();


        $path_parts = pathinfo($data['full']);
        $new_file_name = 'banner-image-' . $new . '.' . $path_parts['extension'];
        rename($fileDestination . '/' . $data['full'], $fileDestination . '/' . $new_file_name);


        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingForModel('banner');


        $thumbnail = $filter->setWidth($image_resize_setting['width_thumbnail'])
                ->setHeight($image_resize_setting['height_thumbnail'])
                ->setQuality(100)
                ->setOverwriteMode(ARTCMF_Filter_ImageSize::OVERWRITE_CACHE_OLDER)
                ->setThumnailDirectory($fileDestination)
                ->setStrategy(new $image_resize_setting['strategy_thumbnail']())
                ->filter($fileDestination . '/' . $new_file_name);


        $data['thumbnail'] = basename($thumbnail);        
        $data['full'] = basename($fileDestination . '/' . $new_file_name);


        $new_banner = $this->getResource('Banner')->getBannerById($new);

        return $this->getResource('Banner')->saveRow($data, $new_banner);
    }

    public function deleteBanner($id) {
        if (!$this->checkAcl('deleteBanner')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $banner = $this->getBannerById($id);

        if (null !== $banner) {

            $fileDestination = realpath(APPLICATION_PATH . '/../www/images/banner');
            unlink($fileDestination . '/' . $banner->thumbnail);
            unlink($fileDestination . '/' . $banner->full);
            $this->getResource('Banner')->deleteTranslatedRows($id);
            $banner->delete();
            return true;
        }

        return false;
    }

    public function moveBannerItem($id, $move) {
        $banner_item = $this->getBannerById($id);

        $near_sort = $this->getResource('Banner')->getNearSort($banner_item->sort_order, $move);

        if (count($near_sort) != 0) {

            $old_sort_order = $banner_item->sort_order;
            $banner_item->sort_order = $near_sort[0]->sort_order;
            $banner_item->save();


            $banner_item_near = $this->getBannerById($near_sort[0]->banner_id);

            $banner_item_near->sort_order = $old_sort_order;
            $banner_item_near->save();
        }
        return $banner_item;
    }

    public function setNewSortOrder($id, $sort_order) {
        foreach ($id as $key => $id) {
            $banner_item = $this->getBannerById($id);
            $banner_item->sort_order = (int) $sort_order[$key];
            $banner_item->save();
        }
    }
    
    
     /**
     * Get a banner type by its id
     *
     * @param  string $ide The id
     * @return Ad_Resource_Bannertype_Item
     */
    public function getBannerTypeById($id) {
        $id = (int) $id;
        return $this->getResource('Bannertype')->getBannerTypeById($id);
    }
    
     /**
     * Get a banner post by its ident
     *
     * @param  string $ide The id
     * @return Ad_Resource_Bannertype_Item
     */
    public function getBannerTypeByIdent($ident, $ignoreRow=null) {
        return $this->getResource('Bannertype')->getBannerTypeByIdent($ident, $ignoreRow);
    }
    
     /**
     * Get banner
     *
     * @param int|boolean   $bannerd    Whether to banner results
     * @param integer|null  $limit    Order results
     * @param integer       $per_banner    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getBannerTypes($onlyActive = true) {
        return $this->getResource('Bannertype')->getBannerTypes($onlyActive);
    }
    
    /**
     * Save a banner post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveBannerType($data, $validator = null) {
        if (!$this->checkAcl('saveBannerType')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }
        
        $validator = $this->getForm('Bannertype' . ucfirst($validator));
             
        if (!$validator->isValid($data)) {
            return false;
        }

        $data = $validator->getValues();
        
        $bannerType = array_key_exists('banner_type_id', $data) ?
                $this->getResource('Bannertype')->getBannerTypeById($data['banner_type_id']) : null;
        
        if(is_null($bannerType)) {
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_who'] = $this->getIdentity()->user_id;
        } else  {
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_who'] = $this->getIdentity()->user_id;
        }
                      
        return $this->getResource('Bannertype')->saveRow($data, $bannerType);
    }

    public function deleteBannerType($id) {
        if (!$this->checkAcl('deleteBannerType')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $bannerType = $this->getBannerTypeById($id);
        if (is_null($bannerType)) 
            return false;
                
        $banners = $this->getBanners($bannerType->ident);
        if(count($banners))
            return false;
        
        $bannerType->delete();
        return true;
    }

    public function moveBannerTypeItem($id, $move) {
        $banner_type_item = $this->getBannerTypeById($id);

        $near_sort = $this->getResource('Bannertype')->getNearSort($banner_type_item->sort_order, $move);

        if (count($near_sort) != 0) {

            $old_sort_order = $banner_type_item->sort_order;
            $banner_type_item->sort_order = $near_sort[0]->sort_order;
            $banner_type_item->save();


            $banner_item_near = $this->getBannerTypeById($near_sort[0]->banner_id);

            $banner_item_near->sort_order = $old_sort_order;
            $banner_item_near->save();
        }
        return $banner_type_item;
    }

    public function setNewSortOrderBannerType($id, $sort_order) {
        foreach ($id as $key => $id) {
            $banner_type_item = $this->getBannerTypeById($id);
            $banner_type_item->sort_order = (int) $sort_order[$key];
            $banner_type_item->save();
        }

        return $banner_type_item;
    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId() {
        return 'Banner';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl) {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl() {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
