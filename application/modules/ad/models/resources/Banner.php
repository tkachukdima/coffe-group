<?php

/**
 * Ad_Resource_Banner
 *
 * @category   Default
 * @package    Ad_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Resource_Banner extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'banner';
    protected $_primary = 'banner_id';
    protected $_rowClass = 'Ad_Resource_Banner_Item';
    protected $_lang;

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_lang = Zend_Registry::get('Current_Lang');
    }

    public function getBannerById($id)
    {
        return $this->find($id)->current();
    }
    
    public function getBannerByIdForEdit($id)
    {
        $select = $this->select();
        $select->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.banner_id = {$this->_name}_translation.banner_id")
                
                ->where($this->_name . '.banner_id = ?', $id);

        return $this->fetchAll($select);
    }

    /**
     * Get a list of banner
     *
     * @param  boolean   $bannerd      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getBanners($typeIdent = null, $status = null)
    {
        $select = $this->select()
                ->from($this->_name)
                ->setIntegrityCheck(false)
                ->joinLeft($this->_name . '_translation', "{$this->_name}.banner_id = {$this->_name}_translation.banner_id")
                
                ->joinLeft('banner_type', 
                        "banner.banner_type_id = banner_type.banner_type_id", 
                        array(
                            'banner_type_ident' => 'ident',
                            'banner_type_name' => 'name'
                            ))
                ->where($this->_name . '_translation.language_code = ?', $this->_lang)
                ->order($this->_name . '.sort_order');

        if (!is_null($typeIdent))
            $select->where('banner_type.ident = ?', $typeIdent);
        
        if (!is_null($status))
            $select->where('banner.status = ?', (int) $status);
        
        //echo $select->__toString();die;

        return $this->fetchAll($select);
    }

    public function getNearSort($sort_order, $move)
    {

        $select = $this->select();

        switch ($move) {
            case 'up':
                $select->where('sort_order < ?', $sort_order)
                        ->order('sort_order DESC')
                        ->limit(1, 0);
                break;

            case 'down':
                $select->where('sort_order > ?', $sort_order)
                        ->order('sort_order ASC')
                        ->limit(1, 0);
                break;

            default:
                break;
        }


        return $this->fetchAll($select);
    }
    
    public function saveTranslatedRows($translate_data, $method = 'insert')
    {
        $translate_data['table'] = $this->_name . '_translation';

        switch ($method) {
            case 'insert':
                $this->getAdapter()->insert($translate_data['table'], array(
                    'banner_id' => $translate_data['banner_id'],
                    'language_code' => $translate_data['language_code'],
                    'title' => $translate_data['title'],
                    'url' => $translate_data['url'],
                    'description' => $translate_data['description']                    
                ));

                break;

            case 'update':
                $this->getAdapter()->update($translate_data['table'], array(
                    'title' => $translate_data['title'],
                    'url' => $translate_data['url'],
                    'description' => $translate_data['description']
                        ), "`banner_id` = {$translate_data['banner_id']}
                      AND `language_code` = '{$translate_data['language_code']}'");

                break;
            default:
                break;
        }
    }

    public function deleteTranslatedRows($id)
    {
        $this->getAdapter()->delete("{$this->_name}_translation", "banner_id = {$id}");
    }
    

}