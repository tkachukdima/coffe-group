<?php

/**
 * Ad_Resource_Bannertype
 *
 * @category   Default
 * @package    Ad_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Resource_Bannertype extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'banner_type';
    protected $_primary = 'banner_type_id';
    protected $_rowClass = 'Ad_Resource_Banner_Item';
   
    public function getBannerTypeById($id)
    {        
        return $this->find($id)->current();
    }

    public function getBannerTypeByIdent($ident, $ignoreRow=null)
    { 
       $select = $this->select()
                  ->where('ident = ?', $ident);
       
        if (null !== $ignoreRow) {
            $select->where('ident <> ?', $ignoreRow->ident);
        }
                       
        return $this->fetchRow($select);
    }
    
    /**
     * Get a list of  banner types
     *
     * @param  boolean   $bannerd      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getBannerTypes($onlyActive = true)
    { 
       $select = $this->select()
                  ->order('sort_order');
       
       if($onlyActive)
           $select->where('status = ?', 1);
                
        return $this->fetchAll($select);
    }

    public function getNearSort($sort_order, $move)
    {

        $select = $this->select();

        switch ($move) {
            case 'up':
                $select->where('sort_order < ?', $sort_order)
                        ->order('sort_order DESC')
                        ->limit(1, 0);
                break;

            case 'down':
                $select->where('sort_order > ?', $sort_order)
                        ->order('sort_order ASC')
                        ->limit(1, 0);
                break;

            default:
                break;
        }


        return $this->fetchAll($select);
    }
    
    

}