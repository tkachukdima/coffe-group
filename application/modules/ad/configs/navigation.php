<?php

return array(array(
        'label' => _('Banners'),
        'module' => 'ad',
        'controller' => 'banner',
        'action' => 'list',
        'route' => 'admin',
        'resource' => 'ad:banner',
        'privilege' => 'list',
        'reset_params' => true,
        'class' => 'dashboard-banner',
        'pages' => array(
            array(
                'label' => _('Banners'),
                'module' => 'ad',
                'controller' => 'banner',
                'action' => 'list',
                'route' => 'admin',
                'resource' => 'ad:banner',
                'privilege' => 'list',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit banner'),
                        'module' => 'ad',
                        'controller' => 'banner',
                        'action' => 'edit',
                        'route' => 'admin',
                        'resource' => 'ad:banner',
                        'privilege' => 'edit',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit banner'),
                        'module' => 'ad',
                        'controller' => 'banner',
                        'action' => 'save',
                        'route' => 'admin',
                        'resource' => 'ad:banner',
                        'privilege' => 'save',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label' => _('Add banner'),
                'module' => 'ad',
                'controller' => 'banner',
                'action' => 'add',
                'route' => 'admin',
                'resource' => 'ad:banner',
                'privilege' => 'add',
                'reset_params' => true
            ),
            array(
                'label' => _('Types of banners'),
                'module' => 'ad',
                'controller' => 'banner',
                'action' => 'list-types',
                'route' => 'admin',
                'resource' => 'ad:banner',
                'privilege' => 'list-types',
                'reset_params' => true,
                'pages' => array(
                    array(
                        'label' => _('Edit Type of banner'),
                        'module' => 'ad',
                        'controller' => 'banner',
                        'action' => 'edit-type',
                        'route' => 'admin',
                        'resource' => 'ad:banner',
                        'privilege' => 'edit-type',
                        'reset_params' => true
                    ),
                    array(
                        'label' => _('Edit Type of banner'),
                        'module' => 'ad',
                        'controller' => 'banner',
                        'action' => 'save-type',
                        'route' => 'admin',
                        'resource' => 'ad:banner',
                        'privilege' => 'save-type',
                        'reset_params' => true
                    )
                )
            ),
            array(
                'label' => _('Add Type of banner'),
                'module' => 'ad',
                'controller' => 'banner',
                'action' => 'add-type',
                'route' => 'admin',
                'resource' => 'ad:banner',
                'privilege' => 'add-type',
                'reset_params' => true
            ),
        )
    )
);