<?php

return array(
    'Redactor' => array(
        'allow' => array(
            'banner' => array(
                'list',
                'add',
                'edit',
                'save',
                'delete',
                'sort',
//                'list-types',
//                'add-type',
//                'edit-type',
//                'save-type',
//                'delete-type',
//                'sort-type'
            )
        )
    ),
    'Manager' => array(
    ),
    'Admin' => array(
    ),
);