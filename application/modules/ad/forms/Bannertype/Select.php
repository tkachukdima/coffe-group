<?php

/**
 * Bannertype Select
 *
 * @category   Ad
 * @package    Ad_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Form_Bannertype_Select extends ARTCMF_Form_Abstract {

    public function init() {
        $this->setMethod('post');

        $types = $this->getModel()->getBannerTypes();
        $typesOptions = array();
        $typesDescription = '';
        $typesOptions[0] = _('Not selected');
        foreach ($types as $type) {
            $typesOptions[$type->banner_type_id] = '[' . $type->ident . '] ' . $type->name;
            $typesDescription .=  '<p class="banner_type_description" style="display: none"; id="banner_type_'. $type->banner_type_id . '">' . $type->description . '</p>';
        }

        $this->addElement('select', 'banner_type_id', array(
            'label' => _('Select a type: '),
            'multiOptions' => $typesOptions,
            'description' => $typesDescription
        ));
        $this->banner_type_id->setDecorators(array(
        'ViewHelper',
            array('Description', array('escape' => false, 'tag' => false)),
            array('HtmlTag', array('tag' => 'dd')),
            array('Label', array('tag' => 'dt')),
            'Errors',
          ));


        $this->addElement('submit', 'View', array(
            'decorators' => array('ViewHelper'),
        ));
    }

}
