<?php

/**
 * Add new page post
 *
 * @category   Default
 * @package    Ad_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Form_Bannertype_Add extends Ad_Form_Bannertype_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->removeElement('banner_type_id');
        $this->getElement('submit')->setLabel(_('Add'));
    }

}
