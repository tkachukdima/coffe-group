<?php

/**
 * Edit Banner
 *
 * @category   Default
 * @package    Ad_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Form_Bannertype_Edit extends Ad_Form_Bannertype_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
