<?php

/**
 * Base Bannertype  Form
 *
 * @category   Default
 * @package    Ad_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Form_Bannertype_Base extends ARTCMF_Form_Abstract {

    public function init() {

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );
        
        $this->addElementPrefixPath('ARTCMF_Validate', 'ARTCMF/Validate/', 'validate');
        
        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );

        $this->setMethod('post');
        $this->setAction('');


        $this->addElement('text', 'name', array(
            'label' => _('Name'),
            'filters' => array('StringTrim'),
            'required' => true
        ));

        $this->addElement('text', 'ident', array(
            'label' => _('Unique identifier'),
            'filters' => array('StringTrim', new ARTCMF_Filter_Ident()),
            'validators' => array(
                array('UniqueIdent', true, array(
                        $this->getModel(),
                        'getBannerTypeByIdent',
                        'getBannerTypeById',
                        'banner_type_id'
                ))
            ),
            'required' => false,
        ));


        $this->addElement('textarea', 'description', array(
            'label' => _('Text'),
            'filters' => array('StringTrim'),
            'cols' => 40,
            'rows' => 4,
            'required' => false
        ));



        $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));


        $this->addElement('select', 'status', array(
            'label' => _('Status: '),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));


        $this->addElement('submit', 'submit', array());

        $this->addElement('hidden', 'banner_type_id', array(
            'filters' => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper', array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
    }

}
