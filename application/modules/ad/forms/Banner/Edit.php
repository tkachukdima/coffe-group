<?php

/**
 * Edit Banner
 *
 * @category   Default
 * @package    Ad_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Form_Banner_Edit extends Ad_Form_Banner_Base {

    public function init() {

        //call the parent init
        parent::init();

        //customize the form
        $this->getElement('full')->setRequired(false);
        $this->getElement('submit')->setLabel(_('Save'));
    }

}
