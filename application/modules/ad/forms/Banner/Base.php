<?php

/**
 * Base Banner Form
 *
 * @category   Default
 * @package    Ad_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Ad_Form_Banner_Base extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $fileDestination = realpath(APPLICATION_PATH . '/../www/images/banner');

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate',
                APPLICATION_PATH . '/modules/default/models/validate/',
                'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter',
                APPLICATION_PATH . '/modules/default/models/filter/',
                'filter'
        );

        $this->setMethod('post');
        $this->setAction('');
        
        foreach (Zend_Registry::get('langList') as $key => $lang) {
            
            $this->addElement('text', 'title_' . $lang->code, array(
                'label' => _('Name'),
                'filters' => array('StringTrim'),
                'required' => true,
                'description' => _('Title image'),
            ));
            
            $this->addElement('text', 'url_' . $lang->code, array(
                'label' => _('Link'),
                'filters' => array('StringTrim'),
                'required' => true,
                'description' => _('Example: "/news"'),
            ));


            $this->addElement('textarea', 'description_' . $lang->code, array(
                'label' => _('Text'),
                'filters' => array('StringTrim'),
                'cols' => 40,
                'rows' => 4,
                'required' => false
            ));
            
        }
        
        // get type select
        $form = new Ad_Form_Bannertype_Select(
            array('model' => $this->getModel())
        );
        $element = $form->getElement('banner_type_id');       
        $this->addElement($element);
        
        /**
         * Andrew Mae
         * Для более гибкой работы модуля, я отключаю ограничения при загрузке
         * изображения, а также ресайз оригинала в модели.
         * ЗЫ: баннеры должны готовиться заранее!!!
         * 01/12/11 01:14:11
         */
 
//        $settingModel = new Default_Model_Settings();
//        $image_resize_setting = $settingModel->getImageResizeSettingByIdent('banner');
//
//        $this->addElement('file','full', array(
//             'label' => _('Image'),
//            'required' => true,
//            'description' => 'Размер изображения должен быть ' . $image_resize_setting->width_full . 'x' . $image_resize_setting->height_full . '. Поддерживаемые форматы: jpg, jpeg, png, gif.',
//            'destination' => $fileDestination,
//            'validators' => array(
//                array('Count', false, array(1)),
//                array('Size', false, array(1048576*5)),
//                array('Extension', false, array('jpg', 'jpeg', 'png', 'gif')),
//                array('ImageSize', false, array('minwidth' => $image_resize_setting->width_full,
//                                                'maxwidth' => $image_resize_setting->width_full,
//                                                'minheight' => $image_resize_setting->height_full,
//                                                'maxheight' => $image_resize_setting->height_full))
//            ),
//        ));
        $settingModel = new Default_Model_Settings();
        $image_resize_setting = $settingModel->getImageResizeSettingByIdent('banner');
        
        $this->addElement('file','full', array(
            'label' => _('Image'),
            'required' => true,
            'description' => 'Рекомендуемый размер изображения должен быть ' . $image_resize_setting->width_full . 'x' . $image_resize_setting->height_full . '. Поддерживаемые форматы: jpg, jpeg, png, gif.',
            'destination' => $fileDestination,
            'validators' => array(
                array('Count', false, array(1)),
                array('Size', false, array(1048576*5)),
                array('Extension', false, array('jpg', 'jpeg', 'png', 'gif')),
                array('ImageSize', false, array('maxwidth' => 1600,
                                                'maxheight' => 1200))
            ),
        ));
                              

         $this->addElement('text', 'sort_order', array(
            'label' => _('Sorting'),
            'filters' => array('StringTrim'),
            'required' => true,
        ));
               
        
        $this->addElement('select', 'status', array(
            'label' => _('Status: '),
            'multiOptions' => array(
                '1' => _('Active'),
                '0' => _('Inactive')
            )
        ));

              
        $this->addElement('submit', 'submit', array());

         $this->addElement('hidden', 'banner_id', array(
            'filters'    => array('StringTrim'),
            'required' => true,
            'decorators' => array('viewHelper',array('HtmlTag', array('tag' => 'dd', 'class' => 'noDisplay')))
        ));
         
         
          foreach (Zend_Registry::get('langList') as $key => $lang) {
            $this->addDisplayGroup(array(
                'title_' . $lang->code,
                'url_' . $lang->code,
                'description_' . $lang->code                
                    ), 'form_' . $lang->code, array('legend' => $lang->name));
        }
              
        
        $this->addDisplayGroup(array('category_service_id', 'banner_type_id', 'full',  'sort_order', 'status', 'submit', 'banner_id'), 'form_all', array('legend' => _('General Settings')));
         
                                         
    }

}
