<?php

/**
 * AdminController
 * 
 * @category   Default
 * @package    Default_Controllers
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_AdminController extends Zend_Controller_Action {

    public function indexAction() {
        
    }

    public function templateListAction() {
//        $tplModel = new Email_Model_Templates();
//        $this->view->templates = $tplModel->getTemplates();
    }

    public function templateEditAction() {
//        $id = $this->getRequest()->getParam('id');
//        if (!empty($id)) {
//            $emailTemplateModel = new Email_Model_Templates();
//            $emailTemplateMapper = $emailTemplateModel->getTemplate($id);
//            $this->view->title = $this->view->translate(_('email_template')) . ' &laquo' . $emailTemplateMapper->subject . '&raquo;';
//        } else {
//            $emailTemplateMapper = new Email_Resource_Template_Item();
//            $this->view->title = $this->view->translate(_('new_email_template'));
//        }
//
//        $form = new Email_Form_TemplateEdit();
//        $form->populate($emailTemplateMapper->toArray());
//
//        if ($this->getRequest()->isPost()) {
//            if ($form->isValid($this->getRequest()->getPost())) {
//                $emailTemplateMapper = $form->getValues();
//                $emailTemplateMapper->save();
//
//                $this->_helper->getHelper('FlashMessenger')->addMessage('save_ok');
//                $this->_helper->redirector->goToRoute(array(
//                    'module' => 'email',
//                    'action' => 'template-list',
//                        ), 'admin', true);
//            }
//        }
//
//        $this->view->form = $form;
    }

}
