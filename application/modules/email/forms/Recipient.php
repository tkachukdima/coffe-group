<?php

/**
 * The Feedback form
 *
 * @category   Default
 * @package    Default_Form
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_Form_Recipient extends ARTCMF_Form_Abstract
{

    public function init()
    {

        $this->setMethod('post');

        // add path to custom validators & filters
        $this->addElementPrefixPath(
                'Default_Validate', APPLICATION_PATH . '/modules/default/models/validate/', 'validate'
        );

        $this->addElementPrefixPath(
                'Default_Filter', APPLICATION_PATH . '/modules/default/models/filter/', 'filter'
        );



        $this->addElement('text', 'name', array(
            'filters' => array('StringTrim'),
            'validators' => array(
                array('NotEmpty')
            ),
            'required' => true,
            'label' => _('Name'),
        ));


        $this->addElement('text', 'email', array(
            'filters' => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('NotEmpty'),
                array('EmailAddress')
            ),
            'required' => true,
            'label' => _('Email'),
        ));
    }

}
