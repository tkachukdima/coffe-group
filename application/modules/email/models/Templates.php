<?php

class Email_Model_Templates extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    protected $_site_settings;
    protected $_html;
    protected $_mail;
    
    
    public function __construct($options = null) {
        parent::__construct($options);
        
        $this->_mail = new Zend_Mail('UTF-8');
    }

    /**
     * Get a template by its id
     *
     * @param  string $ident The ident
     * @return Email_Resource_Template_Item
     */
    public function getTemplateById($id)
    {
        $id = (int) $id;
        return $this->getResource('Template')->getTemplateById($id);
    }
    
    /**
     * Get a template by its id
     *
     * @param  string $ident The ident
     * @return Email_Resource_Template_Item
     */
    public function getTemplateByIdent($ident)
    {
        return $this->getResource('Template')->getTemplateByIdent($ident);
    }


    /**
     * Get template
     *
     * @param int|boolean   $templated    Whether to template results
     * @param integer|null  $limit    Order results
     * @param integer       $per_template    Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator|null
     */
    public function getTemplates()
    {
        return $this->getResource('Template')->getTemplates();
    }

    /**
     * Save a template post
     *
     * @param array $data
     * @param string $validator
     * @return int|false
     */
    public function saveTemplate($data, $validator = null)
    {
        if (!$this->checkAcl('saveTemplate')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        if (null === $validator) {
            $validator = 'add';
        }
        

        $validator = $this->getForm('Template' . ucfirst($validator));

        if (!$validator->isValid($data)) {            
            return false;
        }

        $data = $validator->getValues();

        $template = array_key_exists('template_id', $data) ?
                $this->getResource('Template')->getTemplateById($data['template_id']) : null;
                       
        return $this->getResource('Template')->saveRow($data, $template);

    }

    public function deleteTemplate($id)
    {
        if (!$this->checkAcl('deleteTemplate')) {
            throw new ARTCMF_Acl_Exception("Insufficient rights");
        }

        $template = $this->getTemplateById($id);
        if (null !== $template) {           
            $template->delete();
            return true;
        }

        return false;
    }
    
        
     /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Template';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Member', $this, array('viewTemplates'))
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
