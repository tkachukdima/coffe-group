<?php

class Email_Model_Sender extends ARTCMF_Model_Acl_Abstract implements Zend_Acl_Resource_Interface
{

    private $_view = null;

    /**
     * sendEmail
     *
     * @param mixed $params
     * @param mixed $tplVars
     * @param string $tpl
     * @return void
     */
    public function sendEmail($params, $tplVars, $tpl)
    {
        $html = $this->getView();
        $html->assign($tplVars);
        $bodyHtml = $html->render($tpl . '.phtml');


        $params['subject'] = $html->translate($params['subject']);
        $params['subject'] = str_replace("%hostname%", $tplVars['site'], $params['subject']);


        if ('' != $params['email_to']) {
            $mail = new Zend_Mail('UTF-8');
            $mail->setFrom($params['email_from'], $params['site_name'])
                    ->setReplyTo('' != $params['reply_to'] ? $params['reply_to'] : $params['email_from'])
                    ->addTo($params['email_to'])
                    ->setSubject($params['subject'])
                    ->setBodyHtml($bodyHtml);
            try {
                @$mail->send();
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        } else {
            throw new ARTCMF_Exception('Not passed recipient.');
        }
    }
    
    
    public function getView()
    {
        if(is_null($this->_view)){
            $this->_view = new Zend_View();
            $this->_view->setScriptPath(APPLICATION_PATH . '/modules/email/views/templates/');
            $this->_view->setHelperPath(APPLICATION_PATH . '/../library/ARTCMF/View/Helper/', 'ARTCMF_View_Helper');
        }
        
        return $this->_view;
    }

    /**
     * sendEmail
     *
     * @param mixed $to
     * @param mixed $tpl_vars
     * @param string $tpl_var_wrapper
     * @return void
     */
//    public function sendEmail($params, $tpl_var_wrapper='@')
//    {
//        $email_arr = $this->makeEmail($params, $tpl_var_wrapper);
//        $mail = new Zend_Mail('UTF-8');
//        $mail->setFrom($email_arr['email_from']);
//        $mail->addTo($params['to']);
//        $mail->setSubject($email_arr['subject']);
//        $mail->setReplyTo($email_arr['reply_to']);
//        $mail->setBodyText($email_arr['message']);
//
//        return $mail->send();
//    }

    /**
     * makeEmail
     *
     * @param mixed $tpl_vars
     * @param mixed $tpl_var_wrapper
     * @return void
     */
//    public function makeEmail($tpl_vars, $tpl_var_wrapper)
//    {
//        $tpl = $this->getTemplateByIdent($tpl_vars['ident']);
//
//        if (!$tpl) {
//            throw new Zend_Exception( "Could not find e-mail template with name '".$tpl_vars['ident']."'" );
//        }
//        $result_arr = array();
//        foreach ($tpl->toArray() as $k=>$email_tpl_part){
//            $result_arr[$k] = $this->replaceVars($tpl_vars, $tpl_var_wrapper, $email_tpl_part);
//        }
//        return $result_arr;
//    }

    /**
     * replaceVars
     *
     * @param mixed $tpl_vars
     * @param mixed $tpl_var_wrapper
     * @param mixed $text
     * @return void
     */
//    public function replaceVars($tpl_vars, $tpl_var_wrapper, $text )
//    {
//        foreach ($tpl_vars as $key=>$value){
//            $text = preg_replace('/'.$tpl_var_wrapper.$key.$tpl_var_wrapper.'/', $value, $text);
//        }
//        return $text;
//    }

    /**
     * Implement the Zend_Acl_Resource_Interface, make this model
     * an acl resource
     *
     * @return string The resource id
     */
    public function getResourceId()
    {
        return 'Sender';
    }

    /**
     * Injector for the acl, the acl can be injected either directly
     * via this method or by passing the 'acl' option to the models
     * construct.
     *
     * We add all the access rule for this resource here, so we
     * add $this as the resource, plus its rules.
     *
     * @param ARTCMF_Acl_Interface $acl
     * @return ARTCMF_Model_Abstract
     */
    public function setAcl(ARTCMF_Acl_Interface $acl)
    {
        if (!$acl->has($this->getResourceId())) {
            $acl->add($this)
                    ->allow('Member', $this, array('viewTemplates'))
                    ->allow('Redactor', $this)
                    ->allow('Manager', $this)                    
                    ->allow('Admin', $this)
                    ->allow('Root', $this);
        }
        $this->_acl = $acl;
        return $this;
    }

    /**
     * Get the acl and automatically instantiate the default acl if one
     * has not been injected.
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        if (null === $this->_acl) {
            $this->setAcl(new ARTCMF_Acl());
        }
        return $this->_acl;
    }

}
