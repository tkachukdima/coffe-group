<?php

/**
 * Email_Resource_Recipient
 *
 * @category   Default
 * @package    Email_Model_Resource
 * @author     Andrew Mae, amey.pro@gmail.com
 * @license    Commercial License
 */
class Email_Resource_Recipient extends ARTCMF_Model_Resource_Db_Table_Abstract
{

    protected $_name = 'email_recipient';
    protected $_primary = 'recipient_id';
    protected $_rowClass = 'Email_Resource_Recipient_Item';

    public function getRecipientById($id)
    {
        return $this->find($id)->current();
    }

    /**
     * Get a list of template
     *
     * @param  boolean   $templated      Use Zend_Paginator?
     * @param  array     $order      Order results
     * @return Zend_Db_Table_Rowset|Zend_Paginator
     */
    public function getRecipients()
    {
        return $this->fetchAll();
    }

}