��    '      T  5   �      `     a     u     |     �  	   �     �     �     �     �     �     �     	                4     J  	   V  
   `  #   k  
   �     �     �     �     �     �     �     �  
   �  
   �     �                         $     +     0     8  L  J     �     �     �     �  	   �     �     �     �     �          /     @     Q     Y     m     �  	   �     �     �  
   �     �     �     �     �     	     	     	  
   !	  
   ,	  
   7	     B	     H	     M	  	   \	     f	     m	     r	     z	              $         &                              %                       	      !                          
                     '                           "                 #    404 page not found. Active Add Add File Add group Add poll Banner added Banner not found Banner type can not be found Banner type has been updated Banner type is added Banner updated Choices Date of end of poll Date of start of poll Description Edit file Edit group Error! Completed form is incorrect. File added File not found File updated Files General Settings Groups Main Name Not active Not found  Not selected Polls Save Select a type:  Sorting Status Text To vote Unique identifier Project-Id-Version: art-cmf
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-11-20 12:27+0300
PO-Revision-Date: 2012-11-20 12:28+0300
Last-Translator: Andrew Mae <amey.pro@gmail.com>
Language-Team: art-creative <office.artcreative@gmail.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate;_;gettext;gettext_noop
X-Poedit-Basepath: ../../
X-Poedit-Language: Norwegian Nynorsk
X-Poedit-Country: NORWAY
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: application
X-Poedit-SearchPath-1: library/ARTCMF
 404 side ikke funnet. Active Add Legg til fil Add group Add poll Banner lagt Banner ikke funnet Type banner finnes ikke Type banner har blitt oppdatert Type banner lagt Banner oppdatert Choices Date of end of poll Date of start of poll Beskrivelse Edit file Rediger gruppe Feilen korrekt utfylt skjema. File added Finner ikke filen File updated Filer Generelle innstillinger Grupper Hoved Navn Not active Not found  Ikke valgt Polls Save Velg en type:  Sortering Status Text To vote Unik identifikator 