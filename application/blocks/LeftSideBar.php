<?php

class Block_LeftSideBar extends Zend_Controller_Action_Helper_Abstract
{

    private $_view;

    public function __construct($view)
    {
        $this->_view = $view;        
    }

    public function init()
    {
        $this->createLeftSideBarBlock();
    }

    public function createLeftSideBarBlock()
    {
        $layout = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('Layout');
        $layout->leftSideBar = $this->_view->partial('left_side_bar.phtml');
    }

}
